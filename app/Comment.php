<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function commentable()
    {
        return $this->morphTo();
    }

//    public function user()
//    {
//        return $this->belongsTo(User::class, 'id', 'commentable_id');
//    }

//    public function product()
//    {
//        return $this->belongsTo(Product::class, 'id', 'commentable_id');
//    }

}
