<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyTranslation extends Model
{
    protected $table = 'companies_translations';
    protected $fillable = ['name'];
    public $timestamps = false;
}
