<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Services\DataTable;

class DelegateDatatable extends DataTable
{
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('image', function ($item) {
                 return "<a href=".asset('public/uploads/delegates/'.$item->image)." data-fancybox>" . "<img src=".asset('public/uploads/delegates/'.$item->image)." />";
            })
            ->editColumn('actions', function ($item) {
                $htm = '< data-toggle="tooltip" data-placement="bttom" title="'._lang("back/layout.edit").'" href="'.route("del.show", $item->id).'" class="badge bg-light-blue" style="background-color: #0d95e8"> <i class="fas fa-eye" aria-hidden="true"></i></a>';
                $htm.= '< data-toggle="tooltip" data-placement="bttom" title="'._lang("back/layout.delete").'" href="'.route("del.delete", $item->id).'" class="badge bg-red" style="background-color: red"> <i class="fas fa-trash" aria-hidden="true"></i></a>';

                return $htm;
            })
            ->rawColumns(['id', 'name', 'username', 'phone', 'email', 'status', 'image', 'actions'])
            /*->addColumn('name', 'admin.categorysub.buttons.langcol')*/
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Categorysub::query();

        if (request()->has('from') && request()->get('from') != '') {
            $query = $query->whereDate('created_at', '>=', request()->get('from'));
        }

        if (request()->has('to') && request()->get('to') != '') {
            $query = $query->whereDate('created_at', '<=', request()->get('to'));
        }

        if (request()->has("name") && request()->get("name") != "") {
            $query = $query->where("name", "like", "%" . request()->get("name") . "%");
        }


        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'name' => "id",
                'data' => 'id',
                'title' => 'id',
            ],
            [
                'name' => 'name',
                'data' => 'name',
                'title' => 'name',
                'orderable' => false,
            ],
            [
                'name' => 'phone',
                'data' => 'phone',
                'title' => 'phone',
//                'render' => 'function(){
//                        return JSON.parse(this.categorymain_name).' . getCurrentLang() . ';
//                    }',

            ],
            [
                'name' => 'email',
                'data' => 'email',
                'title' => 'email',

                'orderable' => false,

            ],

            [
                'name' => 'actions',
                'data' => 'actions',
                'title' => 'actions',

                'orderable' => false,

            ],
            [
                'name' => 'control',
                'data' => 'control',
                'title' => trans('curd.control'),
                'exportable' => false,
                'printable' => false,
                'searchable' => false,
                'orderable' => false,
            ]

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Categorysubdatatables_' . time();
    }
}
