<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/21/2019
 * Time: 2:39 PM
 */


use Illuminate\Support\Facades\Lang;

if (!function_exists('_lang')) {
    function _lang($item) {
        if (Lang::has($item)) {
            $line = Lang::get($item);
        } else {
            $item_arr = explode('.', $item);
            array_shift($item_arr);
            $line = end($item_arr);
            $line = str_replace('_', ' ', ucwords($line));
        }
        return $line;
    }
}