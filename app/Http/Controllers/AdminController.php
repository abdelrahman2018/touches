<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = User::Admin()->get();
        return view('dashboard.admins.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('dashboard.admins.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,  [
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        $admin = new User();
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->type = 3;
        $admin->active = 1;
        $admin->password = Hash::make($request->password);
        $admin->save();

        if ($request->roles != null){
            $admin->syncRoles($request->roles);

            foreach ($request->roles as $role){
                $roleObj = Role::findByName($role);
                $admin->givePermissionTo($roleObj->permissions()->get()->pluck('name'));
            }
        }


        session()->flash('success', 'Admin Created');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = User::findOrFail($id);
        $roles = Role::all();
        return view('dashboard.admins.edit', compact('admin', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,  [
            'name' => 'required|string',
            'email' => 'required|email',
        ]);

        $admin = User::findOrFail($id);
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->active = $request->status;

        if($request->password != null)
            $admin->password = Hash::make($request->password);

        if (!empty($request->roles)){
            $admin->syncRoles($request->roles);
            foreach ($request->roles as $role){
                $roleObj = Role::findByName($role);
                $admin->givePermissionTo($roleObj->permissions()->get()->pluck('name'));
            }
        }


        $admin->save();

        session()->flash('success', 'Admin updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = User::findOrFail($id);
        $admin->delete();

        session()->flash('success', 'Admin deleted');
        return redirect()->back();
    }
}
