<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProductResource;
use App\Product;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'tokenCheck'])->except('updateCart');
    }

    public function checkCartValidation(Request $request)
    {
        $items = $request->items;
//        dd($items);
        $check = true;
        $total = 0;
        $invalidProducts = array();
        $updatedProducts = array();
        if(is_array($items)){
            foreach ($items as $item){
                $itemObject = Product::where('id', $item['id'])->first();
                if(!$itemObject){
                    $check = false;
                    array_push($invalidProducts, $item['id']);
                }elseif($itemObject->price != $item['price'] || $itemObject->packet->min_quantity > $item['qty'] || $itemObject->discount_price != $item['discount']){
                    array_push($updatedProducts, $itemObject);
                }

                else
                    $total += $itemObject->price * $item['qty'];
            }

            if (!empty($invalidProducts) || !empty($updatedProducts))
                return response()->json(['valid' => $check, 'total' => $total, 'invalid_products' => $invalidProducts, 'updated_products' => $updatedProducts], 400);

        }

        return response()->json(['valid' => $check, 'total' => $total], 200);
    }

    public function updateCart(Request $request)
    {
        $settings = Setting::all();
        $sett = array();
        foreach ($settings as $row){
            $sett = array_add($sett, $row->key, $row->value);
        }

        $items = $request->items;
        $products = Product::whereIn('id', $items)->get();

        return response()->json([
            'products' => ProductResource::collection($products),
            'delivery_charge' => $sett['delivery_charge']
        ], 200);
    }

}
