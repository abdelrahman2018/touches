<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'tokenCheck']);
    }


    public function createComment(Request $request)
    {
        $rules = [
            'type' => 'required',
            'commentable_id' => 'required',
            'order_id' => 'required',
            'comment' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json($validator->errors(), 400);

        if($request->type == 'product')
            $commentable_type = Product::class;
        else
            $commentable_type = User::class;

        $c = new Comment();
        $c->user()->associate($request->user());
        $c->order_id = $request->order_id;
        $c->commentable_id = $request->commentable_id;
        $c->commentable_type = $commentable_type;
        $c->comment = $request->comment;
        $c->save();

        return response()->json(['msg' => 'comment created'], 200);
    }
}
