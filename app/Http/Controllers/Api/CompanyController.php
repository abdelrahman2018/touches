<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = CompanyResource::collection(Company::paginate(10));
        return $companies;
    }

    public function show($id)
    {
        $company = Company::findOrFail($id);
        return ProductResource::collection($company->products()->paginate(10));
    }
}
