<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{


    public function sendMsg(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email',
            'msg' => 'required',
            'msg_type' => 'required|numeric'
        ];


        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
            return response()->json($validator->errors(), 400);

        DB::table('contact')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'msg_type' => $request->msg_type,
            'msg' => $request->msg,
            'created_at' => now()
        ]);

        return response()->json(['msg' => 'your msg sent success'], 200);
    }
}
