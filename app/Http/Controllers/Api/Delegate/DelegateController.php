<?php

namespace App\Http\Controllers\Api\Delegate;

use App\Rate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DelegateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    public function getRate(Request $request)
    {
        $total = 0;
        $rates = Rate::where('rateable_id', $request->user()->id)->where('rateable_type', User::class)->get();

        foreach ($rates as $rate){
            $total+= $rate->rate;
        }

        return response()->json(['total_rate' => $total/count($rates)], 200);
    }

    public function saveTrip(Request $request)
    {
        DB::table('delegate_trips')->insert([
            'delegate_id' => $request->user()->id,
            'lat' => $request->lat,
            'lng' => $request->lng,
        ]);

        return response()->json(['msg' => 'location saved'], 200);
    }
}
