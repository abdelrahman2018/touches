<?php

namespace App\Http\Controllers\Api\Delegate;

use App\Comment;
use App\Http\Resources\OrderResource;
use App\Notifications\FirebaseNotification;
use App\Order;
use App\Rate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getOrders(Request $request)
    {

        $ooorder = array();
        $delegate = array();
        $final = array();

        if($request->status_code == 1){
            $Orders = Order::where('delegate_id', $request->user()->id)->whereIn('status_code', [1,2,3])->get();
        }else
            $Orders = Order::where('delegate_id', $request->user()->id)->where('status_code', $request->status_code)->with('rate')->get();

            foreach ($Orders as $order){
                $ooorder = array_add($ooorder, 'order', new OrderResource($order));

                if ($order->status_code == 4){
                    $rate = Rate::where('rateable_id', $order->delegate_id)->where('rateable_type', User::class)->where('order_id', $order->id)->first();
                    $comment = Comment::where('commentable_id', $order->delegate_id)->where('commentable_type', User::class)->where('order_id', $order->id)->first();
                    if($rate){
                        $delegate = array_add($delegate, 'isRated', true);
                        $delegate = array_add($delegate, 'rate', $rate->rate);
                    }else{
                        $delegate = array_add($delegate, 'isRated', false);
                    }

                    if ($comment){
                        $delegate = array_add($delegate, 'isCommented', true);
                        $delegate = array_add($delegate, 'comment', $comment->comment);
                    }else{
                        $delegate = array_add($delegate, 'isCommented', false);
                    }
                }

                $ooorder = array_add($ooorder, 'delegate', $delegate);
                array_push($final, $ooorder);
                $ooorder = null;
                $delegate = [];
            }


        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // Create a new Laravel collection from the array data
        $itemCollection = collect($final);

        // Define how many items we want to be visible in each page
        $perPage = 10;

        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator(array_values($currentPageItems) , count($itemCollection), $perPage);

        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return response()->json(['data' => $paginatedItems], 200);

//
//        if ($Orders){
//            $newOrders = OrderResource::collection($newOrders);
//        }else
//            $newOrders = null;

//
//        $currentOrders = Order::where('delegate_id', $request->user()->id)->where('status_code', 1)->get();
//        if ($currentOrders){
//            $currentOrders = OrderResource::collection($currentOrders);
//        }else
//            $currentOrders = null;
//
//
//        $finishedOrders = Order::where('delegate_id', $request->user()->id)->where('status_code', 4)->get();
//        if($finishedOrders){
//            $finishedOrders = OrderResource::collection($finishedOrders);
//        }else
//            $finishedOrders = null;
//
//        return response()->json([['new' => $newOrders, 'count' => $newOrders->count()], ['current' => $currentOrders, 'count' => $currentOrders->count()], ['finished' => $finishedOrders, 'count' => $finishedOrders->count()]],200);
    }

    public function getOrdersCount(Request $request)
    {


        $newOrders = Order::where('delegate_id', $request->user()->id)->where('status_code', 0)->get();
        $currentOrders = Order::where('delegate_id', $request->user()->id)->whereIn('status_code', [1,2,3])->get();
        return response()->json(['new_orders_count' => count($newOrders), 'current_orders_count' => count($currentOrders)], 200);
    }

    public function changeStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|numeric',
            'order_id' => 'required|numeric',
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 400);


        $order = Order::where('delegate_id', $request->user()->id)->where('id', $request->order_id)->first();
        if ($order){
            $data = [
                'click_action' => 'order',
                'target_id' => $order->id
            ];

            $order->status_code = $request->status;
            $order->save();

            $order->user->notify(new FirebaseNotification('Snow Energy', 'Your Order Status Changed', $data));

            return response()->json(['msg' => 'order status changed'], 200);
        }

        return response()->json(['msg' => 'invalid order_id'], 400);

    }
}
