<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 4/22/2019
 * Time: 5:44 PM
 */

namespace App\Http\Controllers\Api\Helpers;


use Illuminate\Http\Request;

class MobSMS
{
    public  function resendCode(Request $request)
    {
        $phone = '+966'.$request->phone;
        $user = \App\User::where('phone', $phone)->first();
        if ($user){
            $code = 1234; //rand(1,9999);
            $user->sms_code = $code;
            $user->save();

            $res = sendSMS($phone, $code);
            if ($res)
                return response()->json(['msg' => 'msg sent success'], 200);
        }

        return response()->json(['msg' => 'invalid phone number'], 400);
    }
}