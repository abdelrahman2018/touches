<?php

namespace App\Http\Controllers\Api;

use App\Coupon;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\UserResource;
use App\Order;
use App\Product;
use App\Rate;
use App\Setting;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'tokenCheck']);
    }


    public function checkOrderTime(Request $request)
    {
        $date = $request->date;
        if ($date < now()->addDay())
            return response()->json(['valid' => false], 400);

        return response()->json(['valid' => true], 200);
    }


    public function orderSave(Request $request)
    {

//        dd($request->items);
//        $tmp = array();
        $cartItems = array();
        $coupon_id = null;
        $totalArr = $this->calculateTotal($request->items);
        $total = $totalArr['total'];
        $deliveryCharge = $totalArr['delivery'];

        if ($request->has('coupon')) {
            $copon = Coupon::where('number', $request->coupon)->first();
            $coupon_id = $copon->id;
            $total = $total - ($total * ($copon->value/100));
        }



//        dd($attachedProducts);

        $order = new Order();
        $order->date = Carbon::parse($request->date)->toDateTimeString();
        $order->lat = $request->lat;
        $order->lng = $request->lng;
        $order->payment_type = $request->payment_type;
        $order->address = json_encode($request->address);
        $order->status_code = 0;
        $order->coupon_id = $coupon_id;
        $order->total = $total;
        $order->user()->associate($request->user());
        $order->save();

//       dd( $request->items);

//
        for ($i = 0; $i < count($request->items); $i+=2){
            $product = Product::findOrFail($request->items[$i]);
//            $rate = Rate::where('rateable_id', $product->id)->where('rateable_type', Product::class)->where('order')

            $cartItem = [
                'id' => $product->id,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
            ];

//            $cartItem = [
//                'id' => $product->id,
//                'name' => $product->name,
//                'qty' => $request->items[$i+1],
//                'price' => $product->price,
//                'options' => [
//                    'company' => $product->company->name,
//                    'size' => $product->size,
//                    'unit' => $product->unit->name,
//                    'packet' => $product->packet->name,
//                    'image' => $product->image,
//                    'discount_price' => $product->discount_price,
//                    'total price' => $request->items[$i+1] * $product->price - ($product->discount_price * $request->items[$i+1])
//                ]
//            ];

            array_push($cartItems, $cartItem);
            $order->products()->attach($request->items[$i]);
        }

        $order->cart = json_encode($request->items);
        $order->productsb = json_encode($cartItems);
        $order->save();

//        return $order->products;


        if($request->payment_type == 1)
            $this->onlinePayment($order);

        return response()->json(['order' => new OrderResource($order), 'delivery' => $deliveryCharge], 200);

    }

    public function calculateTotal($items)
    {


        $total = 0;
        $discount = 0;

        for ($i = 0; $i < count($items); $i+=2){
            $ItemObj = Product::findOrFail($items[$i]);
            if ($ItemObj->discount_price > 0)
                $discount = $ItemObj->discount_price;

            $total+= ($ItemObj->price - $discount) * $items[$i+1];
            $discount = 0;
        }

        return ['total' => $total , 'delivery' => $this->chargeCost()] ;
    }


    public function getOrders(Request $request)
    {
        $orders = $request->user()->orders()->get();
        $ooorder = array();
        $productsArr = array();
        $final = array();


        foreach ($orders as $order){

            $ooorder = array_add($ooorder, 'order', new OrderResource($order));
            $cart = json_decode($order->cart);
            $cartItems = json_decode($order->productsb);

                for ($i = 0; $i < count($cart); $i+=2){

                    foreach ($cartItems as $item){
                        if ($cart[$i] == $item->id){
                            $tmpItem = $item;
                        }
                    }
                    $product = Product::find($cart[$i]);
                    $rate = Rate::where('rateable_id', $product->id)->where('rateable_type', Product::class)->where('order_id', $order->id)->first();
                    $cartItem = [
                        'id' => $product->id,
                        'name' => $product->name,
                        'price' => $tmpItem->price,
                        'qty' => $cart[$i+1],
                        'options' => [
                            'company' => $product->company->name,
                            'size' => $product->size,
                            'unit' => $product->unit->name,
                            'packet' => $product->packet->name,
                            'image' => $product->image,
                            'discount_price' => $tmpItem->discount_price,
                            'rate' => $rate? $rate->rate:null
                        ],
                        'isRated' => $rate? true: false
                    ];

                    array_push($productsArr, $cartItem);
                }

                if ($order->status_code == 4){
                    $rate = Rate::where('rateable_id', $order->delegate_id)->where('rateable_type', User::class)->where('order_id', $order->id)->first();
                    if($rate){
                        $ooorder = array_add($ooorder, 'delegate_isRated', true);
                    }else{
                        $ooorder = array_add($ooorder, 'delegate_isRated', false);
                    }
                }

                $ooorder = array_add($ooorder, 'products', $productsArr);
                array_push($final, $ooorder);
                $ooorder = null;
                $productsArr = array();

        }
//        return $ooorder;
//        return OrderResource::collection($orders);

//        $currentPage = Paginator::resolveCurrentPage();
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // Create a new Laravel collection from the array data
        $itemCollection = collect($final);

        // Define how many items we want to be visible in each page
        $perPage = 10;

        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();


        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator(array_values($currentPageItems) , count($itemCollection), $perPage);
//        $paginatedItems = new Paginator($itemCollection, $perPage, $currentPageItems);

        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return response()->json(['data' => $paginatedItems], 200);
    }

    public function getOrder($id)
    {

        $delegateArr = array();
        $finalProducts = array();
        $final = array();
        $order = Order::findOrFail($id);
        $cartItems = json_decode($order->cart);
        $oldProducts = json_decode($order->productsb);


        for ($i = 0; $i < count($cartItems); $i+=2){

            foreach ($oldProducts as $item){
                if ($cartItems[$i] == $item->id){
                    $tmpItem = $item;
                }
            }

            $product = Product::findOrFail($cartItems[$i]);
            $rate = Rate::where('rateable_id', $product->id)->where('rateable_type', Product::class)->where('order_id', $order->id)->first();
            $comment = $product->comments()->where('order_id', $order->id)->first();
            $cartItem = [
                'id' => $product->id,
                'name' => $product->name,
                'qty' => $cartItems[$i+1],
                'price' => $tmpItem->price,
                'options' => [
                    'company' => $product->company->name,
                    'size' => $product->size,
                    'unit' => $product->unit->name,
                    'packet' => $product->packet->name,
                    'image' => $product->image,
                    'rate' => $rate? $rate->rate: null,
                    'comment' => $comment? $comment->comment: null ,
                    'discount_price' => $tmpItem->discount_price,
                    'total price' => $cartItems[$i+1] * $product->price - ($product->discount_price * $cartItems[$i+1])
                ],
                'isRated' => $rate? true: false
            ];

            array_push($finalProducts, $cartItem);
//            $order->products()->attach($cartItems[$i]);
        }

        $delegate = User::where('id', $order->delegate_id)->first();
        if ($delegate){
            $rate = $delegate->rates()->where('order_id', $id)->pluck('rate')->first();
            $comment =  $delegate->comments()->where('order_id', $id)->pluck('comment')->first();
            $delegateArr = array_add($delegateArr, 'rate', $rate? $rate:null);
            $delegateArr = array_add($delegateArr, 'comment', $comment? $comment:null);
        }else
            $delegateArr = [
                'rate' => null,
                'comment' => null
            ];

        if (count($order->products) > 0){
            foreach ($order->products as $product){
                $productsArr = array();
                $productsArr = array_add($productsArr, 'product', new ProductResource($product));
                $productsArr =array_add($productsArr, 'rate', $product->rates()->where('order_id', $id)->pluck('rate')->first());
                $productsArr =array_add($productsArr, 'comment', $product->comments()->where('order_id', $id)->pluck('comment')->first());
                array_push($final, $productsArr);
            }
        }

//        return($productsArr);
        return response()->json(['order' => new OrderResource($order), 'products' => $finalProducts, 'delegate' => $delegateArr, 'charge' => $this->chargeCost()], 200);
    }



    public function validateCoupon(Request $request)
    {
        $copon = Coupon::where('number', $request->c_number)->first();
        if (!$copon)
            return response()->json(['msg' => 'invalid code'], 400);

        if ($copon->exp < now())
            return response()->json(['msg' => 'coupon expired'], 400);

        return response()->json(['msg' => 'valid', 'value' => $copon->value], 200);
    }

    public function chargeCost()
    {
        $settings = Setting::all();
        $sett = array();
        foreach ($settings as $row){
            $sett = array_add($sett, $row->key, $row->value);
        }

        return $sett['delivery_charge'];
    }

    public function onlinePayment($order)
    {
        //todo pay with cart
    }
}
