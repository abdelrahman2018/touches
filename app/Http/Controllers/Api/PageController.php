<?php

namespace App\Http\Controllers\Api;

use App\Page;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function getPage($id)
    {
        $page = Page::findOrFail($id);
        return response()->json($page, 200);
    }

    public function settings()
    {
        $settings = Setting::all();
        $sett = array();
        foreach ($settings as $row){
            $sett = array_add($sett, $row->key, $row->value);
        }

        return response()->json($sett, 200);
    }
}
