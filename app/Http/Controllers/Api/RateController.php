<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Order;
use App\Product;
use App\Rate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RateController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'tokenCheck']);
    }


    public function rateSave(Request $request)
    {
        $rules = [
            'type' => 'required',
            'order_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json($validator->errors(), 400);

        $order = Order::findOrFail($request->order_id);

        if($request->type == 'product'){
            $rateable_type = Product::class;
            $rateable_id = $request->rateable_id;
        }
        else{
            $rateable_type = User::class;
            $rateable_id = $order->delegate_id;
        }


        $r = new Rate();
        $r->user_id = $request->user()->id;
        $r->order_id = $request->order_id;
        $r->rateable_id = $rateable_id;
        $r->rateable_type = $rateable_type;
        $r->rate = $request->rate;
        $r->save();

        if($request->has('comment')){
            $c = new Comment();
            $c->user_id  = $request->user()->id;
            $c->order_id = $request->order_id;
            $c->commentable_id = $rateable_id;
            $c->commentable_type = $rateable_type;
            $c->comment = $request->comment;
            $c->save();
        }

        return response()->json(['msg' => 'rate & comment created'], 200);

    }
}
