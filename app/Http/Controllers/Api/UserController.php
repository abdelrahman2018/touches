<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Auth\Api\AuthController;
use App\Http\Resources\UserResource;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:api', 'tokenCheck']);
    }

    public function editProfile(Request $request)
    {
        $user = $request->user();

        if ($request->has('name')){
            $user->name = $request->name;
        }

        if ($request->has('username')){
            $user->username = $request->username;
        }

        if ($request->has('email')){
            $user->email = $request->email;
        }

        if ($request->has('phone')){
//            $code = rand(1, 9999);
            $code = 1234;
            $user->sms_code = $code;
            $user->save();

            $res = sendSMS($request->phone, $code);
            if ($res)
                return response()->json(['msg' => 'verification code sent'], 200);
            else
                return response()->json(['msg' => 'failed to send verification code'], 400);
        }

        if ($request->has('profile_pic')){

            $image = $request->profile_pic; // image base64 encoded

            preg_match("/data:image\/(.*?);/",$image,$image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/','',$image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10).'.'.$image_extension[1];
            File::put(public_path(). '/uploads/clients/' . $imageName, base64_decode($image));
            $user->image = 'public/uploads/clients/'.$imageName;

        }

        $user->save();
        return response()->json(['msg' => 'your data updated', 'client' => new UserResource($user)], 200);
    }


    public function verifyPhone(Request $request)
    {
        $user = $request->user();
        if ($user->sms_code == $request->code){
            $user->sms_code = null;
            $user->save();
            return response()->json(['valid' => true], 200);
        }

        return response()->json(['valid' => false], 400);
    }


    public function changePhone(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone' => 'required|numeric'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 400);


        $user = $request->user();
        $user->phone = '+966'.$request->phone;
        $user->save();

        return response()->json(['data' => ['isChanges' => true, 'client' => new UserResource($user)]], 200);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|confirmed'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 400);

        $user = $request->user();
        if (Hash::check($request->old_password, $user->password)){
            $user->password = Hash::make($request->new_password);
            $user->save();

            return response()->json(['msg' => 'your password changed'], 200);
        }

        session()->invalidate();
        return response()->json(['msg' => 'invalid password'], 400);
    }

    public function refreshDeviceToken(Request $request)
    {
        $client = User::findOrFail($request->user()->id);
        $client->device_token = $request->device_token;
        $client->save();

        return \response()->json(['msg' => 'device token refreshed'], 200);
    }
}
