<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Resources\UserResource;
use App\Token;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except(['register', 'login', 'delegateLogin', 'verfyCode', 'forgetPassword', 'resetPassword', 'verifyCode', 'sendRegisterCode', 'verifyRegisterCode']);
    }


    public function sendRegisterCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|string|unique:users|min:9',
        ]);

        if ($validator->fails())
            return response()->json(['errors' => $validator->errors()], 400);

        $code = 1234;
        $res = sendSMS($request->phone, $code);
        if ($res){
            $client = new User();
            $client->phone = $request->phone;
            $client->sms_code = $code;
            $client->save();
            return response()->json(['msg' => 'verification code sent'], 200);
        }

        return response()->json(['msg' => 'invalid phone'], 400);
    }

    public function verifyRegisterCode(Request $request)
    {
        $user = User::where('sms_code', $request->code)->where('phone', $request->phone)->first();
        if ($user){
            $user->sms_code = null;
            $user->active = 1;
            $user->save();

            return response()->json(['msg' => 'phone number verified'], 200);
        }

        return response()->json(['msg' => 'invalid verification code'], 400);
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'username' => 'required|string|unique:users',
            'phone' => 'required|min:9',
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6',
            'type' => 'required',
            'device_token' => 'required'
        ]);

        if ($validator->fails())
            return response()->json(['errors' => $validator->errors()], 400);


        $client = User::where('phone', $request->phone)->first();
        if(!$client)
            return response()->json(['msg' => 'invalid phone'], 400);

        $client->name = $request->name;
        $client->username = $request->username;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->password = Hash::make($request->password);
        $client->type = 1;
        $client->device_token = $request->device_token;

        if ($request->has('image')){
            $image = $request->file('image');
            $imgName = time().$image->getClientOriginalName();
            $image->move(public_path().'/uploads/clients/'. $imgName);

            $client->image = 'public/uploads/clients/'.$imgName;
        }else{
            $client->image = 'public/uploads/profile.png';
        }

        $client->save();

        $tokenResult = $client->createToken('Personal Access Token');
        $token = $tokenResult->token;
//        if ($request->remember_me)
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        DB::table('oauth_refresh_tokens')->insert(
            [
                'access_token_id' =>$token->id,
                'revoked' => 0,
                'expires_at' => now()->addWeeks(2)
            ]
        );

        return response()->json([
            'client' => new UserResource($client),
            'access_token' => 'Bearer '.$tokenResult->accessToken,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'refresh_token' => $tokenResult->token->id,
            'refresh_expires_at' => now()->addWeeks(2)
        ]);

    }


    public function login(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'username' => 'required|string',
            'password' => 'required|string',
            'remember_me' => 'boolean',
            'device_token' => 'required'
        ]);


        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()], 400);
        }


//        $deviceToken = new Token();
//        $deviceToken->token_string = $request->device_token;
//        $deviceToken->save();

        $credentials = Request(['username', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'invalid username or password'
            ], 400);

        $user = $request->user();
        $user->device_token = $request->device_token;
        $user->save();
//        $user->associate($deviceToken);

        if ($user->active == 0)
            return response()->json('your account not verified', 400);

        $tokenResult = $user->createToken('Personal Access Token');

        DB::table('oauth_refresh_tokens')->insert(
            [
                'access_token_id' =>$tokenResult->token->id,
                'revoked' => 0,
                'expires_at' => now()->addWeeks(2)
            ]
        );
        $token = $tokenResult->token;
//        if ($request->remember_me)
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'client' => new UserResource($user),
            'access_token' => 'Bearer '.$tokenResult->accessToken,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'refresh_token' => $tokenResult->token->id,
            'refresh_expires_at' => now()->addWeeks(2)
        ]);
    }

    public function delegateLogin(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'username' => 'required|string',
            'password' => 'required|string',
            'remember_me' => 'boolean',
            'device_token' => 'required'
        ]);


        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()], 400);
        }


//        $deviceToken = new Token();
//        $deviceToken->token_string = $request->device_token;
//        $deviceToken->save();

        $credentials = Request(['username', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'invalid username or password'
            ], 400);

        $user = $request->user();
        $user->device_token = $request->device_token;
        $user->save();
//        $user->associate($deviceToken);

        if ($user->active == 0)
            return response()->json('your account not verified', 400);

        $tokenResult = $user->createToken('Personal Access Token');

        DB::table('oauth_refresh_tokens')->insert(
            [
                'access_token_id' =>$tokenResult->token->id,
                'revoked' => 0,
                'expires_at' => now()->addWeeks(2)
            ]
        );
        $token = $tokenResult->token;
//        if ($request->remember_me)
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'client' => new UserResource($user),
            'access_token' => 'Bearer '.$tokenResult->accessToken,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'refresh_token' => $tokenResult->token->id,
            'refresh_expires_at' => now()->addWeeks(2)
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'], 200);
    }


//    public function verfyCode(Request $request)
//    {
//
//        $validator = Validator::make($request->all(),[
//            'code' => 'required|string',
//            'phone' => 'required',
//        ]);
//
//
//        if($validator->fails()){
//            return response()->json(['errors' => $validator->errors()], 400);
//        }
//
//        $user = User::where('sms_code', $request->code)->where('phone', $request->phone)->first();
//        if($user){
//            $user-> active = 1;
//            $user->sms_code = null;
//            $user->save();
//
//            $tokenResult = $user->createToken('Personal Access Token');
//            $token = $tokenResult->token;
//            $token->save();
//
//            return response()->json([
//                'client' => new UserResource($user),
//                'access_token' => 'Bearer '.$tokenResult->accessToken,
//                'expires_at' => Carbon::parse(
//                    $tokenResult->token->expires_at
//                )->toDateTimeString()
//            ]);
//        }
//        return response()->json('invalid code', 400);
//    }


    public function forgetPassword(Request $request)
    {
        $user = User::where('phone', $request->phone)->first();
        if ($user){
//            $code = rand(1, 9999);
            $code = 1234;
            $user->sms_code = $code;
            $user->save();

            $res = sendSMS($request->phone, $code);

            if ($res)
                return response()->json('code send done', 200);

        }

        return response()->json('invalid phone number', 400);
    }

    public function verifyCode(Request $request)
    {
        $user = User::where('sms_code', $request->code)->first();
        if ($user){
            $user->sms_code = null;
            $user->save();
            return response()->json('code checked done', 200);
        }

        return response()->json('invalid code', 400);
    }

    public function resetPassword(Request $request)
    {
        $rules = [
            'phone' => 'required',
            'new_password' => 'required|string|confirmed'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response()->json($validator->errors(), 400);

        $user = User::where('phone', $request->phone)->first();

        $user->password = Hash::make(strval($request->new_password));
        $user->save();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        DB::table('oauth_refresh_tokens')->insert(
            [
                'access_token_id' =>$token->id,
                'revoked' => 0,
                'expires_at' => now()->addWeeks(2)
            ]
        );

        return response()->json([
            'client' => new UserResource($user),
            'access_token' => 'Bearer '.$tokenResult->accessToken,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'refresh_token' => $tokenResult->token->id,
            'refresh_expires_at' => now()->addWeeks(2)
        ]);

//        return response()->json('invalid password', 400);
    }

    public function refreshToken(Request $request)
    {
        $user = $request->user();
        $user->token()->revoke();
        DB::table('oauth_refresh_tokens')->where('access_token_id', $user->token()->id)->update([
            'revoked' => 1
        ]);

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        DB::table('oauth_refresh_tokens')->insert(
            [
                'access_token_id' =>$token->id,
                'revoked' => 0,
                'expires_at' => now()->addWeeks(2)
            ]
        );


        return response()->json([
            'access_token' => 'Bearer '.$tokenResult->accessToken,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'refresh_token' => $token->id,
            'refresh_expires_at' => now()->addWeeks(2)
        ]);
    }
}
