<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guest')->except('logout');

//        $link = $request->url();
//        if( Str::contains( $link, 'admin' ) !== false) {
//
//            if (isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])
//                && $_SERVER['PHP_AUTH_USER'] === 'adminuser'
//                && $_SERVER['PHP_AUTH_PW'] === '3%?uD}7HSn}-') {
//
////                dd(1);
//                // User is properly authenticated...
//
//            } else {
//                header('WWW-Authenticate: Basic realm="Secure Site"');
//                header('HTTP/1.0 401 Unauthorized');
//                exit('This site requires authentication');
//            }
//
//        }
    }

    public function login(Request $request)
    {


        $email = $request->email;
        $pass = $request->password;

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $user = User::where('email', $email)->first();
        if ($user){
            if (Hash::check($pass, $user->password) && $user->type == 3){
                $this->attemptLogin($request);
                return $this->sendLoginResponse($request);
            }else{
                $this->incrementLoginAttempts($request);

                return $this->sendFailedLoginResponse($request);
            }
        }else{
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }

//
//        if ($this->attemptLogin($request)) {
//            if($this->guard()->user()->type == 3)
//                return $this->sendLoginResponse($request);
//        }
//
//        // If the login attempt was unsuccessful we will increment the number of attempts
//        // to login and redirect the user back to the login form. Of course, when this
//        // user surpasses their maximum number of attempts they will get locked out.
//        $this->incrementLoginAttempts($request);
//
//        return $this->sendFailedLoginResponse($request);
    }


    public function logout(Request $request)
    {
        $this->guard()->logout();

        $_SERVER['PHP_AUTH_USER'] = '';
        $_SERVER['PHP_AUTH_PW'] = '';
        unset($_SERVER['PHP_AUTH_USER']);
        unset($_SERVER['PHP_AUTH_PW']);
        $request->session()->invalidate();

        return redirect('admin');
    }
}
