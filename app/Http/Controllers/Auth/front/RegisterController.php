<?php

namespace App\Http\Controllers\Auth\front;

use App\Customer;
use App\Http\Controllers\Helpers\shoppingCart;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    use RegistersUsers;

    public function showForm()
    {
        return view('auth.front.register');
    }

    protected function guard()
    {
        return auth()->guard('customer');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:customers'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $customer = new Customer();
        $customer->email = $request->email;
        $customer->password = Hash::make($request->password);
        $customer->save();

        $wishContent = Cart::content();
        $this->guard()->login($customer);
        shoppingCart::wishlistToDB($wishContent);

        return redirect('/');
    }


}
