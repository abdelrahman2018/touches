<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    public $locales;

    public function __construct()
    {
        $this->locales = config('translatable.locales');
    }
}
