<?php

namespace App\Http\Controllers;

use App\DataTables\ClientDataTable;
use App\Http\Controllers\Helpers\FCM;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;
class ClientController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        dd($from);
        $clients = User::Client()->get();
        return view('dashboard.clients.index', compact('clients'));
    }


    public function getClients()
    {
        return Datatables::of(User::where('type', 1)->get())->make(true);
    }

    public function index2(ClientDataTable $dataTable)
    {
        return $dataTable->render('dashboard.clients.index');
    }

    public function clientSearch(Request $request)
    {
        $from = $request->from? $request->from:now();
        $to = $request->to? $request->to:now();

//        dd($from);
        $clients = User::Client();
        if ($request->from != null)
            $clients->where('created_at','>=',$request->from);

        if ($request->to != null)
        {
            $to = new Carbon($request->to);
            $to->addDay();
            $to = $to->toDateString();
            $clients->where('created_at','<=',$to);
        }

        $clients = $clients->get();
        return view('dashboard.clients.search', compact('clients', 'from', 'to'));
    }

    public function clientSearchExport($from, $to)
    {
        $clients = User::Client();
        if ($from != null)
            $clients->where('created_at','>=',$from);

        if ($to != null)
        {
            $to = new Carbon($to);
            $to->addDay();
            $to = $to->toDateString();
            $clients->where('created_at','<=',$to);
        }

        $client_data = $clients->get();
        $client_array[] = array('Name', 'Username', 'phone', 'email', 'created At');
        foreach ($client_data as $client) {

            $client_array[] = array(
                'Name' => $client->name,
                'Username' => $client->username,
                'phone' => $client->phone,
                'email' => $client->email,
                'created At' => $client->created_at
            );
        }
        Excel::create('Clients', function ($excel) use ($client_array) {
            $excel->setTitle('Clients');
            $excel->sheet('Clients', function ($sheet) use ($client_array) {
                $sheet->fromArray($client_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    public function clientExport()
    {
        $clients = User::Client();

        $client_data = $clients->get();
        $client_array[] = array('Name', 'Username', 'phone', 'email', 'created At');
        foreach ($client_data as $client) {

            $client_array[] = array(
                'Name' => $client->name,
                'Username' => $client->username,
                'phone' => $client->phone,
                'email' => $client->email,
                'created At' => $client->created_at
            );
        }
        Excel::create('Clients', function ($excel) use ($client_array) {
            $excel->setTitle('Clients');
            $excel->sheet('Clients', function ($sheet) use ($client_array) {
                $sheet->fromArray($client_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    public function changeStatus(Request $request)
    {
        $client = User::findOrFail($request->id);
        if($client->active == 1){
            $client->active = 0;
            $label = 'danger';
            $text = 'Not active';
        }else{
            $client->active = 1;
            $label = 'success';
            $text = 'Active';
        }

        $client->save();
        return response()->json(['label' => $label, 'text' => $text], 200);
    }

    public function notificationView()
    {
        return view('dashboard.clients.notifi');
    }

    public function broadCastNotification(Request $request)
    {
        $this->validate($request, [
            'msg' => 'required'
        ]);

        $notification = [
            'title' => 'Snow Energy',
            'body' => $request->msg,
            'type' => null,
            'click_action' => null,
            'target_id' => null
        ];

        $fcm = new FCM();
        $fcm->broadCastNotification($notification, '/topics/snowClients');

        session()->flash('success', 'Notification sent');
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->orders()->delete();
        $user->delete();

        return response()->json('client deleted', 200);
    }
}
