<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('dashboard.companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name.*' => 'required|string',
            'image' => 'required|image'
        ]);

        $c = new Company();

        foreach ($this->locales as $loc => $val){
            $c->{'name:'.$loc} = $request->name[$loc];
        }

        try{
            $img = $request->file('image');
            $imgName = $img->getClientOriginalName();
            $img->move(public_path().'/uploads/companies/', $imgName);
            $c->image = 'public/uploads/companies/'.$imgName;
        }catch (\Exception $exception){

        }



        $c->save();

        session()->flash('success', 'company.created');
        return redirect()->back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);
        return view('dashboard.companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name.*' => 'required|string',
        ]);

        $c = Company::findOrFail($id);

        foreach ($this->locales as $loc => $val){
            $c->{'name:'.$loc} = $request->name[$loc];
        }

        try{
            if($request->image != null){

                $image = $request->file('image');
                $PageimgName = $c->image;
                $imageName = $image->getClientOriginalName();
                if ($imageName != $PageimgName){
                    if (file_exists(public_path().'/uploads/companies/'.$c->image))
                        unlink(public_path().'/uploads/companies/'.$c->image);
                }
                $image->move(public_path().'/uploads/companies/', $imageName);
                $c->image = 'public/uploads/companies/'.$imageName;
            }
        }catch (\Exception $exception)
        {

        }

        $c->active = $request->status;
        $c->save();

        session()->flash('success', 'company.updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
//        dd($request->id);
        $c = Company::findOrFail($request->id);
        $c->products()->delete();
        $c->delete();

       return response()->json('done', 200);
    }

}
