<?php

namespace App\Http\Controllers;

use App\DataTables\DelegateDatatable;
use App\Http\Controllers\Helpers\FCM;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DelegateController extends Controller
{

    public function index()
    {
        $delegates = User::Delegate()->get();
        return view('dashboard.delegates.index', compact('delegates'));
//        return $datatable->render('dashboard.delegates.index');
    }

    public function getDelegates()
    {
        $users = User::all();
//        $del =  new DelegateDatatable();
        return DataTables::of($users)
            ->addColumn('id', function ($users){
                return $users->id;
            })
            ->addColumn('name', function ($users){
                return $users->name;
            })
            ->addColumn('username', function ($users){
                return $users->username;
            })
            ->addColumn('image', function ($users){
                return "<a href=".asset('public/uploads/delegates/'.$users->image)." data-fancybox>" . "<img src=".asset('public/uploads/delegates/'.$users->image)." />";
            })
            ->rawColumns(['name'])
            ->make(true);
//        return $del->dataTable(User::all())
//            ->only(['id', 'name', 'username', 'phone', 'image', 'email'])
//            ->addColumn('actions', '<a href="#">Html Column</a>')
////            ->addColumn('action', 'dashboard.delegates.view')
////            ->rawColumns(['link', 'action'])
//            ->make(true);
    }


    public function create()
    {
        return view('dashboard.delegates.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'username' => 'unique:users|required',
            'phone' => 'unique:users|string|min:9',
            'email' => 'unique:users|email',
            'image' => 'required|image',
            'password' => 'required|string|min:6',
        ]);



        $d = new User();
        $d->name = $request->name;
        $d->username = $request->username;
        $d->phone = $request->phone;
        $d->email = $request->email;
        $d->password = Hash::make($request->password);
        $d->type = 2;
        $d->active = 1;

        try{
            $image = $request->file('image');
            $imageName = time().'-'.$image->getClientOriginalName();
            $image->move(public_path().'/uploads/delegates/', $imageName);
            $d->image = '/public/uploads/delegates/'.$imageName;

        }catch (\Exception $exception){}


        $d->save();
        session()->flash('success', 'product.created');
        return redirect()->back();
    }

    public function show($id)
    {

        $delegate = User::findOrFail($id);
        $orders = Order::where('delegate_id', $delegate->id)->get();
        $location = DB::table('delegate_trips')->where('delegate_id', $id)->latest()->first();
//        dd($location);
        return view('dashboard.delegates.view', compact('delegate', 'orders', 'location'));
    }



    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'min:6|nullable',
        ]);


        $d = User::findOrFail($id);
        $d->name = $request->name;
        if ($request->username == $d->username)
            unset($request->username);
        else
            $d->username = $request->username;

        if ($request->phone == $d->phone)
            unset($request->phone);
        else
            $d->phone = $request->phone;

        if ($request->email == $d->email)
            unset($request->email);
        else
            $d->email = $request->email;

        if($request->has('passCheck')){
            if($request->new_password != null)
                $d->password = Hash::make($request->password);
        }

        try{
            if($request->image != null){

                $image = $request->file('image');
                $imageName = $image->getClientOriginalName();
                    if (file_exists(public_path().'/uploads/delegates/'.$d->image))
                        unlink(public_path().'/uploads/delegates/'.$d->image);
                $image->move(public_path().'/uploads/delegates/', $imageName);
                $d->image = 'public/uploads/delegates/'.$imageName;
            }
        }catch (\Exception $exception)
        {

        }

        $d->active = $request->status;

        $d->save();
        session()->flash('success', 'delegate.updated');
        return redirect()->back();
    }

    public function getLocation(Request $request)
    {
        $locations = DB::table('delegate_trips')->where('delegate_id', $request->delegate_id)->get()->toArray();
        $location = end($locations);
        $count = DB::table('delegate_trips')->where('delegate_id', $request->delegate_id)->count();
        if ($count > 20)
            DB::table('delegate_trips')->where('delegate_id', $request->delegate_id)->delete();

        return response()->json(['data' => $location], 200);
    }


    public function destroy($id)
    {
        $d = User::findOrFail($id);
        $d->delete();
        session()->flash('success', 'delegate.deleted');
        return redirect()->back();

    }

    public function delegateOrders(Request $request)
    {
        $orders = Order::where('delegate_id', $request->id)->with(['user'])->get();
        return response()->json(['data' => $orders], 200);
    }

    public function notificationView()
    {
        return view('dashboard.delegates.notifi');
    }

    public function broadCastNotification(Request $request)
    {
        $this->validate($request, [
            'msg' => 'required'
        ]);

        $notification = [
            'title' => 'Snow Energy',
            'body' => $request->msg,
            'type' => null,
            'click_action' => null,
            'target_id' => null
        ];

        $fcm = new FCM();
        $fcm->broadCastNotification($notification, '/topics/snowDelegates');

        session()->flash('success', 'Notification sent');
        return redirect()->back();
    }
}
