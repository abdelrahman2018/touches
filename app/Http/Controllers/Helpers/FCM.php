<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 5/8/2019
 * Time: 1:30 PM
 */

namespace App\Http\Controllers\Helpers;


use PHPUnit\Framework\Exception;

class FCM
{
    private $noti_url;
    private $key;

    public function __construct()
    {
        $this->noti_url = 'https://fcm.googleapis.com/fcm/send';
        $this->key = env('FIREBASE_KEY');
    }


    public function broadCastNotification($notification, $topic)
    {
        $this->topic = $topic;
        $message = [
            'title' => $notification['title'],
            'body' => $notification['body'],
            'click_action' => $notification['click_action']? $notification['click_action']: '',
            'icon' => 'myicon',
            'sound' => 'mySound',

        ];
        $data = [

            'type' => $notification['type']? $notification['type']: '',
            "id" => $notification['target_id']? $notification['target_id']:''

        ];
        $fields = json_encode([
            'to' => $topic,
            'notification' => $message,
            'data' => $data
        ]);
//        dd(env('FIREBASE_KEY'));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->noti_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: key=' . $this->key, 'Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);
        if ($result == false) {
            throw new Exception(curl_error($ch), curl_errno($ch));
        }
//        dd($result);
        curl_close($ch);
    }
}