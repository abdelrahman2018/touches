<?php

namespace App\Http\Controllers\Helpers;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderHelper extends Controller
{
    public function delegateOrderStatusName(Request $request)
    {
        $status = '';
        $status_codes = [
            '1' => 'بدا تنفيذ الطلب',
            '2' => 'بدا التوصيل',
            '3' => 'وصل المندوب',
            '4' => 'تم التسليم',
        ];
        $order = Order::findOrFail($request->id);

        foreach ($status_codes as $key => $val){
            if($order->status_code == $key)
                $status = $val;
        }
        return response()->json(['res' => $status], 200);
    }
}
