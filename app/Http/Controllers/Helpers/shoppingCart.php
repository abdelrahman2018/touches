<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 4/4/2019
 * Time: 10:42 AM
 */

namespace App\Http\Controllers\Helpers;


use Illuminate\Support\Facades\DB;

class shoppingCart
{
    public static function wishlistToDB($contents)
    {
        $wishContents = serialize($contents);
        $userCarts = DB::table('user_carts')->where('customer_id', auth('customer')->user()->id)->first();

        if ($userCarts){
            DB::table('user_carts')->where('customer_id', auth('customer')->user()->id)
                ->update(['wishlist' => $wishContents]);
        }else{
            DB::table('user_carts')->insert(
                ['wishlist' => $wishContents, 'customer_id' => auth('customer')->user()->id]
            );
        }

        return $contents;
    }

    public static function wishlistFromDB()
    {
        $userCarts = DB::table('user_carts')->where('customer_id', auth('customer')->user()->id)->first();
        if($userCarts){
            $contents = unserialize($userCarts->wishlist);
//            dd($contents);
            if($contents)
                return $contents;
            else
                return [];
        }

        return [];
    }



    public static function shoppingCartFromDB($user)
    {
        $userCarts = DB::table('user_carts')->where('customer_id', $user->id)->first();

        if($userCarts){
            $contents = json_decode($userCarts->shopping);
            if($contents)
                return $contents;
            else
                return [];
        }

        return [];
    }



    public static function shoppingCartToDB($contents, $user)
    {
        $cartContents = json_encode($contents);
        $userCarts = DB::table('user_carts')->where('customer_id', $user->id)->first();

        if($userCarts){
            DB::table('user_carts')->where('customer_id', $user->id)
                ->update(['shopping' => $cartContents]);
        }else{
            DB::table('user_carts')->insert(
                ['shopping' => $cartContents, 'customer_id' => $user->id]
            );
        }

        return $contents;
    }


    public static function shoppingCountFromDB($user)
    {
        $contents = self::shoppingCartFromDB($user);
        if ($contents)
            return count($contents);
        return 0;
    }


    public static function shoppingSubtotal($user)
    {
        $total = 0;
        $contents = self::shoppingCartFromDB($user);
//        dd($contents);
        if(!empty($contents)){
            for ($i = 0; $i < count($contents); $i++){
                $price = $contents[$i]['options']['discount'];
                $total += $price * $contents[$i]['qty'];
            }
        }

        return $total;
    }

}