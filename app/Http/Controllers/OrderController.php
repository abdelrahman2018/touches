<?php

namespace App\Http\Controllers;

use App\Notifications\FirebaseNotification;
use App\Order;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class OrderController extends Controller

{
    public $orderStatus;
    public $orderPayment;
    public $sett = array();
    public function __construct()
    {
        $this->orderStatus = [
            0 => [
                'label' => 'danger',
                'text' => _lang('app.new_order')
            ],

            1 => [
                'label' => 'info',
                'text' => _lang('app.order_assigned')
            ],

            2 => [
                'label' => 'primary',
                'text' => _lang('app.excution_start')
            ],

            3 => [
                'label' => 'warning',
                'text' => _lang('app.on_my_way')
            ],

            4 => [
                'label' => 'success',
                'text' => _lang('app.arrived')
            ],

            5 => [
                'label' => 'success',
                'text' => _lang('app.order_delivered')
            ]

        ];

        $this->orderPayment = [
            0 => 'Cash',
            1 => 'Mada Card'
        ];


        $settings = Setting::all();
        foreach ($settings as $row){
            $this->sett = array_add($this->sett, $row->key, $row->value);
        }
//
    }


    public function index()
    {
        $orders = Order::where('status_code', 0)->get();
        $delegates = User::Delegate()->where('active', 1)->get();
        return view('dashboard.orders.index', compact('orders', 'delegates'));
    }

    public function assign(Request $request)
    {
        if($request->has('orders') && $request->del_id != null){
            $orders = Order::WhereIn('id', $request->orders)->get();
            foreach ($orders as $order){
                $data = [
                    'click_action' => 'order',
                    'target_id' => $order->id
                ];

                $order->delegate_id = $request->del_id;
                $order->status_code = 1;
                $order->save();

                $order->user->notify(new FirebaseNotification('Snow Energy', 'Your Order Status Changed', $data));

            }
        }else
            return response()->json('', 400);
    }

    public function ordersIndexAjax(Request $request)
    {

//        dd($request->all());
        $orders = Order::with('user');

        if ($request->client != null){
            $ids = User::Client()->where('name', 'LIKE', '%'.$request->client.'%')->get()->pluck('id');
            $orders->whereIn('user_id', $ids);
        }
        if ($request->status != null)
            $orders->where('status_code', $request->status);
        if ($request->from != null)
            $orders->where('created_at', '>=', $request->from);
        if ($request->to != null)
        {
            $to = new Carbon($request->to);
            $to = $to->addDay();
            $to = $to->toDateString();
            $orders->where('created_at', '<=', $to);
        }
//        dd($orders);
        return Datatables::of($orders->orderBy('created_at')->get())
            ->editColumn('address', function ($orders){
                $addrr = json_decode($orders->address);
                return '<strong>City: </strong>'. $addrr->city. '<br>'.
                    '<strong>Area: </strong>'. $addrr->area. '<br>'.
                    '<strong>Street: </strong>'. $addrr->street. '<br>'.
                    '<strong>building: </strong>'. $addrr->building. '<br>'.
                    '<strong>floor: </strong>'. $addrr->floor. '<br>';
            })
            ->addColumn('select', function ($orders){
                if($orders->status_code == 0)
                    return '<input type="checkbox" value="'.$orders->id.'" name="order_id">';
                return '';
            })
            ->addColumn('user', function ($orders){
                return $orders->user->name;
            })
            ->addColumn('status', function ($orders){
                return '<span class="label label-'.$this->orderStatus[$orders->status_code]['label'].'" >'.$this->orderStatus[$orders->status_code]['text'].'</span>';
            })
            ->editColumn('payment_type', function ($orders){
                return $this->orderPayment[$orders->payment_type];
            })
            ->addColumn('order_total', function ($orders){
                $tot = $orders->total;
                return '<span class="label label-success">'.$tot.'</span>';
            })
            ->editColumn('coupon_id', function ($orders){
                if ($orders->coupon)
                    return $orders->coupon->value.' %';
                return 0 .' %';
            })
            ->editColumn('total', function ($orders){
                $t = $orders->total +  $this->sett['delivery_charge'];
                return '<span class="label label-success">'.$t.'</span>';
            })
            ->make(true);
    }

    public function statusSearch(Request $request)
    {
        $del_id = $request->del_id;
        $status_code = $request->status_code;

        $orders = Order::where('delegate_id', $request->del_id)->where('status_code', $request->status_code)->get();
        return view('dashboard.orders.search_status', compact('orders', 'del_id', 'status_code'));
    }

    public function statusSearchExport($del_id, $status)
    {
        $orderStatus = [
            0 => [
                'label' => 'danger',
                'text' => _lang('app.new_order')
            ],

            1 => [
                'label' => 'info',
                'text' => _lang('app.order_assigned')
            ],

            2 => [
                'label' => 'primary',
                'text' => _lang('app.excution_start')
            ],

            3 => [
                'label' => 'warning',
                'text' => _lang('app.on_my_way')
            ],

            4 => [
                'label' => 'success',
                'text' => _lang('app.arrived')
            ],

            5 => [
                'label' => 'success',
                'text' => _lang('app.order_delivered')
            ]

        ];


        $orderPayment = [
            0 => 'Cash',
            1 => 'Mada Card'
        ];


        $order_data = Order::where('delegate_id', $del_id)->where('status_code', $status)->get();
        $order_array[] = array('order date', 'Address', 'client', 'status', 'Payment Method', 'total', 'discount', 'created At');
        foreach ($order_data as $order) {
            $address = json_decode($order->address);

            $order_array[] = array(
                'order date' => $order->date,
                'Address' => 'city: '.$address->city.' - area: '.$address->area.' - street: '.$address->street.' - building: '.$address->building.' - floor: '.$address->floor,
                'client' => $order->user->name,
                'status' => $orderStatus[$order->status_code]['text'],
                'Payment Method' => $orderPayment[$order->payment_type],
                'total' => $order->total,
                'discount' => $order->discount ? $order->discount->val : 0 . ' %',
                'created At' => $order->created_at,
            );
        }
        Excel::create('delegate orders', function ($excel) use ($order_array) {
            $excel->setTitle('delegate orders');
            $excel->sheet('delegate orders', function ($sheet) use ($order_array) {
                $sheet->fromArray($order_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }
}

