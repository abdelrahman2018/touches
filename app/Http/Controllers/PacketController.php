<?php

namespace App\Http\Controllers;

use App\Packet;
use Illuminate\Http\Request;

class PacketController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packets = Packet::all();
        return view('dashboard.packets.index', compact('packets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.packets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name.*' => 'required|string',
           'min_qty' => 'required|numeric'
        ]);

        $p = new Packet();
        $p->min_quantity = $request->min_qty;

        foreach ($this->locales as $loc => $val){
            $p->{'name:'.$loc} = $request->name[$loc];
        }

        $p->save();

        session()->flash('success', 'packet.created');
        return redirect()->back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $packet = Packet::findOrFail($id);
        return view('dashboard.packets.edit', compact('packet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name.*' => 'required|string',
            'min_qty' => 'required|numeric'
        ]);

        $p = Packet::findOrFail($id);
        $p->min_quantity = $request->min_qty;
        $p->active = $request->status;

        foreach ($this->locales as $loc => $val){
            $p->{'name:'.$loc} = $request->name[$loc];
        }

        $p->save();

        session()->flash('success', 'packet.updated');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
