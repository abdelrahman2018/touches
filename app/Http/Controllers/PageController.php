<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        return view('dashboard.pages.index')->with('pages', $pages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required|string',
            'name_ar' => 'required|string',
            'desc_en' => 'required|string',
            'desc_ar' => 'required|string',
            'image' => 'required|image',
        ]);

        $page = new Page();
        $page->{'title:ar'} = $request->name_ar;
        $page->{'title:en'} = $request->name_en;
        $page->{'description:en'} = $request->desc_en;
        $page->{'description:ar'} = $request->desc_ar;

        try{
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->move(public_path().'/uploads/pages/', $imageName);
            $page->image = '/uploads/pages/'.$imageName;
        }catch (\Exception $exception)
        {

        }

        $page->save();

        session()->flash('success', 'page.created');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $page = Page::findOrFail($id);
        return view('dashboard.pages.edit')->with('page', $page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        $this->validate($request, [
            'name_en' => 'required|string',
            'name_ar' => 'required|string',
            'desc_en' => 'required|string',
            'desc_ar' => 'required|string',
        ]);

        $page->{'title:ar'} = $request->name_ar;
        $page->{'title:en'} = $request->name_en;
        $page->{'description:en'} = $request->desc_en;
        $page->{'description:ar'} = $request->desc_ar;

        try{
            if($request->image != null){

                $image = $request->file('image');
                $PageimgName = $page->image;
                $imageName = $image->getClientOriginalName();
                if ($imageName != $PageimgName){
                    if (file_exists(public_path().'/uploads/pages/'.$page->image))
                        unlink(public_path().'/uploads/pages/'.$page->image);
                }
                $image->move(public_path().'/uploads/pages/', $imageName);
                $page->image = $imageName;
            }
        }catch (\Exception $exception)
        {

        }

        $page->save();

        session()->flash('success', 'page.updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        if(file_exists(public_path().'/uploads/pages/'.$page->image))
            unlink(public_path().'/uploads/pages/'.$page->image);

        $page->delete();
        session()->flash('success', 'page.deleted');
        return redirect()->back();

    }
}
