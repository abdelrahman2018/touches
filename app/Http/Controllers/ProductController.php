<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Controllers\Helpers\FCM;
use App\Packet;
use App\Product;
use App\Unit;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('dashboard.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $units = Unit::Active()->get();
        $packets = Packet::Active()->get();
        $companies = Company::Active()->get();

        return view('dashboard.products.create', compact('units', 'packets', 'companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'price' => 'required|numeric',
            'name.*' => 'required|string',
            'desc.*' => 'required',
            'image' => 'required|image',
            'size' => 'required',
            'company' => 'required',
            'unit' => 'required',
            'packet' => 'required',
        ]);

        $p = new Product();
        foreach ($this->locales as $loc => $val){
            $p->{'name:'.$loc} = $request->name[$loc];
            $p->{'description:'.$loc} = $request->desc[$loc];
        }

        $p->size = $request->size;
        $p->unit_id = $request->unit;
        $p->packet_id = $request->packet;
        $p->company_id = $request->company;
        $p->price = $request->price;

        if($request->discount)
            $p->discount_price = $request->discount;

        try{
            $image = $request->file('image');
            $imageName = time().'-'.$image->getClientOriginalName();
            $image->move(public_path().'/uploads/products/', $imageName);
            $p->image = 'public/uploads/products/'.$imageName;

        }catch (\Exception $exception){}

        $p->save();

        $notification = [
            'title' => 'Snow Energy',
            'body' => 'Explore Our New Product '. $p->name,
            'type' => null,
            'click_action' => 'product',
            'target_id' => $p->id
        ];

        $fcm = new FCM();
        $fcm->broadCastNotification($notification, '/topics/snowClients');

        session()->flash('success', 'product.created');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $units = Unit::Active()->get();
        $packets = Packet::Active()->get();
        $companies = Company::Active()->get();

        return view('dashboard.products.edit', compact('units', 'packets', 'companies', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $this->validate($request, [
            'price' => 'required|numeric',
            'name.*' => 'required|string',
            'desc.*' => 'required',
            'size' => 'required',
        ]);

        $p = Product::findOrFail($id);
        foreach ($this->locales as $loc => $val){
            $p->{'name:'.$loc} = $request->name[$loc];
            $p->{'description:'.$loc} = $request->desc[$loc];
        }

        $p->size = $request->size;
        $p->unit_id = $request->unit;
        $p->packet_id = $request->packet;
        $p->company_id = $request->company;
        $p->discount_price = $request->discount;
        $p->price = $request->price;
        $p->active = $request->status;


        try{
            if($request->image != null){

                $image = $request->file('image');
                $PageimgName = $p->image;
                $imageName = $image->getClientOriginalName();
                if ($imageName != $PageimgName){
                    if (file_exists(public_path().'/uploads/products/'.$p->image))
                        unlink(public_path().'/uploads/products/'.$p->image);
                }
                $image->move(public_path().'/uploads/products/', $imageName);
                $p->image = $imageName;
            }
        }catch (\Exception $exception)
        {

        }

        $p->save();

        session()->flash('success', 'product.updated');
        return redirect()->back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
