<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('dashboard.roles.index', compact('roles'));
    }

    public function create()
    {
        $permissions = Permission::all();
        return view('dashboard.roles.create', compact('permissions'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $role = Role::create(['name' => $request->name]);
        $role->syncPermissions($request->permissions);
        $role->save();

        session()->flash('success', 'Role created');
        return redirect()->back();
    }

    public function edit($id)
    {
        $role = Role::findById($id);
        $permissions = Permission::all();
        return view('dashboard.roles.edit', compact('role', 'permissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $role = Role::findById($id);
        $role->name = $request->name;
        $role->syncPermissions($request->permissions);
        $role->save();

        session()->flash('success', 'Role updated');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $role = Role::findById($id);
        $role->delete();

        session()->flash('success', 'Role deleted');
        return redirect()->back();
    }


}
