<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{


    public function edit()
    {
        $locale = app()->getLocale();
        $settings = Setting::all();
//        dd($settings);
        $sett_arr = array();
        foreach ($settings as $setting){
            $sett_arr = array_add($sett_arr, $setting->key, $setting->value);
        }
        return view('dashboard.settings.index')
            ->with('locale', $locale)
            ->with('settings', $sett_arr);
    }

    public function update(Request $request)
    {
//        dd($request->all());
        $requests = $request->only(['facebook', 'google', 'twitter', 'instagram', 'youtube', 'email', 'phone', 'address_ar', 'linkedin',
            'address_en', 'lat', 'lng']);

        foreach ($requests as $key => $value) {
            DB::table('settings')
                ->where('key', $key)
                ->update(['value' => $value]);
        }

        session()->flash('success', 'you have updated settings success');
        return redirect(route('settings.edit'));
    }
}
