<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::all();
        return view('dashboard.slides.index')->with('slides', $slides);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image'
        ]);

        $slide = new Slide();

        try{
            $image = $request->file('image');
            $imageName = time().$image->getClientOriginalName();
            $image->move(public_path().'/uploads/slides/', $imageName);
            $slide->image = $imageName;
        }catch (\Exception $exception)
        {

        }

        $slide->save();

        session()->flash('success', 'slide.created');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::findOrFail($id);
        return view('dashboard.slides.edit')->with('slide', $slide);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $slide = Slide::findOrFail($id);


        try{
            if($request->image != null){

                $image = $request->file('image');
                $SlideimgName = $slide->image;
                $imageName = $image->getClientOriginalName();
                if ($imageName != $SlideimgName){
                    if (file_exists(public_path().'/uploads/slides/'.$slide->image))
                        unlink(public_path().$slide->image);
                }
                $image->move(public_path().'/uploads/slides/', $imageName);
                $slide->image = $imageName;
            }
        }catch (\Exception $exception)
        {

        }

        $slide->save();

        session()->flash('success', 'slide.updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::findOrFail($id);
        if(file_exists(public_path().$slide->image))
            unlink(public_path().$slide->image);

        $slide->delete();
        session()->flash('success', 'slide.deleted');
        return redirect()->back();
    }
}
