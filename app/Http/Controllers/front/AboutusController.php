<?php

namespace App\Http\Controllers\front;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutusController extends Controller
{
    public function view()
    {
        $about = Page::findOrFail(1);
        return view('front.about', compact('about'));
    }

}
