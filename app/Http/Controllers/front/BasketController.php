<?php

namespace App\Http\Controllers\front;

use App\Game;
use App\Http\Controllers\Helpers\shoppingCart;
use App\Service;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BasketController extends Controller
{
    public function index()
    {
//        dd(Cart::content());
//        dd(shoppingCart::wishlistFromDB());
        return view('front.basket');
    }

    public function addToBasket(Request $request)
    {
        $service_id = $request->id;
        $service = Service::findOrFail($service_id);

        $chek = 0;

        foreach (Cart::content() as $item)
        {
            if($item->id == $service_id){
                $chek = 1;
                $msg = 'game already exist in basket';
            }
        }

        if ($chek == 0){
            Cart::add([
                'id' => $service->id,
                'name' => $service->name,
                'price' => 0,
                'qty' => 1,
                'options' => ['image' => $service->image]
            ]);

            $msg = 'Game added to basket';
        }

        if(auth('customer')->check()){
            shoppingCart::wishlistToDB(Cart::content());
        }
        return response()->json($msg, 200);
    }

    public function remove($rowId)
    {
        Cart::remove($rowId);

        if(auth('customer')->check()){
            shoppingCart::wishlistToDB(Cart::content());
        }
        return redirect()->back();
    }
}
