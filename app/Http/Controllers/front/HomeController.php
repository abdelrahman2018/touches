<?php

namespace App\Http\Controllers\front;

use App\Advertisment;
use App\Album;
use App\Category;
use App\Event;
use App\Game;
use App\Http\Controllers\BackendBaseController;
use App\News;
use App\Offer;
use App\Page;
use App\Product;
use App\Slide;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{

    public function index()
    {
//        dd(auth('customer')->user());
        $slides = Slide::all();
        $news = News::all();
        $album = Album::where('show_home', 1)->first();
        $events = Event::all();
        return view('front.home', compact('slides', 'news', 'album', 'events'));
    }

}
