<?php

namespace App\Http\Middleware\Api;

use Closure;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $loc = $request->header('lang', config('locale'));
        app()->setLocale($loc);
        return $next($request);
    }
}
