<?php

namespace App\Http\Middleware\Api;

use Closure;
use Illuminate\Support\Facades\DB;

class RefreshTokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $refresh_token = $request->refresh_token;
//        dd($refresh_token);
        $token_exp = DB::table('oauth_refresh_tokens')->where('access_token_id', $refresh_token)->first()->expires_at;

        if($token_exp < now())
            return response()->json(['msg' => 'refresh token expired'], 400);

        return $next($request);
    }
}
