<?php

namespace App\Http\Middleware\Api;

use Closure;

class TokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->token()->expires_at < now())
            return response()->json(['msg' => 'your token is expired'], 400);

        return $next($request);
    }
}
