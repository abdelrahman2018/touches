<?php

namespace App\Http\Middleware;

use Closure;

class FrontLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (session()->has('frontend-lang'))
            app()->setLocale(session()->get('frontend-lang'));
        else
            app()->setLocale(config('app.locale'));
        return $next($request);
    }
}
