<?php

namespace App\Http\Middleware;

use Closure;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->has('backend-lang'))
            app()->setLocale(session()->get('backend-lang'));
        else
            app()->setLocale(config('app.locale'));

        return $next($request);
    }
}
