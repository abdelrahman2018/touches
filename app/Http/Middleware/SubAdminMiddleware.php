<?php

namespace App\Http\Middleware;

use Closure;

class SubAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->user()->hasPermissionTo('Administer Roles & Permissions'))
//            dd(33);
            return $next($request);

        if ($request->segment(3) == 'slides'){
            if (!auth()->user()->hasPermissionTo('slides'))
                 abort(401);
        }

        if ($request->segment(3) == 'Coupons'){
            if (!auth()->user()->hasPermissionTo('Coupons'))
                abort(401);
        }


//        if ($request->segment(3) == 'packets'){
//            if (!auth()->user()->hasPermissionTo('Packets'))
//                abort(401);
//        }

        if ($request->segment(3) == 'packets'){
            if (!auth()->user()->hasPermissionTo('Packets'))
                abort(401);
        }

        return $next($request);
    }
}
