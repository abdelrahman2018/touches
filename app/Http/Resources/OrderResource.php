<?php

namespace App\Http\Resources;

use App\Rate;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'id' => $this->id,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'date' => $this->date,
            'address' => json_decode($this->address),
            'payment_type' => $this->payment_type,
            'status_code' => $this->status_code,
            'client' => $this->user,
//            'rate' => $this->rate? $this->rate->rate:null,
//            'comment' => $this->comment? $this->comment->comment:null,
//            'products' => ProductResource::collection($this->products),
//            'cart' => json_decode($this->cart),
            'coupon' => $this->coupon,
            'total' => $this->total,
            'created_at' => $this->created_at,
        ];

    }
}
