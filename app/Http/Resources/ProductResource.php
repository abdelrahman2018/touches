<?php

namespace App\Http\Resources;

use App\Comment;
use App\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'name' => $this->name,
          'description' => strip_tags(html_entity_decode($this->description)),
          'discount_price' => $this->discount_price,
          'price' => $this->price,
          'unit' => $this->unit->name,
          'packet' => $this->packet->name,
          'min_quantity' => $this->packet->min_quantity,
          'company' => $this->company,
          'size' => $this->size,
          'image' => $this->image,
//            'comments' => $this->comments,
            'rates' => $this->rates
        ];
    }
}
