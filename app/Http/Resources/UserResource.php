<?php

namespace App\Http\Resources;

use App\Rate;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $total = 0;
        $rates = Rate::where('rateable_id', $this->id)->get();

        foreach ($rates as $rate){
            $total+= $rate->rate;
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'phone' => $this->phone,
            'image' => $this->image,
            'email' => $this->email,
            'device_token' => $this->device_token,
            'rate' => $this->when($this->type, 2)
//            'orders' => $this->orders,
//            'comments' => $this->comments,
            ];
    }
}
