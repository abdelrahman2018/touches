<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FirebaseNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    private $title;
    private $body;
    private $data;

    public function __construct($title, $body, $data = null)
    {
        $this->title = $title;
        $this->body = $body;
        $this->data = $data;
    }


    public function via($notifiable)
    {
        return ['firebase'];
    }


    public function toFirebase($notifiable)
    {
        return (new \Liliom\Firebase\FirebaseMessage)
            ->notification([
                'title' => $this->title,
                'body' => $this->body,
                'sound' => '', // Optional
                'icon' => '', // Optional
                'click_action' => $this->data['click_action']
            ])
            ->setData([
                'target_id' => $this->data['target_id']
            ])
            ->setPriority('high'); // Default is 'normal'
    }



    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
