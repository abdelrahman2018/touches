<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }


    public function comment()
    {
        return $this->hasOne(Comment::class);
    }

    public function rate()
    {
        return $this->hasOne(Rate::class);
    }


}
