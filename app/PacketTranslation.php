<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PacketTranslation extends Model
{
    protected $table = 'packets_translations';
    protected $fillable = ['name'];
    public $timestamps = false;
}
