<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use Translatable;

    static protected $unguarded = true;
    public $translatedAttributes = ['title', 'description'];
}
