<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    protected $table = 'pages_translations';
    protected $fillable = ['name', 'description'];
    public $timestamps = false;
}
