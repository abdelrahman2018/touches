<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use Translatable, SoftDeletes;

    static protected $unguarded = true;
    public $translatedAttributes = ['name', 'description'];


//    protected $appends = ['rate'];
    


    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function packet()
    {
        return $this->belongsTo(Packet::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function rates()
    {
        return $this->hasMany(Rate::class, 'rateable_id', 'id');
    }


}
