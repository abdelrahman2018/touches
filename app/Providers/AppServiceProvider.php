<?php

namespace App\Providers;

use App\Category;
use App\Page;
use App\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
//
////
        $locale = app()->getLocale();
        $locs = config('translatable.locales');
//////
        $settings = Setting::all();
        $sett = array();
        foreach ($settings as $row){
            $sett = array_add($sett, $row->key, $row->value);
        }

        $orderStatus = [
            0 => [
                'label' => 'danger',
                'text' => _lang('app.new_order')
            ],

            1 => [
                'label' => 'info',
                'text' => _lang('app.order_assigned')
            ],

            2 => [
                'label' => 'primary',
                'text' => _lang('app.excution_start')
            ],

            3 => [
                'label' => 'warning',
                'text' => _lang('app.on_my_way')
            ],

            4 => [
                'label' => 'success',
                'text' => _lang('app.arrived')
            ],

            5 => [
                'label' => 'success',
                'text' => _lang('app.order_delivered')
            ]

        ];

        $orderPayment = [
            0 => 'Cash',
            1 => 'Mada Card'
        ];
//
        view()->share('settings', $sett);
        View::share('locale', $locale);
        View::share('locales', $locs);
        View::share('orderStatus', $orderStatus);
        View::share('orderPayment', $orderPayment);

//        dd($locs);
    }
}
