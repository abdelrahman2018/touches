<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class, 'id', 'rateable_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'rateable_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
