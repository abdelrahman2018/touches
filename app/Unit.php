<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use Translatable;

    static protected $unguarded = true;
    public $translatedAttributes = ['name'];


    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
