<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitTranslation extends Model
{
    protected $table = 'units_translations';
    protected $fillable = ['name'];
    public $timestamps = false;
}
