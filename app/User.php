<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


   protected static $unguarded = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function scopeDelegate($query)
    {
        return $query->where('type', 2);
    }

    public function scopeClient($query)
    {
        return $query->where('type', 1);
    }

    public function scopeAdmin($query)
    {
        return $query->where('type', 3);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }


    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function rates()
    {
        return $this->hasMany(Rate::class, 'rateable_id', 'id');
    }

    public function routeNotificationForMail()
    {
        return $this->email;
    }

    public function tokens()
    {
        return $this->hasMany(Token::class);
    }

    public function routeNotificationForFirebase()
    {
//        $tokensArr = array();
//        foreach ($this->tokens as $token){
//            array_push($tokensArr, $token->device_token);
//        }
//
//        return $tokensArr;
        return $this->device_token;
    }

}
