<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    $locales = config('translatable.locales');

    $res = array();
    $res['image'] = $faker->imageUrl();

    foreach ($locales as $loc => $val){
        app()->setLocale($loc);
        $res['name:'.$loc] = $faker->company;
    }
    return $res;
});
