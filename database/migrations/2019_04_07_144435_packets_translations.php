<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PacketsTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packets_translations', function (Blueprint $table){
            $table->increments('id');
            $table->integer('packet_id')->unsigned();
            $table->string('name');
            $table->string('locale')->index();

            $table->unique(['packet_id','locale']);
            $table->foreign('packet_id')->references('id')->on('packets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packets_translations');
    }
}
