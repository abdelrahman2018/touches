<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->double('total');
            $table->string('lat');
            $table->string('lng');
            $table->string('payment_type')->default('cash');
            $table->text('address');
            $table->integer('status_code')->default(0);
            $table->integer('user_id');
            $table->integer('delegate_id');
            $table->integer('delegate_id');
            $table->integer('coupon_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
