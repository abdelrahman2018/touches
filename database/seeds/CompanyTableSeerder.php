<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class CompanyTableSeerder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = config('translatable.locales');

        $c = new \App\Company();
        $c->image = 'public/uploads/companies/neslte.jpg';
        $c->save();


        foreach ($locales as $loc => $val){
            $cTranslation = new \App\CompanyTranslation();
            $cTranslation->company_id = $c->id;
            $cTranslation->name = 'company 1';
            $cTranslation->locale = $loc;
            $cTranslation->save();
        }



        $c = new \App\Company();
        $c->image = 'public/uploads/companies/evan.jpg';
        $c->save();


        foreach ($locales as $loc => $val){
            $cTranslation = new \App\CompanyTranslation();
            $cTranslation->company_id = $c->id;
            $cTranslation->name = 'company 2';
            $cTranslation->locale = $loc;
            $cTranslation->save();
        }

        $c = new \App\Company();
        $c->image = 'public/uploads/companies/aqua.jpg';
        $c->save();


        foreach ($locales as $loc => $val){
            $cTranslation = new \App\CompanyTranslation();
            $cTranslation->company_id = $c->id;
            $cTranslation->name = 'company 3';
            $cTranslation->locale = $loc;
            $cTranslation->save();
        }

    }
}
