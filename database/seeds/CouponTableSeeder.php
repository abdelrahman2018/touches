<?php

use Illuminate\Database\Seeder;

class CouponTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c = new \App\Coupon();
        $c->number = '34343r';
        $c->exp = now()->addDay();
        $c->value = 5;
        $c->save();



        $c = new \App\Coupon();
        $c->number = 'eytyet';
        $c->exp = now()->addDay(2);
        $c->value = 10;
        $c->save();
    }
}
