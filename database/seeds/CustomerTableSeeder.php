<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c = new \App\Customer();
        $c->email = 'abdo@gmail.com';
        $c->password = bcrypt('12345678');
        $c->save();

        $c1 = new \App\Customer();
        $c1->email = 'boody@gmail.com';
        $c1->password = bcrypt('12345678');
        $c1->save();
    }
}
