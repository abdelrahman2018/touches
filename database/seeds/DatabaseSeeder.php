<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
//        $this->call(SettingsTableSeeder::class);
//        $this->call(PagesTableSeeder::class);
//        $this->call(ContactTableSeeder::class);
//        $this->call(Order_nowTableSeeder::class);
//        $this->call(OrderTableSeeder::class);
//        $this->call(CustomerTableSeeder::class);
//        factory(\App\Company::class, 2)->create();

//        $this->call(CompanyTableSeerder::class);
//        $this->call(UnitTableSeeder::class);
//        $this->call(PacketTableSeerder::class);
//        $this->call(ProductTableSeeder::class);
//        $this->call(PagesTableSeeder::class);
//        $this->call(CouponTableSeeder::class);
    }
}
