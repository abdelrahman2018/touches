<?php

use Illuminate\Database\Seeder;

class PacketTableSeerder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = config('translatable.locales');

        $p = new \App\Packet();
        $p->min_quantity = 5;
        $p->save();


        foreach ($locales as $loc => $val)
        {
            $packetTranslation = new \App\PacketTranslation();
            $packetTranslation->packet_id = $p->id;
            $packetTranslation->name = 'p1';
            $packetTranslation->locale = $loc;
            $packetTranslation->save();
        }


        $p = new \App\Packet();
        $p->min_quantity = 5;
        $p->save();


        foreach ($locales as $loc => $val)
        {
            $packetTranslation = new \App\PacketTranslation();
            $packetTranslation->packet_id = $p->id;
            $packetTranslation->name = 'p2';
            $packetTranslation->locale = $loc;
            $packetTranslation->save();
        }
    }
}
