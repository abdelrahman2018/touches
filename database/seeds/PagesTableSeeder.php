<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p1 = new \App\Page();
        $p1->{'title:ar'} = 'من نحن';
        $p1->{'title:en'} = 'About Us';
        $p1->{'description:en'} = 'About Us About Us About Us About Us ';
        $p1->{'description:ar'} = 'من نحن من نحن  من نحن  من نحن ';
        $p1->image = 'public/uploads/pages/1.jpg';
        $p1->save();



        $p1 = new \App\Page();
        $p1->{'title:ar'} = 'سياسة الاستخدام';
        $p1->{'title:en'} = 'privacy policy';
        $p1->{'description:en'} = 'privacy policy privacy policy privacy policy privacy policy privacy policy';
        $p1->{'description:ar'} = 'سياسة الاستخدام سياسة الاستخدام سياسة الاستخدام سياسة الاستخدام';
        $p1->image = 'public/uploads/pages/1.jpg';
        $p1->save();

    }
}
