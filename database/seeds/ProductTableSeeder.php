<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = config('translatable.locales');

        $p = new \App\Product();
        $p->discount_price = 5;
        $p->price = 60;
        $p->unit_id = 2;
        $p->company_id = 4;
        $p->image = 'public/uploads/products/bottel.png';
        $p->size = 1;
        $p->save();

        foreach ($locales as $loc => $val)
        {
            $productTranslation = new \App\ProductTranslation();
            $productTranslation->name = 'water bottel 1';
            $productTranslation->description = 'water bottel 1';
            $productTranslation->product_id = $p->id;
            $productTranslation->locale = $loc;
            $productTranslation->save();
        }



        $p = new \App\Product();
        $p->discount_price = 3;
        $p->price = 50;
        $p->unit_id = 3;
        $p->company_id = 5;
        $p->image = 'public/uploads/products/bottel.png';
        $p->size = 1;
        $p->save();

        foreach ($locales as $loc => $val)
        {
            $productTranslation = new \App\ProductTranslation();
            $productTranslation->name = 'water bottel 1';
            $productTranslation->description = 'water bottel 1';
            $productTranslation->product_id = $p->id;
            $productTranslation->locale = $loc;
            $productTranslation->save();
        }



        $p = new \App\Product();
        $p->discount_price = 5;
        $p->price = 70;
        $p->unit_id = 1;
        $p->company_id = 6;
        $p->image = 'public/uploads/products/bottel.png';
        $p->size = 1;
        $p->save();

        foreach ($locales as $loc => $val)
        {
            $productTranslation = new \App\ProductTranslation();
            $productTranslation->name = 'water bottel 1';
            $productTranslation->description = 'water bottel 1';
            $productTranslation->product_id = $p->id;
            $productTranslation->locale = $loc;
            $productTranslation->save();
        }

    }
}
