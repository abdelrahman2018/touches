<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sett1 = new \App\Setting();
        $sett1->key = 'email';
        $sett1->save();

        $sett2 = new \App\Setting();
        $sett2->key = 'phone';
        $sett2->save();

        $sett3 = new \App\Setting();
        $sett3->key = 'facebook';
        $sett3->save();

        $sett4 = new \App\Setting();
        $sett4->key = 'twitter';
        $sett4->save();


        $sett6 = new \App\Setting();
        $sett6->key = 'instagram';
        $sett6->save();

        $sett6 = new \App\Setting();
        $sett6->key = 'delivery_charge';
        $sett6->save();

//        $sett7 = new \App\Setting();
//        $sett7->key = 'linkedin';
//        $sett7->save();
//
//        $sett8 = new \App\Setting();
//        $sett8->key = 'address_ar';
//        $sett8->save();
//
//        $sett9 = new \App\Setting();
//        $sett9->key = 'address_en';
//        $sett9->save();
//
//        $sett91 = new \App\Setting();
//        $sett91->key = 'lat';
//        $sett91->save();
//
//        $sett92 = new \App\Setting();
//        $sett92->key = 'lng';
//        $sett92->save();

    }
}
