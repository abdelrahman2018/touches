<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = config('translatable.locales');

        $unit = new \App\Unit();
        $unit->save();


        foreach ($locales as $loc => $val){
            $unitTranslation = new \App\UnitTranslation();
            $unitTranslation->unit_id = $unit->id;
            $unitTranslation->name= 'litre';
            $unitTranslation->locale = $loc;
            $unitTranslation->save();
        }


        $unit = new \App\Unit();
        $unit->save();


        foreach ($locales as $loc => $val){
            $unitTranslation = new \App\UnitTranslation();
            $unitTranslation->unit_id = $unit->id;
            $unitTranslation->name = 'mm';
            $unitTranslation->locale = $loc;
            $unitTranslation->save();
        }
    }
}
