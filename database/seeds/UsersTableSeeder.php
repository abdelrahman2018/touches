<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**************** Admin *******************/
        $user = new \App\User();
        $user->name = 'Admin';
        $user->email = 'admin@atiaf.com';
        $user->password = \Illuminate\Support\Facades\Hash::make('12345678');
        $user->type = 3;
        $user->active = 1;
        $user->save();

        /*************** Delegate *****************/
//        $user = new \App\User();
//        $user->name = 'delegate';
//        $user->username = 'delegate';
//        $user->phone = '+96601112233';
//        $user->image = 'public/uploads/profile.jpg';
//        $user->email = 'delegate@atiaf.com';
//        $user->device_token = rand(1, 999999999);
//        $user->active = 1;
//        $user->type = 2;
//        $user->password = \Illuminate\Support\Facades\Hash::make(123456);
//        $user->save();
    }
}
