$( document ).ready(function() {

wowReInitor(jQuery("#wowslider-container1"),{effect:"blur",prev:"",next:"",duration:19*100,delay:29*100,width:960,height:800,autoPlay:true,playPause:true,stopOnHover:false,loop:false,bullets:true,caption:true,captionEffect:"move",controls:true,onBeforeStep:0,images:0});
});


$('.navbar-nav .nav-item.active .nav-link').click(function(){
$('.navbar-nav .nav-item.active .nav-link').removeClass("active");
$(this).addClass("active");
});



(function() {
"use strict";
$(document).ready(function() {
var fakeAJAXOptions = {
chart: {
type: "line"
},
title: {
text: "e"
},
subtitle: {
text: ""
},
xAxis: {
categories: [
"Jan",
"Feb",
"Mar",
"Apr",
"May",
"Jun",
"Jul",
"Aug",
"Sep",
"Oct",
"Nov",
"Dec"
]
},
yAxis: {
title: {
text: "Temperature (°C)"
}
},
plotOptions: {
line: {
dataLabels: {
enabled: true
},
enableMouseTracking: false
}
}
};

var fakeAJAXData = [
{
name: "Tokyo",
data: [
7.0,
6.9,
9.5,
14.5,
18.4,
21.5,
25.2,
26.5,
23.3,
18.3,
13.9,
9.6
]
},
{
name: "London",
data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
}
];

var chartOptions = {
chart: {
type: "area"
},
title: {
text: ""
},
xAxis: {
categories: ["Apples", "Oranges", "Pears", "Grapes", "Bananas"]
},
credits: {
enabled: false
},
series: [
{
name: "John",
data: [5, 3, 4, 7, 2]
},
{
name: "Jane",
data: [2, -2, -3, 2, 1]
},
{
name: "Joe",
data: [3, 4, 4, -2, 5]
}
]
};


$("#update").click(function(e) {
e.preventDefault();

// update #chart1 with new data and options
$("#chart1")
.reusableHighchart()
.updateChart(fakeAJAXData, fakeAJAXOptions);

// rerender #chart2
$("#chart2").reusableHighchart("updateChart");
// or
// $("#chart2")
//   .pcmHighChart()
//   .updateChart();

var hcObject = $("#chart3").reusableHighchart("getChart");
console.log(hcObject); // array of all the High chart objects

var optionOverrides = {
xAxis: {
labels: {
formatter: function() {
return this.value; // clean, unformatted number for year
}
}
},
yAxis: {
labels: {
formatter: function() {
return this.value / 1000 + "(thousand)";
}
}
},
series: [
{
name: "USA",
data: [
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
5,
25,
50,
120,
150,
200,
426,
660,
869,
1060,
1605,
2471,
3322,
4238,
5221,
6129,
7089,
8339,
9399,
10538,
11643,
13092,
14478,
15915,
17385,
19055,
21205,
23044,
25393,
27935,
30062,
32049,
33952,
35804,
37431,
39197,
45000,
43000,
41000,
39000,
37000,
35000,
33000,
31000,
29000,
27000,
25000,
24000,
23000,
22000,
21000,
20000,
19000,
18000,
18000,
17000,
16000,
15537,
14162,
12787,
12600,
11400,
5500,
4512,
4502,
4502,
4500,
4500
]
},
{
name: "USSR/Russia",
data: [
null,
null,
null,
null,
null,
6,
11,
32,
110,
235,
369,
640,
1005,
1436,
2063,
3057,
4618,
6444,
9822,
15468,
20434,
24126,
27387,
29459,
31056,
31982,
32040,
31233,
29224,
27342,
26662,
26956,
27912,
28999,
28965,
27826,
25579,
25722,
24826,
24605,
24304,
23464,
23708,
24099,
24357,
24237,
24401,
24344,
23586,
22380,
21004,
17287,
14747,
13076,
12555,
12144,
11009,
10950,
10871,
10824,
10577,
10527,
10475,
10421,
10358,
10295,
10104,
9914,
9620,
9326,
5113,
5113,
4954,
4804,
4761,
4717,
4368,
4018
]
}
]
};


});
});
})();


$('#vmap').vectorMap({
map: 'world_en',
backgroundColor: '#fff',
color: '#ffffff',
hoverOpacity: 0.7,
selectedColor: '#666666',
enableZoom: true,
showTooltip: true,
scaleColors: ['#C8EEFF', '#006491'],
// values: sample_data,
normalizeFunction: 'polynomial'
});

$('#us3').locationpicker({
location: {
latitude: 46.15242437752303,
longitude: 2.7470703125
},
radius: 300,
inputBinding: {
latitudeInput: $('#us3-lat'),
longitudeInput: $('#us3-lon'),
radiusInput: $('#us3-radius'),
locationNameInput: $('#us3-address')
},
enableAutocomplete: true,
onchanged: function (currentLocation, radius, isMarkerDropped) {
// Uncomment line below to show alert on each Location Changed event
//alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
}
});

///////////////////////////////////END map

$(".a-minus").on('click', function() {
$(this).toggleClass("rotate");
$(this).parent().next(".disbox").slideToggle("slow");
});
///////////////////////////////////END slideToggle

$('#example').DataTable( {
columnDefs: [ {
orderable: false,
className: 'select-checkbox',
targets:   0
} ],
select: {
style:    'os',
selector: 'td:first-child'
},
order: [[ 1, 'asc' ]]
} );

$('#example2').DataTable( {
columnDefs: [ {
orderable: false,
className: 'select-checkbox',
targets:   0
} ],
select: {
style:    'os',
selector: 'td:first-child'
},
order: [[ 1, 'asc' ]]
} );

///////////////////////////////////END DataTable


$(".addCF").click(function(){
$(".customFields").append('<tr class="cities_tr"><td><select required name="form_cities[]"   class="form-control tabinp form_city_id"></select></td><td><input type="number" required  name="form_quantity[]" class="form-control tabinp"  value="" placeholder="" min="1"> </td><td><a href="javascript:void(0);"  class="btn btn-sm btn-danger remCF" aria-hidden="true"><i class="fas fa-trash"></i> </a></td></tr>');
$(".remCF").on('click',function(){
$(this).parent().parent().remove();
});
	
});



$(".addCF2").click(function(){
$(".customFields").append('<tr><td><select class="form-control tabinp"><option value="1">Gender</option><option value="2">City</option><option value="3">Age</option><option value="4">No of Orders</option><option value="5">Total of Orders</option><option value="6">Registration Date</option><option value="7">Registration Source</option><option value="8">Platform</option><option value="9">Customer Group</option></select></td><td><select class="form-control tabinp"><option value="1">Is Equal To</option><option value="2">Is not Equal to</option></select></td><td><select class="form-control tabinp"><option>--Select--</option><option value="1">Male</option><option value="2">Female</option></select></td><td><a href="javascript:void(0);"  class="btn btn-sm btn-danger remCF2" aria-hidden="true">Remove </a></td></tr>');
$(".remCF2").on('click',function(){
$(this).parent().parent().remove();
});
});




$(".addCF3").click(function(){
$(".customFields").append('<tr><tr><td><input class="form-control tabinp"  placeholder="Id"></td><td><input class="form-control tabinp"  placeholder="Country"></td><td><a href="javascript:void(0);"  class="btn btn-sm btn-danger remCF3" aria-hidden="true">Remove </a></td></tr>');
$(".remCF3").on('click',function(){
$(this).parent().parent().remove();
});
});


 







$(".txtEditor").Editor();
$(".txtEditor2").Editor();


var pickedup;
$(document).ready(function() {
$( ".sourcetable tbody tr" ).on( "click", function( event ) {
if (pickedup != null) {
// pickedup.css( "background-color", "#ffccff" );
}

 $("#configreset").show("");

$("#fillname1").val($(this).find("td").eq(0).html());
$("#fillname2").val($(this).find("td").eq(1).html());
$("#fillname3").val($(this).find("td").eq(2).html());
$("#fillname4").val($(this).find("td").eq(3).html());
$("#fillname5").val($(this).find("td").eq(4).html());
$("#fillname6").val($(this).find("td").eq(5).html());
$("#fillname7").val($(this).find("td").eq(6).html());
$("#fillname8").val($(this).find("td").eq(7).html());
$("#fillname9").val($(this).find("td").eq(8).html());
$("#fillname10").val($(this).find("td").eq(9).html());
$("#fillname11").val($(this).find("td").eq(10).html());
$("#fillname12").val($(this).find("td").eq(11).html());
$("#fillname13").val($(this).find("td").eq(12).html());
$("#fillname14").val($(this).find("td").eq(13).html());
$("#fillname15").val($(this).find("td").eq(14).html());
$("#fillname16").val($(this).find("td").eq(15).html());

$( this ).css( "background-color", "#e3e3e3" );

 $('#configreset').click(function(){
  $(this).hide("");
  $('#fillname1')[0].reset();
  
 
 });

pickedup = $( this );
});
});	

$('.datepicker').daterangepicker({
    "singleDatePicker": true,
    "startDate": "08/3/2000",
    "endDate": "09/06/2050"
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});


 
			
			
			
 



 
	
 

//
//var current_socialid = 0;
//var current_langid = 0;
//var current_skillid = 0;
//$(document).ready(function($){
//  $("#langadd,#skilladd,#socialadd").click(function(event){
//	event.preventDefault();
//	var fieldname = $(this).attr('name');
//	var newElement = $("#"+fieldname+"_rep").clone(true, true ).css('display','block');
//    var id = 1;
//	var element = $("#"+fieldname+"_rep");
//	//console.log(element);
//	if (fieldname == "langadd")
//	{
//		id = current_langid + 1;
//		current_langid = id;
//	}
//	else if (fieldname == "skilladd")
//	{
//		id = current_skillid + 1;
//		current_skillid = id;
//	}
//	else if (fieldname == "socialadd")
//	{
//		id = current_socialid + 1;
//		current_socialid = id;
//	}
//    if(id <10)id = "0"+id;
//    newElement.attr("id",fieldname+"_"+id);
//	newElement.attr("name",fieldname+"_"+id);
//	
//	$('input,select,button', newElement).each( function() {
//		var field = $(this).attr("id");
//		$(this).attr("id", field.split("_")[0]+"_"+id ).val('');
//		var field2 = $(this).attr("name");
//		$(this).attr("name", field2.split("_")[0]+"_"+id ).val('');
//	});
//    newElement.appendTo($("#"+fieldname+"_out"));
//  });
//  
//  
//  $('.remScnt').click( function(e) { 
//	e.preventDefault();
//
//	$(this).parent('div').remove();
//
//	return false;
//	});
//});
//	
	
