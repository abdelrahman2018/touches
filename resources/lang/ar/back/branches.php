<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/18/2019
 * Time: 12:27 PM
 */

return [
  'branch_list' => 'قائمة الفروع',
    'branch_all' => 'كل الفروع',
    'branch_add' => 'اضافة فرع جديد',
    'branch_edit' => 'تعديل فرع',
    'city' => 'المدينة',
    'name' => 'الاسم',
    'address' => 'العنوان',
    'phone' => 'الهاتف',
    'email' => 'البريد الالكتروني',
    'fax' => 'الفاكس',
    'map' => 'الموقع'
];