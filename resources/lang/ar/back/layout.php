<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 9:41 AM
 */




return [

    /*********  Side Menu ***********/
    'cats' => 'التصنيفات',
    'cats.main' => 'التصنيفات الرئيسية',
    'cats.sub' => 'التصنيفات الفرعية',
    'products' => 'المنتجات',
    'offers' => 'العروض',
    'jobs' => 'الوظائف',
    'ads' => 'الاعلانات',
    'news' => 'الاخبار',
    'cities' => 'المدن',
    'branches' => 'الفروع',
    'subscriptions' => 'القائمة البريدية',
    'questions' => 'الاسئلة',
    'msgs' => 'الرسائل',
    'questionaire' => 'الاستبيان',
    'pages' => 'الصفحات',
    'slides' => 'شرائح العروض',
    'setting' => 'الاعدادات',

    'dashboard' => 'لوحة التحكم',
    'logout' => 'تسجيل خروج',


      'back' => 'رجوع',
    'added.modified' => 'تاريخ التعديل/الاضافة',
    'added.date' => 'تاريخ الاضافة,',
    'modified.date' => 'تاريخ التعديل,',
    'action' => 'Action',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'create' => 'اضافة',


        /************ datatable *******************/
    'title' => 'العنوان',
    'image' => 'الصورة',
    'url' => 'الرابط',
    'name' => 'الاسم',
    'user' => 'اسم المستخدم',
];


?>