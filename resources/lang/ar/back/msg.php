<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/18/2019
 * Time: 11:10 AM
 */


return [
    'msg_list' => 'اظهار كل الرسائل',
    'msg_all' => 'كل الرسائل',
    'name' => 'الاسم',
    'email' => 'البريد الالكتروني',
    'msg_type' => 'نوع الرسالة',
    'msg' => 'الرسالة',
    'reply' => 'ارسال رسالة',
    'subject' => 'الموضوع',
    'phone' => 'الهاتف',
];