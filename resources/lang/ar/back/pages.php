<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 11:07 AM
 */


return [

    'index' => 'كل الصفحات',
    'list' => 'كل الصفحات ',

      'title.ar' => 'العنوان (العربية)',
    'title.en' => 'العنوان (English)',

    'description.ar' => 'الوصف (العربية)',
    'description.en' => 'الوصف (English)',

];