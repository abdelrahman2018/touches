<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/18/2019
 * Time: 11:48 AM
 */

return [
    'q_all' => 'كل الاسئلة',
    'q_list' => 'قائمة الاسئلة',
    'title' => 'السوال',
    'title_ar' => 'السوال (العربية)',
    'title_en' => 'السؤال (انجليزية)',
    'q_add' => 'اضافة سوال جديد',
    'q_edit' => 'تعديل السؤال'
    
];