<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 11:45 AM
 */


return [

    'list' => 'كل الاستبيانات',
    'all' => 'كل الاستبيانات',
    'questionaire' => 'الاستبيان',
    'city' => 'المدينة',
    'branch' => 'الفرع',
    'add' => 'اضافة سوال',

    'name' => 'الاسم',
    'phone' => 'الهاتف',
    'email' => 'البريد الالكتروني',
    'notes' => 'ملاحظات',

    'question' => 'السؤال',
    'answer' => 'الاجابة'
];