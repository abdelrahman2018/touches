<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 10:09 AM
 */


return [

    'general' => 'اعدادت عامة',
    'facebook' => 'لينك فيسبوك',
    'google' => 'لينك جوجل',
    'twitter' => 'لينك تويتر',
    'insta' => 'لينك انستجرام',
    'whatsapp' => 'رقم واتساب',
    'contact.email' => 'بريد الاتصال',
    'contact.label' => 'البريد الالكتروني',
    'info' => 'معلومات الموقع',
    'phone' => 'رقم الهاتف',
    'address.ar' => 'العنوان عربي',
    'address.en' => 'العنوان بالانجليزية',
    'map' => 'الموقع',
    'linked' => 'لينكد ان'


];