<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 10:38 AM
 */

return [

    'list.all' => 'عرض كل الشرائح',
    'add' => 'اضافة شريحة جديدة',
    'title.ar' => 'العنوان(العربية)',
    'title.en' => 'العنوان (الانجليزية)',
    'url' => 'الرابط',
    'image' => 'الصورة',
    'slides' => 'شرائح العرض',
    'edit' => 'تعديل'

];