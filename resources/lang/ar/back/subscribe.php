<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/18/2019
 * Time: 11:22 AM
 */


return [
    'subscribe_all' => 'كل الاشتراكات',
    'email' => 'البريد الالكتروني',
    'send' => 'ارسل رسالة',
    'send_all' => 'ارسال للكل'

];