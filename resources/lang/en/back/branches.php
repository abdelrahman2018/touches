<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/18/2019
 * Time: 12:27 PM
 */

return [
  'branch_list' => 'Listing Branches',
    'branch_all' => 'All Branches',
    'branch_add' => 'Add New Branch',
    'branch_edit' => 'Edit Branch',
    'city' => 'City',
    'name' => 'Name',
    'address' => 'Address',
    'phone' => 'Phone',
    'email' => 'Email',
    'fax' => 'Fax',
    'map' => 'Map'
];