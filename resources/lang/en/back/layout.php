<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 9:41 AM
 */




return [

    /*********  Side Menu ***********/
  'cats' => 'Ctegories',
    'cats.main' => 'Main Categories',
    'cats.sub' => 'Sub Category',
  'products' => 'Products',
    'offers' => 'Offers',
    'jobs' => 'Jobs',
    'ads' => 'Advertisments',
    'news' => 'News',
    'cities' => 'Cities',
    'branches' => 'Branches',
    'subscriptions' => 'Subscriptions',
    'questions' => 'Questions',
    'msgs' => 'Messages',
    'questionaire' => 'Questionaire',
    'pages' => 'Pages',
    'slides' => 'Slides',
    'setting' => 'Settings',

    'dashboard' => 'Dashboard',
    'logout' => 'Logout',

    /************ datatable *******************/
    'title' => 'Title',
    'name' => 'Name',
    'url' => 'link',

    /************ Buttons ********************/
    'back' => 'Back',
    'added.modified' => 'Added/Last Modified Date',
    'added.date' => 'Added Date,',
    'modified.date' => 'Modified Date,',
    'action' => 'Action',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'create' => 'Create',
    'image' => 'Image',
    'user' => 'User Name',

//    /**************** modal dialouge **********/
//    'modal.head' => ''

];


?>