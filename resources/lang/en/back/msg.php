<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/18/2019
 * Time: 11:10 AM
 */


return [
    'msg_list' => 'List All Messages',
    'msg_all' => 'All Messages',
    'name' => 'Name',
    'email' => 'Email',
    'msg_type' => 'Message Type',
    'msg' => 'Message',
    'reply' => 'Reply',
    'subject' => 'Subject',
    'phone' => 'Phone',
];