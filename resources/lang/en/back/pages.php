<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 11:07 AM
 */


return [

    'index' => 'All Pages',
    'list' => 'List All Pages',

    'title.ar' => 'Title (Arabic)',
    'title.en' => 'Title (English)',

    'description.ar' => 'Description (Arabic)',
    'description.en' => 'Description (English)',

];