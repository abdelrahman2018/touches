<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/18/2019
 * Time: 11:48 AM
 */

return [
    'q_all' => 'All Questions',
    'q_list' => 'List All Questions',
    'title' => 'Title',
    'title_ar' => 'question (Arabic)',
    'title_en' => 'question (English)',
    'q_add' => 'Add New Question',
    'q_edit' => 'Edit Question'
];