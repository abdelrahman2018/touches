<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 11:45 AM
 */


return [

    'list' => 'List All Questionaire',
    'all' => 'All Questionaire',
    'questionaire' => 'Questionaire',
    'city' => 'City',
    'branch' => 'Branch',
    'add' => 'Add New question',
    'name' => 'Name',
    'phone' => 'Phone',
    'email' => 'Email',
    'notes' => 'Notes',
    'question' => 'Question',
    'answer' => 'Answer'
];