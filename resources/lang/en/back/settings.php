<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 10:09 AM
 */


return [

    'general' => 'General Settings',
    'facebook' => 'Facebook URL',
    'google' => 'Google Url',
    'twitter' => 'Twitter URL',
    'insta' => 'Instagram URL',
    'whatsapp' => 'whatsapp',
    'contact.email' => 'Inquery Email',
    'contact.label' => 'Contact Us Email',
    'info' => 'Our Info',
    'phone' => 'Phone Number',
    'address.ar' => 'Address Arabic',
    'address.en' => 'Adderss English',
    'map' => 'Map',
    'linked' => 'Linkedin'


];