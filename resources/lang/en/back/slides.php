<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/17/2019
 * Time: 10:38 AM
 */

return [

    'list.all' => 'Listing All Slides',
    'add' => 'Add New Slide',
    'title.ar' => 'Title (Arabic)',
    'title.en' => 'Title (English)',
    'url' => 'link',
    'image' => 'Image',
    'slides' => 'Slides',
    'edit' => 'Edit'

];