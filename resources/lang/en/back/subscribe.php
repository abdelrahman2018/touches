<?php
/**
 * Created by PhpStorm.
 * User: Atiaf
 * Date: 3/18/2019
 * Time: 11:22 AM
 */


return [
    'subscribe_all' => 'All Subscriptions',
    'email' => 'Email',
    'send' => 'Send Message',
    'send_all' => 'Send To All'

];