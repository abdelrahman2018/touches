@extends('layouts.front.master')


@section('title')
    <title>Forget password</title>
@stop

@section('content')

    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="about padding games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>نسيت كلمة المرور</h2>
                </div>

                <div class="clients-det login-margin">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <form action="{{route('forget.submit')}}" method="post" class="well games-form">
                            @csrf
                            <div class="control-group">
                                <div class="form-group">
                                    <label>ادخل بريدك الالكترونى</label>
                                    <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{old('email')}}">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                             </span>
                                    @endif
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ارسال</button>
                        </form>
                    </div>

                </div>

            </div>
        </section>

    </main>

@stop