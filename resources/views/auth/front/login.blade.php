@extends('layouts.front.master')


@section('title')
    <title>login</title>
@stop

{{--@section('styles')--}}
    {{--<link href="{{asset('public/css/front/blue.css')}}" rel="stylesheet">--}}
{{--@stop--}}

@section('content')


    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="about padding games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>تسجيل دخول</h2>
                </div>

                <div class="clients-det login-margin">
                    <div class="row">
                        <form action="{{route('login.submit')}}" method="post" class="well games-form">
                            @csrf
                            <div class="control-group">
                                <div class="form-group">
                                    <label>البريد الالكترونى:</label>
                                    <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="يجب كتابة البريد الالكترونى الذى سجلته عند الطلب">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>كلمة المرور:</label>
                                    <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="كلمة المرور">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <a href="{{route('forget.form')}}" class="forget">نسيت كلمة المرور؟</a>
                            <button type="submit" class="btn btn-primary">دخول</button>
                            <a href="{{route('register.form')}}" class="signin">غير مسجل من قبل, اريد التسجيل</a>
                        </form>
                    </div>

                </div>

            </div>
        </section>

    </main>

@stop