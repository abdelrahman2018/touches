@extends('layouts.front.master')


@section('title')
    <title>Register</title>
@stop

{{--@section('styles')--}}
    {{--<link href="{{asset('public/css/front/purple.css')}}" rel="stylesheet">--}}
{{--@stop--}}

@section('content')


    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="about padding games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>تسجيل جديد</h2>
                </div>

                <div class="clients-det login-margin">
                    <div class="row">
                        <form action="{{route('register.submit')}}" method="post" class="well games-form">
                            @csrf
                            <div class="control-group">
                                <div class="form-group">
                                    <label>البريد الالكترونى:</label>
                                    <input type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{old('email')}}" placeholder="يجب كتابة البريد الالكترونى الذى سجلته عند الطلب">

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>كلمة المرور:</label>
                                    <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="كلمة المرور">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>تأكيد كلمة المرور:</label>
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="كلمة المرور">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">تسجيل</button>
                        </form>
                    </div>

                </div>

            </div>
        </section>

    </main>


@stop