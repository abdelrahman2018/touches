@extends('layouts.front.master')


@section('title')
    <title>Reset password</title>
@stop

@section('content')



    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="about padding games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>تغيير كلمة المرور</h2>
                </div>

                <div class="clients-det login-margin">
                    <div class="row">
                        <form action="{{route('newPassword.submit')}}" method="post" class="well games-form">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="control-group">
                                <div class="form-group">
                                    <label>البريد الالكتروني</label>
                                    <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ $email ?? old('email') }}" >

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="form-group">
                                    <label>كلمة المرور الجديدة:</label>
                                    <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>اعادة كلمة المرور الجديدة:</label>
                                    <input type="password" name="password_confirmation" class="form-control">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ارسال</button>
                        </form>
                    </div>

                </div>

            </div>
        </section>

    </main>

@stop