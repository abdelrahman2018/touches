@extends('layouts.app')


@section('content')


    <section class="balogin"> </section>
    <div class="container">
        <div class="loginbok">
            <div class="col-sm-6 inputbox">
                <div class="logo"> <a href="{{ asset('assets/admin') }}/#"> <img src="{{ asset('public/images2/logologin.png') }}" title="Atiaf  For Completely Solutions"> </a> </div>
                <h2 class="title-con">Welcome Atiaf  For Completely Solutions!</h2>

                {{--@if(Session::has('invalidCred'))--}}
                    {{--<div  style="color: red" class="login-box-msg text-center text-danger">--}}

                        {{--{{_lang('app.invalid_username_or_password')}}--}}
                    {{--</div>--}}
                {{--@elseif(Session::has('inactive'))--}}
                    {{--<div  style="color: red" class="login-box-msg text-center text-danger">--}}

                        {{--{{ _lang('app.sorry_your_account_is_inactive_and_may_not_login') }}--}}
                    {{--</div>--}}

                {{--@else--}}
                    {{--<p class="text">Please login to your account</p>--}}
                {{--@endif--}}

                <form method="POST"   action="{{ route('login') }}" aria-label="{{ __('Login') }}" class="form-horizontal">
                    @csrf
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter your email or username" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif


                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Enter your password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif

                    <div class="divlink">
                        <label>
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <span class="label-text">Remember Me</span> </label>
                    </div>
                    <button type="submit" class="largeredbtn"> Login To Your Account</button>
                </form>
            </div>
            <!--inputbox-->

            <div class="col-sm-6 photos">
                <div id="wowslider-container1">
                    <div class="ws_images">
                        <ul>
                            <li><img src="{{ asset('public/images2/img1.jpg')}}" title=""/></li>
                            <li><img src="{{ asset('public/images2/img1.jpg')}}" title=""/></li>
                            <li><img src="{{ asset('public/images2/img1.jpg')}}" title=""/></li>
                            <li><img src="{{ asset('public/images2/img1.jpg')}}" title=""/></li>
                            <li><img src="{{ asset('public/images2/img1.jpg')}}" title=""/></li>
                            <li><img src="{{ asset('public/images2/img1.jpg')}}" title=""/></li>
                        </ul>
                    </div>
                    <!--//.ws_images-->

                </div>
                <!--wowslider-container1-->
            </div>
        </div>
    </div>
    <!--container-->











{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Login') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('login') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<div class="form-check">--}}
                                    {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                                    {{--<label class="form-check-label" for="remember">--}}
                                        {{--{{ __('Remember Me') }}--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-8 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Login') }}--}}
                                {{--</button>--}}

                                {{--@if (Route::has('password.request'))--}}
                                    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                        {{--{{ __('Forgot Your Password?') }}--}}
                                    {{--</a>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
@endsection
