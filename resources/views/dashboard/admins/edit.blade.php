@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    {{--@lang('back/layout.edit')--}}
    Edit Admin
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('admin.index')}}"><i class="fa fa-bars"></i> List all Admins</a></li>
                <li class="active">Edit Unit</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Edit Admin </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="{{route('admin.update', $admin->id)}}" accept-charset="UTF-8" class="form-horizontal form-validate">
                                                @csrf


                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Name</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="name" value="{{$admin->name}}" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Email</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="email" name="email" value="{{$admin->email}}" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Password</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="password" name="password"  class="form-control field-validate">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">status</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <select name="status" class="form-control field-validate">
                                                            <option value="1" {{$admin->active == 1? 'selected':''}}>Active</option>
                                                            <option value="0" {{$admin->active == 0? 'selected':''}}>Not Active</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 topbox">
                                                    <div  class="inneradmin">
                                                        <h4 class="admintitle"> Roles</h4>
                                                        <div  id="permissions_add" >
                                                            @foreach($roles as $role)
                                                                <div class="col-sm-2 topbox nobord">
                                                                    <label class="col-sm-2 topbox">
                                                                        <input value="{{$role->name}}" {{$admin->hasRole($role->name)? 'checked': ''}} type="checkbox" class="form_permissions" name="roles[]"><span class="label-text">{{$role->name}}</span>
                                                                    </label>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">@lang('back/layout.edit')</button>
                                                    <a href="{{route('admin.index')}}" type="button" class="btn btn-default">@lang('back/layout.back')</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection
