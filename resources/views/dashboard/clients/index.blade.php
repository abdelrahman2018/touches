@extends('layouts.admin.master')

@section("title")
    {{_lang('app.all_clients')}}
@endsection


@section('styles')
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">--}}
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">--}}
@endsection

@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('client.index')}}"><i class="fa fa-bars"></i>{{_lang('app.list_all_clients')}}</a></li>
                <li class="active">{{_lang('app.all_clients')}}</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">{{_lang('app.all_clients')}}</h1>
                            <a href="{{route('client.exp')}}" class="btn btn-success">{{_lang('app.export_excel')}}</a>

                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                               <div class="text-center">
                                   <form id="search-form" method="post" action="{{route('client.search')}}" accept-charset="UTF-8" class="form-horizontal form-validate" >
                                       @csrf
                                       <label>{{_lang('app.from')}}</label><input type="date" name="from">
                                       <label>{{_lang('app.to')}}</label><input type="date" name="to">
                                       <button>{{_lang('app.search')}}</button>
                                   </form>
                               </div>
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">
                                            {{--{{$dataTable->table()}}--}}

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="5%">id</th>
                                                    <th>{{_lang('app.name')}}</th>
                                                    <th>{{_lang('app.username')}}</th>
                                                    <th>{{_lang('app.phone')}}</th>
                                                    <th>{{_lang('app.email')}}</th>
                                                    <th>{{_lang('app.image')}}</th>
                                                    <th>{{_lang('app.status')}}</th>
                                                    <th>@lang('back/layout.added.modified')</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($clients as $client)
                                                    <tr class="trData">
                                                        <td>{{$client->id}}</td>
                                                        <td>{{$client->name}}</td>
                                                        <td>{{$client->username}}</td>
                                                        <td>{{$client->phone}}</td>
                                                        <td>{{$client->email}}</td>
                                                        <td>
                                                            <a href="{{asset($client->image)}}" data-fancybox> <img src="{{asset($client->image)}}" width="100%"></a>
                                                        </td>
                                                        <td><a href="#"><span data-id="{{$client->id}}" class="label {{$client->active == 1? 'label-success':'label-danger'}}">{{$client->active == 1? _lang('app.active'):_lang('app.not_active')}}</span></a></td>
                                                        <td>
                                                            <strong>@lang('back/layout.added.date'): </strong>
                                                            {{$client->created_at}}
                                                            <br>
                                                            <strong>@lang('back/layout.modified.date'): </strong>
                                                            {{$client->updated_at}}
                                                        </td>
                                                        <td>
                                                            <a data-toggle="tooltip" data-id="{{$client->id}}" data-placement="bottom" title="@lang('back/layout.delete')" href=""  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            {{--<a data-toggle="tooltip" data-placement="bottom" title="@lang('back/layout.edit')" href="{{route('company.edit', $company->id)}}" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-edit" aria-hidden="true"></i></a>--}}
                                                            {{--<form action="{{route('main.destroy', $category->id)}}" method="post">--}}
                                                            {{--@csrf--}}
                                                            {{--{{method_field('DELETE')}}--}}
                                                            {{--</form>--}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection

@section('scripts')
    <script>
        $('.bg-red').on('click', function (e) {
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                $(this).parent().parent().remove();
                $.ajax({
                    type: 'post',
                    url: '{{route('client.delete')}}',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: $(this).data('id')
                    },
                    success: function (data) {

                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    }
                })
            }
        })


        })
    </script>

    <script>
        $('.label').on('click', function () {

            $.ajax({
                type: 'post',
                url: '{{route('client.status.change')}}',
                data: {
                    _token: '{{csrf_token()}}',
                    id: $(this).data('id')
                },
                success: function (data) {
                    location.reload();
                }
            })
        })
    </script>

    <script>
    $(document).ready(function() {
    var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
    console.log('loc : '+ locale );
    $('#dataTable').DataTable({
    "language": {
    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
    }
    });
    });
    </script>
@stop


{{--@section('scripts')--}}
    {{--<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>--}}
    {{--<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>--}}
    {{--{{$dataTable->scripts()}}--}}
    {{--<script>--}}
        {{--$(function() {--}}
            {{--$('#users-table').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: '{{route('client.data')}}',--}}
                {{--columns: [--}}
                    {{--{ data: 'id', name: 'id' },--}}
                    {{--{ data: 'name', name: 'name' },--}}
                    {{--{ data: 'username', name: 'username' },--}}
                    {{--{ data: 'email', name: 'email' },--}}
                    {{--{ data: 'phone', name: 'phone' },--}}
                    {{--{ data: 'image', name: 'image' },--}}
                    {{--{ data: 'active', name: 'active' }--}}
                {{--]--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
{{--@endsection--}}
