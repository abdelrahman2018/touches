@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    {{--@lang('back/slides.slides')--}}
    {{_lang('app.send_notification')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        {{--<section class="content-header">--}}


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title"> {{_lang('app.all_clients')}}</h1>
                            {{--<a href="{{route('client.notifi')}}" class="btn btn-success">Send</a>--}}
                            {{--<div class="box-tools @if(app()->getLocale() == 'en') pull-right @else pull-left @endif">--}}
                            {{--<a href="" data-toggle="modal" data-target="#addModal" type="button" class="btn btn-block btn-primary">Add Coupon</a>--}}
                            {{--</div>--}}
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">
                                            <form method="post" action="{{route('notifi.send')}}" class="form-horizontal form-validate">
                                                @csrf

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label"> {{_lang('app.message')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <textarea rows="10" cols="70" name="msg"> </textarea>
                                                    </div>
                                                </div>

                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">{{_lang('app.send')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

            <a href="{{\Illuminate\Support\Facades\URL::previous()}}" class="btn btn-primary btn-lg">{{_lang('app.back')}}</a>

        </section>

    </div>


@endsection

