@extends('layouts.admin.master')

@section("title")
    {{_lang('app.all_clients')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        {{--<section class="content-header">--}}


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">{{_lang('app.all_clients')}}</h1>
                            <a href="{{route('client.search.exp', ['from' => $from, 'to' => $to])}}" class="btn btn-success">{{_lang('app.export_excel')}}</a>
                            {{--<div class="box-tools @if(app()->getLocale() == 'en') pull-right @else pull-left @endif">--}}
                                {{--<a href="" data-toggle="modal" data-target="#addModal" type="button" class="btn btn-block btn-primary">Add Coupon</a>--}}
                            {{--</div>--}}
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">
                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="5%">id</th>
                                                    <th>{{_lang('app.name')}}</th>
                                                    <th>{{_lang('app.username')}}</th>
                                                    <th>{{_lang('app.phone')}}</th>
                                                    <th>{{_lang('app.email')}}</th>
                                                    <th>{{_lang('app.image')}}</th>
                                                    <th>{{_lang('app.status')}}</th>
                                                    <th>@lang('back/layout.added.modified')</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                {{--                                                    {{dd($orderStatus)}}--}}
                                                @foreach($clients as $client)
                                                    <tr class="trData">
                                                        <td>{{$client->id}}</td>
                                                        <td>{{$client->name}}</td>
                                                        <td>{{$client->username}}</td>
                                                        <td>{{$client->phone}}</td>
                                                        <td>{{$client->email}}</td>
                                                        <td>
                                                            <a href="{{asset($client->image)}}" data-fancybox> <img src="{{asset($client->image)}}" width="100%"></a>
                                                        </td>
                                                        <td><span class="label {{$client->active == 1? 'label-success':'label-danger'}}">{{$client->active == 1? _lang('app.active'):_lang('app.not_active')}}</span></td>
                                                        <td>
                                                            <strong>@lang('back/layout.added.date'): </strong>
                                                            {{$client->created_at}}
                                                            <br>
                                                            <strong>@lang('back/layout.modified.date'): </strong>
                                                            {{$client->updated_at}}
                                                        </td>
                                                        <td>
                                                            <a data-toggle="tooltip" data-id="{{$client->id}}" data-placement="bottom" title="@lang('back/layout.delete')" href=""  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            {{--<a data-toggle="tooltip" data-placement="bottom" title="@lang('back/layout.edit')" href="{{route('company.edit', $company->id)}}" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-edit" aria-hidden="true"></i></a>--}}
                                                            {{--<form action="{{route('main.destroy', $category->id)}}" method="post">--}}
                                                            {{--@csrf--}}
                                                            {{--{{method_field('DELETE')}}--}}
                                                            {{--</form>--}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

            <a href="{{\Illuminate\Support\Facades\URL::previous()}}" class="btn btn-primary btn-lg">{{_lang('app.back')}}</a>

        </section>

    </div>


@endsection

