@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    {{--@lang('back/layout.edit')--}}
    {{_lang('app.edit_company')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('company.index')}}"><i class="fa fa-bars"></i>{{_lang('app.list_all_companies')}}</a></li>
                <li class="active">Edit Company</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{_lang('app.edit_company')}}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="{{route('company.update', $company->id)}}" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                @csrf

                                                @foreach($locales as $loc => $val)
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.name')}} ({{$val}})</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="name[{{$loc}}]" value="{{$company{'name:'.$loc} }}" class="form-control field-validate">
                                                        </div>
                                                    </div>
                                                @endforeach

                                                {{--<div class="form-group">--}}
                                                    {{--<label for="name" class="col-sm-2 col-md-3 control-label">status</label>--}}
                                                    {{--<div class="col-sm-10 col-md-4">--}}
                                                        {{--<select name="status" class="form-control field-validate">--}}
                                                            {{--<option value="1" {{$unit->active == 1? 'selected':''}}>Active</option>--}}
                                                            {{--<option value="0" {{$unit->active == 0? 'selected':''}}>Not Active</option>--}}
                                                        {{--</select>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}


                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.status')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <select name="status" class="form-control field-validate">
                                                            <option value="1" {{$company->active == 1? 'selected':''}}>{{_lang('app.active')}}</option>
                                                            <option value="0" {{$company->active == 0? 'selected':''}}> {{_lang('app.not_active')}}</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/layout.image')</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input id="newImage" name="image" type="file">
                                                        <img src="{{asset('public/uploads/companies/'.$company->image)}}" width="100px">
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">@lang('back/layout.edit')</button>
                                                    <a href="{{route('company.index')}}" type="button" class="btn btn-default">@lang('back/layout.back')</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection
