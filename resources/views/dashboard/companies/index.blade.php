@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    {{--@lang('back/slides.slides')--}}
    {{_lang('app.all_companies')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('company.index')}}"><i class="fa fa-bars"></i> {{_lang('app.list_all_companies')}}</a></li>
                <li class="active">  {{_lang('app.all_companies')}}</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">  {{_lang('app.all_companies')}}</h1>
                            <div class="box-tools @if(app()->getLocale() == 'en') pull-right @else pull-left @endif">
                                <a href="{{route('company.create')}}" type="button" class="btn btn-block btn-primary"> {{_lang('app.add_company')}}</a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="5%">id</th>
                                                    <th>{{_lang('app.image')}}</th>
                                                    <th>{{_lang('app.name')}}</th>
                                                    <th>{{_lang('app.status')}}</th>
                                                    <th>@lang('back/layout.added.modified')</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($companies as $company)
                                                    <tr class="trData">
                                                        <td>{{$company->id}}</td>
                                                        <td>
                                                            <a href="{{asset($company->image)}}" data-fancybox> <img src="{{asset($company->image)}}" width="100%"></a>
                                                        </td>
                                                        <td>{{$company->name}}</td>
                                                        <td><span class="label {{$company->active == 1? 'label-success':'label-danger'}}">{{$company->active == 1? _lang('app.active'):_lang('app.not_active')}}</span></td>
                                                        <td>
                                                            <strong>@lang('back/layout.added.date'): </strong>
                                                            {{$company->created_at}}
                                                            <br>
                                                            <strong>@lang('back/layout.modified.date'): </strong>
                                                            {{$company->updated_at}}
                                                        </td>
                                                        <td>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="@lang('back/layout.edit')" href="{{route('company.edit', $company->id)}}" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                                            {{--<a data-toggle="tooltip" data-id="{{$company->id}}" data-placement="bottom" title="@lang('back/layout.delete')" href=""  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>--}}
                                                            {{--<form action="{{route('main.destroy', $category->id)}}" method="post">--}}
                                                            {{--@csrf--}}
                                                            {{--{{method_field('DELETE')}}--}}
                                                            {{--</form>--}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection

@section('scripts')
    <script>
        $('.bg-red').on('click', function (e) {
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                $(this).parent().parent().remove();
                $.ajax({
                    type: 'post',
                    url: '{{route('company.delete')}}',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: $(this).data('id')
                    },
                    success: function () {

                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    }
                })
            }
        })


        })
    </script>

    <script>
    $(document).ready(function() {
    var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
    console.log('loc : '+ locale );
    $('#dataTable').DataTable({
    "language": {
    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
    }
    });
    });
    </script>
@stop
