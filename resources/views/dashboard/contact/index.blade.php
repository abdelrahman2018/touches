@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    users messages
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('contact.msgs')}}"><i class="fa fa-bars"></i>@lang('back/msg.msg_list')</a></li>
                <li class="active">@lang('back/msg.msg_all')</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">

                        {{--<div class="box-header">--}}
                            {{--<div class="box-tools @if(app()->getLocale() == 'en') pull-right @else pull-left @endif">--}}
                                {{--<a href="mailto:{{$group}}" type="button" class="btn btn-block btn-success">Send to all</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                    <tr>
                                                        <th width="5%">id</th>
                                                        <th>@lang('back/msg.name')</th>
                                                        <th>@lang('back/msg.email')</th>
                                                        <th>@lang('back/msg.msg_type')</th>
                                                        <th width="25%">@lang('back/msg.msg')</th>
                                                        <th>@lang('back/layout.added.modified')</th>
                                                        <th width="5%">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach($msgs as $msg)
                                                        <tr>
                                                            <td>{{$msg->id}}</td>
                                                            <td>{{$msg->name}}</td>
                                                            <td>{{$msg->email}}</td>
                                                            @if(app()->getLocale() == 'en')
                                                                @switch($msg->msg_type)
                                                                    @case(1)
                                                                    <td>inquery</td>
                                                                    @break

                                                                    @default
                                                                    <td>complaint</td>
                                                                @endswitch
                                                            @else
                                                                @switch($msg->msg_type)
                                                                    @case(1)
                                                                    <td>استفسار</td>
                                                                    @break

                                                                    @default
                                                                    <td>شكوي</td>
                                                                @endswitch
                                                            @endif
                                                            <td>{{$msg->msg}}</td>
                                                            <td>
                                                                <strong>@lang('back/layout.added.date'): </strong>
                                                                {{$msg->created_at}}
                                                                <br>
                                                                <strong>@lang('back/layout.modified.date'): </strong>
                                                                {{$msg->updated_at}}
                                                            </td>
                                                            <td>
                                                                <a href="mailto:{{$msg->email}}" class="btn btn-primary">@lang('back/msg.reply')</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
            console.log('loc : '+ locale );
            $('#dataTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                }
            });
        });
    </script>
@stop

