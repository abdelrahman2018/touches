@extends('layouts.admin.master')

@section("title")
    {{_lang('app.all_coupons')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('coupon.index')}}"><i class="fa fa-bars"></i> {{_lang('app.list_all_coupons')}}</a></li>
                <li class="active">  {{_lang('app.all_coupons')}}</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title"> {{_lang('app.all_coupons')}}</h1>
                            <div class="box-tools @if(app()->getLocale() == 'en') pull-right @else pull-left @endif">
                                <a href="" data-toggle="modal" data-target="#addModal" type="button" class="btn btn-block btn-primary"> {{_lang('app.add_coupon')}}</a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="5%">id</th>
                                                    <th> {{_lang('app.code')}}</th>
                                                    <th> {{_lang('app.expiration')}}</th>
                                                    <th> {{_lang('app.discount_value')}}</th>
                                                    <th>@lang('back/layout.added.modified')</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($coupons as $coupon)
                                                    <tr>
                                                        <td>{{$coupon->id}}</td>
                                                        <td>{{$coupon->number}}</td>
                                                        <td>{{$coupon->exp}}</td>
                                                        <td>{{$coupon->value}}%</td>
                                                        <td>
                                                            <strong>@lang('back/layout.added.date'): </strong>
                                                            {{$coupon->created_at}}
                                                            <br>
                                                            <strong>@lang('back/layout.modified.date'): </strong>
                                                            {{$coupon->updated_at}}
                                                        </td>
                                                        <td>
                                                            {{--<a data-toggle="modal" data-target="#editModal" data-placement="bottom" title="@lang('back/layout.edit')" href="{{route('coupon.edit', $coupon->id)}}" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-edit" aria-hidden="true"></i></a>--}}
                                                            <a data-toggle="tooltip" data-placement="bottom" title="@lang('back/layout.delete')" href="{{route('coupon.delete', $coupon->id)}}"  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            {{--<form action="{{route('main.destroy', $category->id)}}" method="post">--}}
                                                            {{--@csrf--}}
                                                            {{--{{method_field('DELETE')}}--}}
                                                            {{--</form>--}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>



    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> {{_lang('app.add_coupon')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form method="post" action="{{route('coupon.store')}}" accept-charset="UTF-8">
                        @csrf
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label"> {{_lang('app.code')}}:</label>
                            <input type="text" name="code" class="form-control" id="c-code" readonly>
                            <button type="button" onclick="generateCode()" class="btn btn-xs btn-success">Generate</button>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="form-control-label"> {{_lang('app.expiration')}}:</label>
                            <input type="date" name="exp" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="message-text" class="form-control-label"> {{_lang('app.discount_value')}}:</label>
                            <input type="number" name="val" placeholder="%" class="form-control">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"> {{_lang('app.close')}}</button>
                            <button type="submit" class="btn btn-primary"> {{_lang('app.add_coupon')}}</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

        function generateCode() {
            code = Math.floor(Math.random() *100) + Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5);
            $('#c-code').val(code);
        }
    </script>

    <script>
        $(document).ready(function() {
            var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
            console.log('loc : '+ locale );
            $('#dataTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                }
            });
        });
    </script>
@endsection
