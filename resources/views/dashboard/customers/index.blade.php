@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    Customers
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{route('customer.index')}}"><i class="fa fa-bars"></i> List All Customers</a></li>
                <li class="active">all customers</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">Listing customers </h1>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="5%">id</th>
                                                    <th>email</th>
                                                    <th>Added/Last modified date</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($customers as $customer)
                                                    <tr>
                                                        <td>{{$customer->id}}</td>
                                                        <td>{{$customer->email}}</td>
                                                        <td>
                                                            <strong>Added date: </strong>
                                                            {{$customer->created_at}}
                                                            <br>
                                                            <strong>Modified date: </strong>
                                                            {{$customer->updated_at}}
                                                        </td>
                                                        <td>
                                                            <ul class="nav table-nav">
                                                                <li class="dropdown">
                                                                    <a  data-toggle="dropdown" href="#">
                                                                        Action <span class="caret"></span>
                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        @if($customer->active == 1)
                                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('customer.update', $customer->id)}}">Deactive</a></li>
                                                                        @else
                                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('customer.update', $customer->id)}}">Active</a></li>
                                                                        @endif
                                                                        <li role="presentation"><a data-toggle="tooltip" data-placement="bottom" title="Delete" id="deleteCustomerFrom" href="{{route('customer.delete', $customer->id)}}">Delete</a></li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
            console.log('loc : '+ locale );
            $('#dataTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                }
            });
        });
    </script>
@endsection
