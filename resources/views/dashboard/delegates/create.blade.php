@extends('layouts.admin.master')

@section("title")
    {{_lang('app.add_delegate')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('del.index')}}"><i class="fa fa-bars"></i> {{_lang('app.list_all_delegates')}}</a></li>
                <li class="active">{{_lang('app.add_delegate')}}</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{_lang('app.add_delegate')}}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form id="form-submit" method="POST" action="{{route('del.store')}}" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                @csrf

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.name')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="name" value="{{old('name')}}" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.username')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="username" value="{{old('username')}}" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.phone')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" id="phone-number" name="phone" value="{{old('phone')}}" placeholder="123456789" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.email')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="email" name="email" value="{{old('email')}}"  class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.password')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="password" name="password"  class="form-control field-validate">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.image')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="file" name="image">
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button onclick="validatPhone()" type="submit" class="btn btn-primary">{{_lang('app.add_delegate')}}</button>
                                                    <a href="{{route('del.index')}}" type="button" class="btn btn-default">@lang('back/layout.back')</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection

@section('scripts')
    {{--<script>--}}

        {{--function validatPhone() {--}}
            {{--var number = $('#phone-number').val();--}}
            {{--reg = new RegExp(/^(5|0|3|6|4|9|1|8|7)([0-9]{7})$/);--}}
            {{--res = reg.test(number);--}}
            {{--if(! res)--}}
                {{--alert('incorrect phone number');--}}

        {{--}--}}
    {{--</script>--}}
@stop
