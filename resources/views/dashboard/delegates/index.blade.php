@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    {{--@lang('back/slides.slides')--}}
    {{_lang('app.all_delegates')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('del.index')}}"><i class="fa fa-bars"></i> {{_lang('app.list_all_delegates')}}</a></li>
                <li class="active">  {{_lang('app.all_delegates')}}</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title"> {{_lang('app.all_delegates')}}</h1>
                            <div class="box-tools @if(app()->getLocale() == 'en') pull-right @else pull-left @endif">
                                <a href="{{route('del.create')}}" type="button" class="btn btn-block btn-primary"> {{_lang('app.add_delegate')}}</a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="5%">id</th>
                                                    <th> {{_lang('app.name')}}</th>
                                                    <th> {{_lang('app.username')}}</th>
                                                    <th> {{_lang('app.image')}}</th>
                                                    <th> {{_lang('app.phone')}}</th>
                                                    <th> {{_lang('app.email')}}</th>
                                                    <th> {{_lang('app.status')}}</th>
                                                    <th>@lang('back/layout.added.modified')</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($delegates as $delegate)
                                                    <tr class="trData">
                                                        <td>{{$delegate->id}}</td>
                                                        <td>{{$delegate->name}}</td>
                                                        <td>{{$delegate->username}}</td>
                                                        <td>
                                                            <a href="{{asset($delegate->image)}}" data-fancybox> <img src="{{asset($delegate->image)}}" width="100%"></a>
                                                        </td>
                                                        <td>{{$delegate->phone}}</td>
                                                        <td>{{$delegate->email}}</td>
                                                        <td><span class="label label-{{$delegate->active == 1? 'success':'danger'}}">{{$delegate->active == 1? 'Active':'not Active'}}</span></td>
                                                        <td>
                                                            <strong>@lang('back/layout.added.date'): </strong>
                                                            {{$delegate->created_at}}
                                                            <br>
                                                            <strong>@lang('back/layout.modified.date'): </strong>
                                                            {{$delegate->updated_at}}
                                                        </td>
                                                        <td>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="@lang('back/layout.edit')" href="{{route('del.show', $delegate->id)}}" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-eye" aria-hidden="true"></i></a>
                                                            <a data-toggle="tooltip"  data-placement="bottom" title="@lang('back/layout.delete')" href="{{route('del.delete', $delegate->id)}}"  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            {{--<form action="{{route('main.destroy', $category->id)}}" method="post">--}}
                                                            {{--@csrf--}}
                                                            {{--{{method_field('DELETE')}}--}}
                                                            {{--</form>--}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
            console.log('loc : '+ locale );
            $('#dataTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                }
            });
        });
    </script>

    {{--<script>--}}
       {{--function reload() {--}}
           {{--var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';--}}
           {{--$('#dataTable').DataTable({--}}
               {{--processing: true,--}}
               {{--serverSide: true,--}}
               {{--ajax: '{{ route('del.data') }}',--}}
               {{--columns: [--}}
                   {{--{data: 'id', name: 'id'},--}}
                   {{--{data: 'name', name: 'name'},--}}
                   {{--{data: 'username', name: 'username'},--}}
                   {{--{data: 'email', 'name': 'email'},--}}
                   {{--{data: 'phone', name: 'phone'},--}}
                   {{--{data: 'image', name: 'image'},--}}


                   {{--// {data: 'link', name: 'link'},--}}
                   {{--// {data: 'action', name: 'action'},--}}
               {{--],--}}
               {{----}}
               {{--"language": {--}}
                   {{--"url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"--}}
               {{--}--}}
           {{--});--}}
       {{--}--}}
    {{--</script>--}}
@endsection
