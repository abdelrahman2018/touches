@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    {{--@lang('back/layout.edit')--}}
    {{_lang('app.view_delegate')}}
@endsection

@section('styles')
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link>
@stop

@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('del.index')}}"><i class="fa fa-bars"></i>{{_lang('app.list_all_delegates')}}</a></li>
                <li class="active">{{_lang('app.view_delegate')}}</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{_lang('app.view_delegate')}} </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <ul class="nav nav-tabs">
                                            <li><a data-toggle="tab" href="#edit">{{_lang('app.edit')}}</a></li>
                                            <li class="active"><a data-toggle="tab" href="#orders">{{_lang('app.orders')}}</a></li>
                                            <li><a data-toggle="tab" href="#map">{{_lang('app.view_on_map')}}</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div id="edit" class="tab-pane fade  ">
                                                <h3>{{_lang('app.edit_delegate')}}</h3>
                                                <form method="POST" action="{{route('del.update', $delegate->id)}}" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                    @csrf


                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.name')}}</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="name" value="{{$delegate->name}}" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.username')}}</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="username" value="{{$delegate->username}}" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.phone')}}</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" id="phone-number" name="phone" value="{{$delegate->phone}}" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.email')}}</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="email" name="email" value="{{$delegate->email}}"  class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <hr style="font-size: larger;">

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.status')}}</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <select name="status" class="form-control field-validate">
                                                                <option value="0" {{$delegate->active == 0? 'selected':''}}>{{_lang('app.not_active')}}</option>
                                                                <option value="1" {{$delegate->active == 1? 'selected':''}}>{{_lang('app.active')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.do_you_want_change_password')}}</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="checkbox" name="passCheck" value="yes" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.password')}}</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="password" name="password"  class="form-control field-validate">
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/layout.image')</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input id="newImage" name="image" type="file">
                                                            <img src="{{asset('public/uploads/delegates/'.$delegate->image)}}" width="100px">
                                                        </div>
                                                    </div>

                                                    <!-- /.box-body -->
                                                    <div class="box-footer text-center">
                                                        <button type="submit" class="btn btn-primary">@lang('back/layout.edit')</button>
                                                        <a href="{{route('product.index')}}" type="button" class="btn btn-default">@lang('back/layout.back')</a>
                                                    </div>
                                                    <!-- /.box-footer -->
                                                </form>


                                            </div>

                                            <div id="orders" class="tab-pane fade in active">
                                                <h3>{{_lang('app.orders')}}</h3>

                                                <div class="text-center">
                                                    <form action="{{route('orders.status.search')}}" method="post" class="form-horizontal form-validate">
                                                        @csrf

                                                        <input type="hidden" name="del_id" value="{{$delegate->id}}">
                                                        <div class="form-group">
                                                            <label>{{_lang('app.status')}}</label>
                                                            <select name="status_code" >
                                                                @for($i = 1; $i < count($orderStatus); $i++)
                                                                    <option value="{{$i}}">{{$orderStatus[$i]['text']}}</option>
                                                                @endfor
                                                            </select>
                                                            <button class="">{{_lang('app.search')}}</button>
                                                        </div>
                                                    </form>
                                                </div>


                                                <table id="dataTable" class="table table-bordered table-striped">

                                                    <thead>
                                                    <tr>
                                                        <th width="5%">id</th>
                                                        <th>{{_lang('app.date')}}</th>
                                                        <th width="15%">{{_lang('app.address')}}</th>
                                                        <th>{{_lang('app.client')}}</th>
                                                        <th>{{_lang('app.status')}}</th>
                                                        <th>{{_lang('app.payment_method')}}</th>
                                                        <th>{{_lang('app.total')}}</th>
                                                        <th>{{_lang('app.discount')}}</th>
                                                        <th>{{_lang('app.order_added_date')}}</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
{{--                                                    {{dd($orderStatus)}}--}}
                                                    @foreach($orders as $order)
                                                        @php($address = json_decode($order->address))
                                                        <tr>
                                                            <td>{{$order->id}}</td>
                                                            <td>{{$order->date}}</td>
                                                            <td>
                                                                <strong>City:</strong>
                                                                {{$address->city}}
                                                                <br>

                                                                <strong>Area:</strong>
                                                                {{$address->area}}
                                                                <br>

                                                                <strong>Street:</strong>
                                                                {{$address->street}}
                                                                <br>

                                                                <strong>Building:</strong>
                                                                {{$address->building}}
                                                                <br>

                                                                <strong>Floor:</strong>
                                                                {{$address->floor}}
                                                                <br>
                                                            </td>
                                                            <td>{{$order->user->name}}</td>

                                                            <td><span class="label label-{{$orderStatus[$order->status_code]['label']}}">{{$orderStatus[$order->status_code]['text']}}</span></td>

                                                            <td>{{$orderPayment[$order->payment_type]}}</td>

                                                            <td>{{$order->total}}</td>

                                                            <td>{{$order->coupon? $order->coupon->val:0}}%</td>

                                                            <td>{{$order->created_at}}</td>

                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>


                                            <div id="map" class="tab-pane fade">
                                               {{--<example-component></example-component>--}}

                                                <button onclick="updateMap()" class="btn btn-success">{{_lang('app.update_map')}}</button>

                                                <div id="del_map">
                                                    <iframe src="https://maps.google.com/maps?q={{$location? $location->lat:''}}%2C{{$location? $location->lng:''}}&t=&z=13&ie=UTF8&iwloc=&output=embed" height="450" width="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                                                </div>

                                                <div class="form-group">
                                                    <div id="mapCanvas"></div>
                                                </div>

                                                <div class="form-group">
                                                    <input type="hidden" id="lat" name="lat" value="29.996533" >
                                                    <input type="hidden" id="lng" name="lng" value="31.207152" >
                                                </div>

                                            </div>
                                        </div>


                                        <div class="box-body">


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection

@section('scripts')
    <script>

        function updateMap() {
            $.ajax({
                type: 'post',
                url: "{{route('del.loc')}}",
                data: {
                    _token: '{{csrf_token()}}',
                    delegate_id: '{{$delegate->id}}',
                    // name: 'fddf'
                },
                success: function (data) {
                    console.log(data);
                    $('#del_map').html('');
                    $('#del_map').html(
                        '<iframe src="https://maps.google.com/maps?q='+data.data.lat+'%2C'+data.data.lng+'&t=&z=13&ie=UTF8&iwloc=&output=embed" height="450" width="100%" frameborder="0" style="border:0" allowfullscreen="" ></iframe>'
                    );
                }
            });
        }

    </script>

    <script>
        $(document).ready(function() {
            var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
            console.log('loc : '+ locale );
            $('#dataTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                }
            });
        });
    </script>

    {{--<script>--}}
        {{--var position = [29.996533, 31.207152];--}}

        {{--function initialize() {--}}
            {{--var latlng = new google.maps.LatLng(position[0], position[1]);--}}
            {{--var myOptions = {--}}
                {{--zoom: 16,--}}
                {{--center: latlng,--}}
                {{--mapTypeId: google.maps.MapTypeId.ROADMAP--}}
            {{--};--}}
            {{--var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));--}}
            {{--google.maps.event.addListener(searchBox,'places_changed',function(){--}}
                {{--var places = searchBox.getPlaces();--}}
                {{--var bounds = new google.maps.LatLngBounds();--}}
                {{--var i, place;--}}
                {{--for(i=0; place=places[i];i++){--}}
                    {{--bounds.extend(place.geometry.location);--}}
                    {{--marker.setPosition(place.geometry.location); //set marker position new...--}}
                    {{--$('#lat').val(place.geometry.location.lat());--}}
                    {{--$('#lng').val(place.geometry.location.lng());--}}
                {{--}--}}
                {{--map.fitBounds(bounds);--}}
                {{--map.setZoom(15);--}}

            {{--});--}}

            {{--map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);--}}

            {{--marker = new google.maps.Marker({--}}
                {{--position: latlng,--}}
                {{--map: map,--}}
                {{--title: "Latitude:"+position[0]+" | Longitude:"+position[1]--}}
            {{--});--}}

            {{--google.maps.event.addListener(map, 'click', function(event) {--}}
                {{--var result = [event.latLng.lat(), event.latLng.lng()];--}}
                {{--transition(result);--}}
            {{--});--}}
            {{--$('#lat').val(position[0]);--}}
            {{--$('#lng').val(position[1]);--}}
        {{--}--}}

        {{--//Load google map--}}
        {{--google.maps.event.addDomListener(window, 'load', initialize);--}}


        {{--var numDeltas = 100;--}}
        {{--var delay = 10; //milliseconds--}}
        {{--var i = 0;--}}
        {{--var deltaLat;--}}
        {{--var deltaLng;--}}

        {{--function transition(result){--}}
            {{--i = 0;--}}
            {{--deltaLat = (result[0] - position[0])/numDeltas;--}}
            {{--deltaLng = (result[1] - position[1])/numDeltas;--}}
            {{--$('#lat').val(position[0]);--}}
            {{--$('#lng').val(position[1]);--}}
            {{--moveMarker();--}}
        {{--}--}}

        {{--function moveMarker(){--}}
            {{--position[0] += deltaLat;--}}
            {{--position[1] += deltaLng;--}}
            {{--var latlng = new google.maps.LatLng(position[0], position[1]);--}}
            {{--marker.setTitle("Latitude:"+position[0]+" | Longitude:"+position[1]);--}}
            {{--marker.setPosition(latlng);--}}

            {{--$('#lat').val(position[0]);--}}
            {{--$('#lng').val(position[1]);--}}
            {{--if(i!=numDeltas){--}}
                {{--i++;--}}
                {{--setTimeout(moveMarker, delay);--}}
            {{--}--}}

        {{--}--}}
    {{--</script>--}}
@stop
