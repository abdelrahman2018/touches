
@if(session()->has('success'))
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="icon fa fa-check" aria-hidden="true"></span>
        {{session()->get('success')}}
    </div>

@endif

@if($errors->any())
    @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="icon fa fa-times" aria-hidden="true"></span>
            {{$error}}
        </div>
    @endforeach
@endif
