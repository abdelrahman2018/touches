@extends('layouts.admin.master')

@section("title")
    {{_lang('app.all_orders')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> {{_lang('app.dashboard')}}</a></li>
                <li><a href="{{route('order.index')}}"><i class="fa fa-bars"></i>{{_lang('app.list_all_orders')}}</a></li>
                <li class="active">{{_lang('app.all_orders')}}</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">{{_lang('app.all_orders')}}</h1>
                        </div>

                        <div class="topbox-in ibox-content">
                            <h2 class="neworders">{{_lang('app.search')}}</h2>
                            {{--<span class="clickbot"> <i class="a-minus"></i> </span>--}}
                            <div class="disbox">
                                <form method="POST" id="search-form" class="" role="form">
                                    {{--@csrf--}}
                                    <div class="row">
                                        <div class="col-sm-3 topbox nobord">
                                            <label for="name">{{_lang('app.client_name')}}</label>
                                            <input type="text" class="form-control" name="client" id="name"
                                                   placeholder="Name">
                                        </div>


                                        <div class="col-sm-3 topbox nobord">
                                            <label for="email">{{_lang('app.status')}}</label>
                                            <select class="form-control" name="status_code" id="status">
                                                <option value="">{{_lang('app.status_search')}}</option>
                                                @for($i = 0; $i < count($orderStatus); $i++)
                                                    <option value="{{$i}}">{{$orderStatus[$i]['text']}}</option>
                                                @endfor
                                            </select>
                                        </div>

                                        <div class="col-sm-3 topbox nobord">
                                            <label>{{_lang('app.from')}}</label>
                                            <div class="input-group date">
                                                <input max="2019-05-05" type="date" name="from" id="from" value="11/07/2000"
                                                       class="form-control ">
                                                <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 topbox nobord">
                                            <label>{{_lang('app.to')}}</label>
                                            <div class="input-group date">
                                                <input type="date" class="form-control " value="11/07/2050" name="to" id="to"
                                                       autocomplete="off">
                                                <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 topbox nobord">
                                            <div class="input-group date col-2">
                                                <button id="sumbit_search" type="button" class="btn btn-primary">{{_lang('app.search')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!--row-->
                                </form>

                                <form>
                                    <div class="col-sm-3 topbox nobord">
                                        <label>{{_lang('app.assign_to')}}</label>
                                        <select name="del" id="assign" class="form-control">
                                            {{--<option value="">Assign To</option>--}}
                                            @foreach($delegates as $delegate)
                                                <option value="{{$delegate->id}}">{{$delegate->name}}</option>
                                            @endforeach
                                        </select>

                                        <div class="input-group date col-2">
                                            <button onclick="assignOrders()" type="button" class="btn btn-primary">{{_lang('app.assign')}}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--disbox-->

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">
                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                    <tr>
                                                        <th class="check">
                                                        <input type="checkbox" id="flowcheckall" value="" />&nbsp;
                                                        </th>
                                                        <th>id</th>
                                                        <th>{{_lang('app.date')}}</th>
                                                        <th width="15%">{{_lang('app.address')}}</th>
                                                        <th>{{_lang('app.client')}}</th>
                                                        <th>{{_lang('app.status')}}</th>
                                                        <th>{{_lang('app.payment_method')}}</th>
                                                        <th>{{_lang('app.total')}}</th>
                                                        <th>{{_lang('app.discount')}}</th>
                                                        <th>{{_lang('app.order_total')}}</th>
                                                        <th>{{_lang('app.order_date')}}</th>
                                                    </tr>
                                                </thead>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            </div>
        </section>

    </div>

@endsection

@section('scripts')

    <script>
        function  assignOrders() {
            console.log(1);
            var selectedDelegate = $('#assign option:selected').val();
            var selectedOrders = [];
            $.each($("input[name='order_id']:checked"), function(){
                selectedOrders.push($(this).val());
            });

            console.log(selectedOrders);

            $.ajax({
                type: 'post',
                url: "{{route('order.assign')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    del_id: selectedDelegate,
                    orders: selectedOrders
                },

                success: function (data) {
                    $('#dataTable').dataTable().fnDestroy();
                    reload();
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'Your orders assigned',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }

            });
        }
    </script>

    <script>
        var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';

        function reload(){
            oTableStaticFlow = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                dom: 'Bfrtip',
                'bSortable': false,
                'aTargets': [0],
                "searching": false,
                ajax: {
                    url: '{{route('order.data')}}',
                    data: {
                        _token: '{{csrf_token()}}',
                        client: $('#name').val(),
                        from: $('#from').val(),
                        to: $('#to').val(),
                        status: $('#status').val()
                    },
                    method: 'post'
                },
                buttons: [
                    'excel'
                ],
                columns: [
                    { data: 'select', name: 'select', sortable: false },
                    { data: 'id', name: 'id'},
                    { data: 'date', name: 'date' },
                    { data: 'address', name: 'address' },
                    { data: 'user', name: 'user.name' },
                    { data: 'status', name: 'status'},
                    { data: 'payment_type', name: 'payment_type'},
                    { data: 'order_total', name: 'order_total'},
                    { data: 'coupon_id', name: 'coupon_id'},
                    { data: 'total', name: 'total'},
                    { data: 'created_at', name: 'created_at'},
                ],

                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                }

            });
        }

        reload();

        $('#sumbit_search').on('click', function () {
            $('#dataTable').dataTable().fnDestroy();
            reload();
        });

        $("#flowcheckall").click(function () {
            var cols = oTableStaticFlow.column(0).nodes(),
                targetCls = [];
            for (var s = 0; s < cols.length; s += 1) {
                if (cols[s].querySelector("input[type='checkbox']") != null)
                    targetCls.push(cols[s])
            }
                state = this.checked;

            for (var i = 0; i < targetCls.length; i += 1) {
                targetCls[i].querySelector("input[type='checkbox']").checked = state;
            }
        });
    </script>

@endsection
