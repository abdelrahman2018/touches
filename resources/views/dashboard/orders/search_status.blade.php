@extends('layouts.admin.master')

@section("title")
   {{_lang('app.all_orders')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        {{--<section class="content-header">--}}


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">{{_lang('app.all_orders')}}</h1>
                            <a href="{{route('orders.status.exp', ['del_id' => $del_id, 'status' => $status_code])}}" class="btn btn-success">{{_lang('app.export_excel')}}</a>
                            {{--<div class="box-tools @if(app()->getLocale() == 'en') pull-right @else pull-left @endif">--}}
                            {{--<a href="" data-toggle="modal" data-target="#addModal" type="button" class="btn btn-block btn-primary">Add Coupon</a>--}}
                            {{--</div>--}}
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">
                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="5%">id</th>
                                                    <th>{{_lang('app.date')}}</th>
                                                    <th width="15%">{{_lang('app.address')}}</th>
                                                    <th>{{_lang('app.client')}}</th>
                                                    <th>{{_lang('app.status')}}</th>
                                                    <th>{{_lang('app.payment_method')}}</th>
                                                    <th>{{_lang('app.total')}}</th>
                                                    <th>{{_lang('app.discount')}}</th>
                                                    <th>{{_lang('app.order_added_date')}}</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                {{--                                                    {{dd($orderStatus)}}--}}
                                                @foreach($orders as $order)
                                                    @php($address = json_decode($order->address))
                                                    <tr>
                                                        <td>{{$order->id}}</td>
                                                        <td>{{$order->date}}</td>
                                                        <td>
                                                            <strong>{{_lang('app.city')}}:</strong>
                                                            {{$address->city}}
                                                            <br>

                                                            <strong>{{_lang('app.area')}}:</strong>
                                                            {{$address->area}}
                                                            <br>

                                                            <strong>{{_lang('app.street')}}:</strong>
                                                            {{$address->street}}
                                                            <br>

                                                            <strong>{{_lang('app.building')}}:</strong>
                                                            {{$address->building}}
                                                            <br>

                                                            <strong>{{_lang('app.floor')}}:</strong>
                                                            {{$address->floor}}
                                                            <br>
                                                        </td>
                                                        <td>{{$order->user->name}}</td>

                                                        <td><span class="label label-{{$orderStatus[$order->status_code]['label']}}">{{$orderStatus[$order->status_code]['text']}}</span></td>

                                                        <td>{{$orderPayment[$order->payment_type]}}</td>

                                                        <td>{{$order->total}}</td>

                                                        <td>{{$order->coupon? $order->coupon->val:0}}%</td>

                                                        <td>{{$order->created_at}}</td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

            <a href="{{\Illuminate\Support\Facades\URL::previous()}}" class="btn btn-primary btn-lg">{{_lang('app.back')}}</a>

        </section>

    </div>


@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
            console.log('loc : '+ locale );
            $('#dataTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                }
            });
        });
    </script>
@stop