@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    @lang('back/layout.create')
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('page.index')}}"><i class="fa fa-bars"></i> @lang('back/pages.list')</a></li>
                <li class="active">Add Page</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">@lang('back/layout.create')</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="{{route('page.store')}}" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Name (English)</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="name_en" class="form-control field-validate">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Name (عربى)</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="name_ar" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="desc" class="col-sm-2 col-md-3 control-label">description (English)</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <textarea name="desc_en" id="desc_en"></textarea>
                                                    </div>
                                                </div>
                                                <script>
                                                    CKEDITOR.replace( 'desc_en' );
                                                </script>

                                                <div class="form-group">
                                                    <label for="desc" class="col-sm-2 col-md-3 control-label">description (عربى)</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <textarea name="desc_ar" id="desc_ar"></textarea>
                                                    </div>
                                                </div>
                                                <script>
                                                    CKEDITOR.replace( 'desc_ar' );
                                                </script>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Image</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input id="newImage" name="image" type="file">
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">Add Page</button>
                                                    <a href="{{route('page.index')}}" type="button" class="btn btn-default">Back</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection
