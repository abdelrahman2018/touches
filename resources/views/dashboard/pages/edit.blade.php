@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    @lang('back/layout.edit')
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('page.index')}}"><i class="fa fa-bars"></i> @lang('back/pages.list')</a></li>
                <li class="active">Add Page</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">@lang('back/layout.edit') </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="{{route('page.update', $page->id)}}" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/pages.title.en')</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="name_en" value="{{$page->translate('en')->title}}" class="form-control field-validate">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/pages.title.ar')</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="name_ar" value="{{$page->translate('ar')->title}}" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="desc" class="col-sm-2 col-md-3 control-label">@lang('back/pages.description.en')</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <textarea name="desc_en"  id="desc_en">
                                                            {{$page->translate('en')->description}}
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <script>
                                                    CKEDITOR.replace( 'desc_en' );
                                                </script>

                                                <div class="form-group">
                                                    <label for="desc" class="col-sm-2 col-md-3 control-label">@lang('back/pages.description.ar')</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <textarea name="desc_ar" id="desc_ar">
                                                            {{$page->translate('ar')->description}}
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <script>
                                                    CKEDITOR.replace( 'desc_ar' );
                                                </script>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/layout.image')</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input id="newImage" name="image" type="file">
                                                        <img src="{{asset('public/uploads/pages/'.$page->image)}}" width="100px">
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">@lang('back/layout.edit')</button>
                                                    <a href="{{url()->previous()}}" type="button" class="btn btn-default">@lang('back/layout.back')</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection
