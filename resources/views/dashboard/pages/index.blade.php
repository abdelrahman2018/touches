@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    @lang('back/pages.index')
@endsection

@section('styles')
    <style>
        .col-md-4{
            width: -webkit-fill-available !important;
        }

        .form-horizontal .control-label {
            text-align: -webkit-auto;
        }
    </style>
@stop

@section('content')
    {{--{{dd($locales)}}--}}
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('page.index')}}"><i class="fa fa-bars"></i> @lang('back/pages.list')</a></li>
                <li class="active">all pages</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">Listing pages </h1>
                            <div class="box-tools @if(app()->getLocale() == 'en') pull-right @else pull-left @endif">
                                {{--<button data-toggle="modal" data-target="#createModal" type="button" class="btn btn-block btn-primary">Add New Page</button>--}}
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="10%">id</th>
                                                    <th>@lang('back/layout.name')</th>
                                                    <th>@lang('back/layout.image')</th>
                                                    <th>@lang('back/layout.added.modified')</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($pages as $page)
                                                    <tr>
                                                        <td>{{$page->id}}</td>
                                                        <td>{{$page->title}}</td>
                                                        <td>
                                                            <a href="{{asset('public/uploads/pages/'.$page->image)}}" data-fancybox> <img src="{{asset('public/uploads/pages/'.$page->image)}}" width="100%"></a>
                                                        </td>
                                                        <td>
                                                            <strong>@lang('back/layout.added.date'): </strong>
                                                            {{$page->created_at}}
                                                            <br>
                                                            <strong>@lang('back/layout.modified.date'): </strong>
                                                            {{$page->updated_at}}
                                                        </td>
                                                        <td>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="@lang('back/layout.edit')" class="badge bg-light-blue" href="{{route('page.edit', $page->id)}}" style="background-color: #0d95e8"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                                            {{--<a data-toggle="tooltip" data-placement="bottom" title="Delete" href="{{route('page.delete', $page->id)}}"  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>--}}
                                                            {{--<form action="{{route('main.destroy', $category->id)}}" method="post">--}}
                                                            {{--@csrf--}}
                                                            {{--{{method_field('DELETE')}}--}}
                                                            {{--</form>--}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>



@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
            console.log('loc : '+ locale );
            $('#dataTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                }
            });
        });
    </script>
@stop
