@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    {{--@lang('back/layout.create')--}}
    {{_lang('app.add_product')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('product.index')}}"><i class="fa fa-bars"></i> {{_lang('app.list_all_products')}}</a></li>
                <li class="active">{{_lang('app.add_product')}}</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{_lang('app.add_product')}}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="{{route('product.store')}}" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                @csrf

                                                @foreach($locales as $loc => $val)
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.name')}} ({{$val}})</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="name[{{$loc}}]" value="{{old("name['.$loc.']")}}" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="desc" class="col-sm-2 col-md-3 control-label">{{_lang('app.description')}} ({{$val}})</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <textarea name="desc[{{$loc}}]" id="desc_{{$loc}}"></textarea>
                                                        </div>
                                                    </div>

                                                    <script>
                                                        CKEDITOR.replace( 'desc_{{$loc}}' );
                                                    </script>
                                                @endforeach

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.price')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="price" value="{{old('price')}}" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.discount')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="discount" value="{{old('discount')}}" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.size')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="size" value="{{old('size')}}" class="form-control field-validate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.unit')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <select name="unit" class="form-control field-validate">
                                                            @foreach($units as $unit)
                                                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.company')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <select name="company" class="form-control field-validate">
                                                            @foreach($companies as $company)
                                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.packet')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <select name="packet" class="form-control field-validate">
                                                            @foreach($packets as $packet)
                                                                <option value="{{$packet->id}}">{{$packet->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.image')}}</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="file" name="image">
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">{{_lang('app.add')}}</button>
                                                    <a href="{{route('product.index')}}" type="button" class="btn btn-default">@lang('back/layout.back')</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection
