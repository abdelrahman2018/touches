@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    {{--@lang('back/layout.edit')--}}
    Edit Role
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('role.index')}}"><i class="fa fa-bars"></i> List all Roles</a></li>
                <li class="active">Edit Role</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Edit Role </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="{{route('role.update', $role->id)}}" accept-charset="UTF-8" class="form-horizontal form-validate">
                                                @csrf

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Name</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="text" name="name" value="{{$role->name}}" class="form-control field-validate">
                                                    </div>
                                                </div>


                                                <div class="col-sm-12 topbox">
                                                    <div  class="inneradmin">
                                                        <h4 class="admintitle"> Permissions</h4>
                                                        <div  id="permissions_add" >
                                                            @foreach($permissions as $permission)
                                                                <div class="col-sm-2 topbox nobord">
                                                                    <label class="col-sm-2 topbox">
                                                                        <input value="{{$permission->name}}" type="checkbox" {{$role->hasPermissionTo($permission->name)? 'checked': ''}} class="form_permissions" name="permissions[]"><span class="label-text">{{$permission->name}}</span>
                                                                    </label>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">@lang('back/layout.edit')</button>
                                                    <a href="{{route('role.index')}}" type="button" class="btn btn-default">@lang('back/layout.back')</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection
