@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    @lang('back/layout.setting')
@endsection

@section('styles')
    <style>
        #map {
            height: 100%;
        }
    </style>

    {{--<style>--}}
        {{--#mapCanvas > div{--}}
            {{--height: 70% !important;--}}
            {{--position: relative !important;--}}
            {{--overflow: -webkit-paged-y !important;--}}
        {{--}--}}
    {{--</style>--}}

    {{--<style>--}}
        {{--.form-horizontal .control-label {--}}
            {{--padding-top: 17px !important;--}}
        {{--}--}}
    {{--</style>--}}
@stop


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i>@lang('back/layout.dashboard')</a></li>
            <li class="active">@lang('back/layout.setting')</li>
        </ol>
    </section>


    <section class="content">

        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    {{--<div class="box-header">--}}
                        {{--<h3 class="box-title">Settings</h3>--}}
                    {{--</div>--}}

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box box-info">
                                    <!--<div class="box-header with-border">
                                      <h3 class="box-title">Setting</h3>
                                    </div>-->
                                    <!-- /.box-header -->
                                    <!-- form start -->
                                    <div class="box-body">

                                        @include('dashboard.includes.feedback')

                                        <form method="POST" action="{{route('settings.update')}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data"><input name="_token" type="hidden" value="SAoXwG10HIWUXxLghoEVsoGFXl2Xme19oh4UwvhE">
                                           @csrf
                                            <h4>@lang('back/settings.general')</h4>
                                            <hr>


                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/settings.facebook')</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <input class="form-control" id="facebook_url" name="facebook" type="url" value="{{$settings['facebook']}}">
                                                </div>
                                            </div>

                                            {{--<div class="form-group">--}}
                                                {{--<label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/settings.linked')</label>--}}
                                                {{--<div class="col-sm-10 col-md-4">--}}
                                                    {{--<input class="form-control" id="linkedin_url" name="linkedin" type="url" value="{{$settings['linkedin']}}">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                            {{--<div class="form-group">--}}
                                            {{--<label for="name" class="col-sm-2 col-md-3 control-label">Youtube Url</label>--}}
                                            {{--<div class="col-sm-10 col-md-4">--}}
                                            {{--<input class="form-control" id="whatsapp" name="youtube" type="text" value="{{$settings['youtube']}}">--}}
                                            {{--</div>--}}
                                            {{--</div>--}}

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/settings.twitter')</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <input class="form-control" id="twitter_url" name="twitter" type="url" value="{{$settings['twitter']}}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/settings.insta')</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <input class="form-control" id="linked_in" name="instagram" type="url" value="{{$settings['instagram']}}">
                                                </div>
                                            </div>


                                            <hr>
                                            <h4>@lang('back/settings.contact.email')</h4>
                                            <hr>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/settings.contact.label')</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <input class="form-control" id="contact_us_email" name="email" type="email" value="{{$settings['email']}}"></div>
                                            </div>

                                            <hr>

                                            <h4>@lang('back/settings.info')</h4>
                                            <hr>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/settings.phone')</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <input class="form-control" id="phone_no" name="phone" type="text" value="{{$settings['phone']}}">
                                                </div>
                                            </div>

                                        </form>
                                            {{--<div class="form-group">--}}
                                                {{--<label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/settings.address.ar')</label>--}}
                                                {{--<div class="col-sm-10 col-md-4">--}}
                                                    {{--<input class="form-control" id="address" name="address_ar" type="text" value="{{$settings['address_ar']}}">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                            {{--<div class="form-group">--}}
                                                {{--<label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/settings.address.en')</label>--}}
                                                {{--<div class="col-sm-10 col-md-4">--}}
                                                    {{--<input class="form-control" id="address" name="address_en" type="text" value="{{$settings['address_en']}}">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}



                                            {{--<div class="form-group">--}}
                                                {{--<label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/settings.map')</label>--}}
                                                {{--<div class="col-sm-10 col-md-4">--}}
                                                    {{--<input type="text" id="searchmap" class="form-control">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                            {{--<div class="form-group">--}}
                                                {{--<div id="mapCanvas"></div>--}}
                                            {{--</div>--}}

                                            {{--<div class="form-group">--}}
                                                {{--<input type="hidden" id="lat" name="lat" value="29.996533" >--}}
                                                {{--<input type="hidden" id="lng" name="lng" value="31.207152" >--}}
                                            {{--</div>--}}

                                        {{--<input type="hidden" name="latitude" id="latitude">--}}
                                        {{--<input type="hidden" name="longitude" id="longitude">--}}
                                        {{--<input id="pac-input" class="controls" type="text" placeholder="{{_lang('app.search')}}">--}}
                                        {{--<div id="map">--}}

                                        {{--</div>--}}
                                    </div>





                                    <!-- /.box-body -->
                                    <div class="box-footer text-center">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a href="{{url()->previous()}}" type="button" class="btn btn-default">Back</a>
                                    </div>

                                    <!-- /.box-footer -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>

    </div>

@endsection

{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5Jrp9PtHe0WapppUzxbIpMDWMAcV3qE4&libraries=places"></script>--}}
@section('scripts')
    {{--<script type="text/javascript"--}}
            {{--src="//maps.googleapis.com/maps/api/js?key=AIzaSyC5Jrp9PtHe0WapppUzxbIpMDWMAcV3qE4&libraries=places&callback=initAutocomplete" async defer></script>--}}

    {{--<script>--}}
        {{--google.maps.event.addDomListener(window, 'load', initAutocomplete);--}}

        {{--var flat = '{{$settings['lat']}}';--}}
        {{--var flng = '{{$settings['lng']}}';--}}
        {{--var lp;--}}
        {{--function initAutocomplete(flat,flng) {--}}
            {{--// var onClickPositionView = document.getElementById('onClickPositionView');--}}
            {{--var map = document.getElementById('map');--}}

            {{--// Initialize LocationPicker plugin--}}
            {{--window.lp = new locationPicker(map, {--}}
                {{--setCurrentPosition: true, // You can omit this, defaults to true--}}
                {{--lat: parseFloat(flat),--}}
                {{--lng: parseFloat(flng)--}}
            {{--}, {--}}
                {{--zoom: 15 // You can set any google map options here, zoom defaults to 15--}}
            {{--});--}}


            {{--// Listen to map idle event, listening to idle event more accurate than listening to ondrag event--}}
            {{--google.maps.event.addListener(lp.map, 'idle', function (event) {--}}
                {{--// Get current location and show it in HTML--}}
                {{--var location = lp.getMarkerPosition();--}}
                {{--$('#latitude').val(location.lat);--}}
                {{--$('#longitude').val(location.lng);--}}
            {{--});--}}

            {{--var input = document.getElementById('pac-input');--}}
            {{--var searchBox = new google.maps.places.SearchBox(input);--}}
            {{--lp.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);--}}

            {{--// Bias the SearchBox results towards current map's viewport.--}}
            {{--lp.map.addListener('bounds_changed', function () {--}}
                {{--searchBox.setBounds(lp.map.getBounds());--}}
            {{--});--}}

            {{--var markers = [];--}}
            {{--// Listen for the event fired when the user selects a prediction and retrieve--}}
            {{--// more details for that place.--}}
            {{--searchBox.addListener('places_changed', function () {--}}
                {{--var places = searchBox.getPlaces();--}}

                {{--if (places.length == 0) {--}}
                    {{--return;--}}
                {{--}--}}

                {{--// Clear out the old markers.--}}
                {{--markers.forEach(function (marker) {--}}
                    {{--marker.setMap(null);--}}
                {{--});--}}
                {{--markers = [];--}}

                {{--// For each place, get the icon, name and location.--}}
                {{--var bounds = new google.maps.LatLngBounds();--}}
                {{--places.forEach(function (place) {--}}
                    {{--if (!place.geometry) {--}}
                        {{--console.log("Returned place contains no geometry");--}}
                        {{--return;--}}
                    {{--}--}}
                    {{--var icon = {--}}
                        {{--url: place.icon,--}}
                        {{--size: new google.maps.Size(71, 71),--}}
                        {{--origin: new google.maps.Point(0, 0),--}}
                        {{--anchor: new google.maps.Point(17, 34),--}}
                        {{--scaledSize: new google.maps.Size(25, 25)--}}
                    {{--};--}}

                    {{--// Create a marker for each place.--}}
                    {{--markers.push(new google.maps.Marker({--}}
                        {{--map: lp.map,--}}
                        {{--icon: icon,--}}
                        {{--title: place.name,--}}
                        {{--position: place.geometry.location--}}
                    {{--}));--}}

                    {{--if (place.geometry.viewport) {--}}
                        {{--// Only geocodes have viewport.--}}
                        {{--bounds.union(place.geometry.viewport);--}}
                    {{--} else {--}}
                        {{--bounds.extend(place.geometry.location);--}}
                    {{--}--}}
                {{--});--}}
                {{--lp.map.fitBounds(bounds);--}}
            {{--});--}}

        {{--}--}}
    {{--</script>--}}




    {{--<script>--}}
        {{--var position = [{{$settings['lat']}}, {{$settings['lng']}}];--}}

        {{--function initialize() {--}}
            {{--var latlng = new google.maps.LatLng(position[0], position[1]);--}}
            {{--var myOptions = {--}}
                {{--zoom: 16,--}}
                {{--center: latlng,--}}
                {{--mapTypeId: google.maps.MapTypeId.ROADMAP--}}
            {{--};--}}
            {{--var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));--}}
            {{--google.maps.event.addListener(searchBox,'places_changed',function(){--}}
                {{--var places = searchBox.getPlaces();--}}
                {{--var bounds = new google.maps.LatLngBounds();--}}
                {{--var i, place;--}}
                {{--for(i=0; place=places[i];i++){--}}
                    {{--bounds.extend(place.geometry.location);--}}
                    {{--marker.setPosition(place.geometry.location); //set marker position new...--}}
                    {{--$('#lat').val(place.geometry.location.lat());--}}
                    {{--$('#lng').val(place.geometry.location.lng());--}}
                {{--}--}}
                {{--map.fitBounds(bounds);--}}
                {{--map.setZoom(15);--}}

            {{--});--}}

            {{--map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);--}}

            {{--marker = new google.maps.Marker({--}}
                {{--position: latlng,--}}
                {{--map: map,--}}
                {{--title: "Latitude:"+position[0]+" | Longitude:"+position[1]--}}
            {{--});--}}

            {{--google.maps.event.addListener(map, 'click', function(event) {--}}
                {{--var result = [event.latLng.lat(), event.latLng.lng()];--}}
                {{--transition(result);--}}
            {{--});--}}
            {{--$('#lat').val(position[0]);--}}
            {{--$('#lng').val(position[1]);--}}
        {{--}--}}

        {{--//Load google map--}}
        {{--google.maps.event.addDomListener(window, 'load', initialize);--}}


        {{--var numDeltas = 100;--}}
        {{--var delay = 10; //milliseconds--}}
        {{--var i = 0;--}}
        {{--var deltaLat;--}}
        {{--var deltaLng;--}}

        {{--function transition(result){--}}
            {{--i = 0;--}}
            {{--deltaLat = (result[0] - position[0])/numDeltas;--}}
            {{--deltaLng = (result[1] - position[1])/numDeltas;--}}
            {{--$('#lat').val(position[0]);--}}
            {{--$('#lng').val(position[1]);--}}
            {{--moveMarker();--}}
        {{--}--}}

        {{--function moveMarker(){--}}
            {{--position[0] += deltaLat;--}}
            {{--position[1] += deltaLng;--}}
            {{--var latlng = new google.maps.LatLng(position[0], position[1]);--}}
            {{--marker.setTitle("Latitude:"+position[0]+" | Longitude:"+position[1]);--}}
            {{--marker.setPosition(latlng);--}}

            {{--$('#lat').val(position[0]);--}}
            {{--$('#lng').val(position[1]);--}}
            {{--if(i!=numDeltas){--}}
                {{--i++;--}}
                {{--setTimeout(moveMarker, delay);--}}
            {{--}--}}

        {{--}--}}
    {{--</script>--}}

    <script>
        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZwVcV9L6csTaSY_G5FOQpp33Of6C2Txo&callback=initMap"
            async defer></script>
@stop

