@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    @lang('back/layout.edit')
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('slide.index')}}"><i class="fa fa-bars"></i> @lang('back/slides.list.all')</a></li>
                <li class="active">@lang('back/slides.edit')</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">@lang('back/slides.edit') </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="{{route('slide.update', $slide->id)}}" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                @csrf

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">@lang('back/layout.image')</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input id="newImage" name="image" type="file">
                                                        <img src="{{asset('public/uploads/slides/'.$slide->image)}}" width="100%">
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">@lang('back/layout.edit')</button>
                                                    <a href="{{url()->previous()}}" type="button" class="btn btn-default">@lang('back/layout.back')</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection
