@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    @lang('back/slides.slides')
@endsection


@section('content')

    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('slide.index')}}"><i class="fa fa-bars"></i> @lang('back/slides.list.all')</a></li>
                <li class="active">@lang('back/slides.list.all')</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">@lang('back/slides.list.all') </h1>
                            <div class="box-tools @if(app()->getLocale() == 'en') pull-right @else pull-left @endif">
                                <a href="{{route('slide.create')}}" type="button" class="btn btn-block btn-primary">@lang('back/slides.add')</a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                    <tr>
                                                        <th width="10%">id</th>
                                                        <th>@lang('back/slides.image')</th>
                                                        <th>@lang('back/layout.added.modified')</th>
                                                        <th width="10%">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach($slides as $slide)
                                                        <tr>
                                                            <td>{{$slide->id}}</td>
                                                            <td>
                                                                <a href="{{asset('public/uploads/slides/'.$slide->image)}}" data-fancybox> <img src="{{asset('public/uploads/slides/'.$slide->image)}}" width="100%"></a>
                                                            </td>
                                                            <td>
                                                                <strong>@lang('back/layout.added.date'): </strong>
                                                                {{$slide->created_at}}
                                                                <br>
                                                                <strong>@lang('back/layout.modified.date'): </strong>
                                                                {{$slide->updated_at}}
                                                            </td>
                                                            <td>
                                                                <a data-toggle="tooltip" data-placement="bottom" title="@lang('back/layout.edit')" href="{{route('slide.edit', $slide->id)}}" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                                                <a data-toggle="tooltip" data-placement="bottom" title="@lang('back/layout.delete')" href="{{route('slide.delete', $slide->id)}}"  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                                {{--<form action="{{route('main.destroy', $category->id)}}" method="post">--}}
                                                                    {{--@csrf--}}
                                                                    {{--{{method_field('DELETE')}}--}}
                                                                {{--</form>--}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';
            console.log('loc : '+ locale );
            $('#dataTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                }
            });
        });
    </script>
@endsection
