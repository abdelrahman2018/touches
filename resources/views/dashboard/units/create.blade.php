@extends('layouts.admin.master')

@section("title")
    {{--{{_lang('app.blog')}}--}}
    {{--@lang('back/layout.create')--}}
    {{_lang('app.add_unit')}}
@endsection


@section('content')
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> @lang('back/layout.dashboard')</a></li>
                <li><a href="{{route('unit.index')}}"><i class="fa fa-bars"></i> {{_lang('app.list_all_units')}}</a></li>
                <li class="active">{{_lang('app.add_unit')}}</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{_lang('app.add_unit')}} </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        @include('dashboard.includes.feedback')
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="{{route('unit.store')}}" accept-charset="UTF-8" class="form-horizontal form-validate">
                                                @csrf

                                                @foreach($locales as $loc => $val)
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">{{_lang('app.name')}} ({{$val}})</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="name[{{$loc}}]" value="{{old("name['.$loc.']")}}" class="form-control field-validate">
                                                        </div>
                                                    </div>
                                                @endforeach

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">{{_lang('app.add')}}</button>
                                                    <a href="{{route('unit.index')}}" type="button" class="btn btn-default">@lang('back/layout.back')</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

@endsection
