@extends('layouts.front.master')


@section('title')
    <title>About us</title>
@stop

@section('content')
    <main id="main">

        <!--==========================
          About Section
        ============================-->
        <section class="about" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>{{$about->title}}</h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 about-img">
                        <img src="{{asset('public/uploads/pages/'.$about->image)}}" alt="">
                    </div>

                    <div class="col-lg-8 content">
                        {!! $about->description !!}
                    </div>
                </div>

            </div>
        </section><!-- #about -->


    </main>
@stop