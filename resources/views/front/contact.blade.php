@extends('layouts.front.master')


@section('title')
    <title>Contact Us</title>
@stop

@section('content')
    <main id="main">

        <!--==========================
      Contact Section
    ============================-->
        <section class="contact wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>اتصل بنا</h2>
                </div>

                <div class="row contact-info">
                    <div class="col-lg-5">
                        <div class="contact-address">
                            <i class="ion-ios-location-outline"></i>
                            <h3>العنوان</h3>
                            @if(app()->getLocale() == 'ar')
                                <address>{{$settings['address_ar']}}</address>
                            @else
                                <address>{{$settings['address_en']}}</address>
                            @endif

                        </div>
                        <div class="contact-phone">
                            <i class="ion-ios-telephone-outline"></i>
                            <h3>رقم الجوال</h3>
                            <p><a>{{$settings['phone']}}</a></p>
                        </div>
                        <div class="contact-email">
                            <i class="ion-ios-email-outline"></i>
                            <h3>البريد الالكتروني</h3>
                            <p><a href="mailto:{{$settings['email']}}">{{$settings['email']}}</a></p>
                        </div>
                        <div class="social-links">
                            <a href="{{$settings['twitter']}}" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="{{$settings['facebook']}}" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="{{$settings['linkedin']}}" class="linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="{{$settings['instagram']}}" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="container">
                            <div class="form">

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <!-- Form itself -->
                                <form action="{{route('contact.send')}}" method="post" class="well" id="contactForm" >
                                    @csrf
                                    <div class="control-group">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" placeholder="الاسم" id="name">
                                            <p class="help-block"></p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="email" name="email" class="form-control" placeholder="البريد الالكتروني" id="email">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <select name="msg_type" class="form-control">
                                                <option value="">نوع الرسالة</option>
                                                <option value="1">استفسار</option>
                                                <option value="0">شكوى</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                        <textarea name="message" rows="10" cols="100" class="form-control" placeholder="الرسالة" id="message"  style="resize:none"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">إرسال</button><br />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container mb-4 map">
                <iframe src="https://maps.google.com/maps?q={{$settings['lat']}}%2C{{$settings['lng']}}&t=&z=13&ie=UTF8&iwloc=&output=embed" height="350" width="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>
            </div>

        </section><!-- #contact -->


    </main>
@stop