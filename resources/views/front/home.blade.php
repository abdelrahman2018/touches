
@extends('layouts.front.master')

@section('title')
    <title>katheeb</title>
@stop


@section('content')
    <!--==========================
    Intro Section
  ============================-->
    <section id="intro">
        <div class="container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @for($i = 0; $i < count($slides); $i++)
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="{{$i == 0? 'active' : ''}}"></li>
                    @endfor
                </ol>
                <div class="carousel-inner">
                    @for($i = 0; $i < count($slides); $i++)
                        <div class="carousel-item {{$i == 0? 'active': ''}}">
                            <img src="{{asset('public/uploads/slides/'.$slides[$i]->image)}}" class="d-block w-100" alt="...">
                        </div>
                    @endfor
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
    <!-- #intro -->

    <main id="main">

        <!--==========================
          About Section
        ============================-->
        <section id="about" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>{{$about->title}}</h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 about-img">
                        <img src="{{asset('public/uploads/pages/'.$about->image)}}" alt="">
                    </div>

                    <div class="col-lg-8 content">
                        {{strip_tags(html_entity_decode($about->description))}}
                    </div>
                </div>

            </div>
        </section><!-- #about -->


        <!--==========================
          Services Section
        ============================-->
        <section id="services">
            <div class="container">
                <div class="section-header">
                    <h2>شركاتنا</h2>
                </div>

                <div class="row">

                    @foreach($categories as $category)
                        <div class="col-lg-4">
                            @if($category->id != 3)
                                <a href="{{route('cats.games.show', $category->id)}}">
                                    <div class="box wow fadeInLeft">
                                        <div class="icon">
                                            <img src="{{asset('public/uploads/mainCats/'.$category->image)}}" alt="" class="img-responsive">
                                            <h4>{{$category->name}}</h4>
                                        </div>
                                    </div>
                                </a>
                            @else
                                <a href="{{route('cats.time.show', $category->id)}}">
                                    <div class="box wow fadeInLeft">
                                        <div class="icon">
                                            <img src="{{asset('public/uploads/mainCats/'.$category->image)}}" alt="" class="img-responsive">
                                            <h4>{{$category->name}}</h4>
                                        </div>
                                    </div>
                                </a>
                            @endif

                        </div>
                    @endforeach
                </div>
            </div>
        </section><!-- #services -->

        <!--==========================
          Clients Section
        ============================-->
        <section id="clients" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>الأخبار</h2>
                </div>

                <div class="owl-carousel clients-carousel">
                    @foreach($news as $item)
                        <div class="row">
                            <div class="col-lg-3 about-img">
                                <img src="{{asset('public/uploads/news/'.$item->image)}}" alt="" class="img-responsive">
                            </div>

                            <div class="col-lg-9 content">
                                <h5>{{$item->title}}</h5>
                                <p>{{str_limit(strip_tags(html_entity_decode($item->description)), 800)}}</p>
                                <a href="#">قراءة المزيد</a>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </section>

        <!--==========================
          Our Portfolio Section
        ============================-->
        <section id="portfolio" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>ألبوم الصور</h2>
                </div>
            </div>

            <div class="container">
                <div class="row no-gutters">
                    @foreach($album->images as $image)
                        <div class="col-md-3">
                            <div class="portfolio-item wow fadeInUp">
                                <a href="{{asset('public/uploads/images/'.$image->image)}}" class="portfolio-popup">
                                    <img src="{{asset('public/uploads/images/'.$image->image)}}" alt="">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </section><!-- #portfolio -->

        <!--==========================
          Testimonials Section
        ============================-->
        <section id="testimonials" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>الفعاليات</h2>
                </div>
                <div class="owl-carousel testimonials-carousel">

                    @foreach($events as $event)
                        <div class="testimonial-item">
                            <div class="latest-post">
                                <img src="{{asset('public/uploads/events/'.$event->image)}}" class="img-responsive" alt="">
                                <div class="details">
                                    <h4>{{$event->title}}</h4>
                                    <p>{{str_limit(strip_tags(html_entity_decode($event->description)), 800)}}</p>
                                    <a href="news-details.php" class="btn btn-primary">المزيد</a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>

            </div>
        </section><!-- #testimonials -->

        <!--==========================
          Contact Section
        ============================-->
        <section id="contact" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>اتصل بنا</h2>
                </div>

                <div class="row contact-info">
                    <div class="col-lg-5">
                        <div class="contact-address">
                            <i class="ion-ios-location-outline"></i>
                            <h3>العنوان</h3>
                            @if(app()->getLocale() == 'ar')
                                <address>{{$settings['address_ar']}}</address>
                            @else
                                <address>{{$settings['address_en']}}</address>
                            @endif

                        </div>
                        <div class="contact-phone">
                            <i class="ion-ios-telephone-outline"></i>
                            <h3>رقم الجوال</h3>
                            <p><a>{{$settings['phone']}}</a></p>
                        </div>
                        <div class="contact-email">
                            <i class="ion-ios-email-outline"></i>
                            <h3>البريد الالكتروني</h3>
                            <p><a href="mailto:{{$settings['email']}}">{{$settings['email']}}</a></p>
                        </div>
                        <div class="social-links">
                            <a href="{{$settings['twitter']}}" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="{{$settings['facebook']}}" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="{{$settings['linkedin']}}" class="linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="{{$settings['instagram']}}" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="container">
                            <div class="form">

                                <!-- Form itself -->
                                <form action="" class="well" id="contactForm" >
                                    <div class="control-group">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" placeholder="الاسم" id="name">
                                            <p class="help-block"></p>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="email" name="email" class="form-control" placeholder="البريد الالكتروني" id="email">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <select name="msg_type" class="form-control">
                                                <option value="">نوع الرسالة</option>
                                                <option value="1">استفسار</option>
                                                <option value="0">شكوى</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <textarea name="message" rows="10" cols="100" class="form-control" placeholder="الرسالة" id="message"  style="resize:none"></textarea>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary" onclick="sendForm()">إرسال</button><br/>
                                </form>
                            </div>

                        </div>
                    </div>


                </div>
            </div>

            <div class="container mb-4 map">
                <iframe src="https://maps.google.com/maps?q={{$settings['lat']}}%2C{{$settings['lng']}}&t=&z=13&ie=UTF8&iwloc=&output=embed" height="300" width="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>
            </div>

        </section><!-- #contact -->

    </main>
@stop

@section('scripts')
    <script>
        function sendForm() {
            var data = document.querySelector('#contactForm');
            var dataForm = new FormData(data);
            console.log(dataForm);

            $.ajax({
                headers: {'X-CSRF-Token': "{{csrf_token()}}"},
                url: "{{route('contact.ajax')}}",
                type: 'post',
                processData: false,
                contentType: false,
                data: dataForm,

                success: function (data) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'center',
                        showConfirmButton: false,
                        timer: 2000
                    });

                    Toast.fire({
                        type: 'success',
                        title: data
                    })
                },
                
                error: function (data) {
                    result = '';
                    $.each(data.responseJSON.errors, function (i, item) {
                        result+= item + '\n';
                    });

                    console.log(result);
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: result
                    })

                }
            })
        }
    </script>
@stop

