
<!doctype html>
<html>


    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('public/css/admin/bootstrap.min.css')}}" />

        @if(app()->getLocale() == 'en')
            <link rel="stylesheet" href="{{asset('public/css/admin/style.css')}}" />
        @else
            <link rel="stylesheet" href="{{asset('public/css/admin/bootstrap-rtl.css')}}" />
            <link rel="stylesheet" href="{{asset('public/css/admin/style-en.css')}}" />
        @endif
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        {{--<link rel="stylesheet" type="text/css" href="{{asset('public/css/admin/font-awesome.min.css')}}">--}}
        <link rel="stylesheet" href="{{asset('public/css/admin/settings.css')}}" media="screen" />

        {{--<link rel="stylesheet" href="{{asset('public/css/admin/jquery.dataTables.min.css')}}" media="screen" />--}}
        <link rel="stylesheet" href="{{asset('public/css/admin/settings.css')}}" media="screen" />
        <link rel="stylesheet" href="{{asset('public/css/admin/editor.css')}}"/>
        <link rel="stylesheet" href="{{asset('public/css/admin/jquery.fancybox.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/admin/daterangepicker.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/admin/toastr.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/datatables.min.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />


        <link rel="icon" href="{{asset('public/images2/favicon.png')}}" type="image/png">

        <title>@yield('title')</title>
        <script>var SITE_URL = '{{config('app.url')}}';</script>
        <script src="//cdn.ckeditor.com/4.11.1/full/ckeditor.js"></script>

        <style>
            .content-wrapper{
                min-height: auto !important;
            }
        </style>

        <style>
            @import ur
            .content-header>.breadcrumb {
                float: right;
                background: transparent;
                margin-top: 0;
                margin-bottom: 0;
                font-size: 12px;
                padding: 7px 5px;
                position: absolute;
                top: 15px;
                right: 10px;
                border-radius: 2px
            }
            .content-header>.breadcrumb>li>a {
                color: #444;
                text-decoration: none;
                display: inline-block
            }
            .content-header>.breadcrumb>li>a>.fa, .content-header>.breadcrumb>li>a>.glyphicon, .content-header>.breadcrumb>li>a>.ion {
                margin-right: 5px
            }
            .content-header>.breadcrumb>li+li:before {
                content: '>\00a0'
            }

            @media (max-width:991px) {
                .content-header>.breadcrumb {
                    position: relative;
                    margin-top: 5px;
                    top: 0;
                    right: 0;
                    float: none;
                    background: #d2d6de;
                    padding-left: 10px
                }
                .content-header>.breadcrumb li:before {
                    color: #97a0b3
                }

                /*.badge{*/
                    /*background-color: #00e765;*/
                /*}*/

            }
        </style>

        <style>
            #mapCanvas > div{
                height: 70% !important;
                position: relative !important;
                overflow: -webkit-paged-y !important;
            }
        </style>



        @yield('styles')
    </head>
    <body>

        <header class="header">
            <div class="logo-desh"> <img src="{{asset('public/images2/logologin.png')}}" title="Atiaf  For Completely Solutions"> </div>

            <a href="#exampleModal" data-toggle="modal"  class="logout" style="margin-right: 20px"> <i class="fas fa-sign-out-alt"></i>@lang('back/layout.logout')</a>

            @if(app()->getLocale() == 'en')
                <a href="{{url('admin/locales/ar')}}" class="button-bars" id="sidebarToggle"><i class="fa fa-bars"></i></a> <a href="{{url('locales/ar')}}" class="logout" style="margin-right: 20px"> <i class="fa fa-language"></i> العربيه </a>
            @else
                <a href="{{url('locales/en')}}" class="button-bars" id="sidebarToggle"><i class="fa fa-bars"></i></a> <a href="{{url('locales/en')}}" class="logout" style="margin-left: 20px"> <i class="fa fa-language"></i> English </a>
            @endif

            <h1 class="title">@yield('title')</h1>
        </header>
        <div id="wrapper">

            @auth()
                @include('layouts.admin.sidebar')
            @endauth

            <div id="content-wrapper">
                <div class="container-fluid" id='op'>
                    @yield('content')


                    <div id="wait" style="display:none;width:200px;height:200px;position:absolute;top:50%;left:50%;padding:2px;z-index:1000"><img src='http://haretnaapp.net/images/loading.gif' width="200" height="200" /><br></div>

                </div>
            </div>

        </div>


        <!-- Logout Modal-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">Logout</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>
                </div>
            </div>
        </div>


        <script src="//code.jquery.com/jquery.js"></script>

        <script type="text/javascript" src="{{asset('public/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/wowslider.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/sb-admin.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/amcharts.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/serial.js')}}" ></script>
        <script type="text/javascript" src="{{asset('public/js/moment.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/daterangepicker.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/toastr.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/jquery.fancybox.min.js')}}"></script>

        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/datatables.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        {{--<script type="text/javascript" src="{{asset('js/script.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>--}}

        {{--<script type="text/javascript" src="{{asset('js/jquery.dataTables.min.js')}}"></script>--}}
        {{--<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>--}}
        {{--<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.jqueryui.min.js"></script>--}}

        {{--<script type="text/javascript" src="{{asset('js/jquery.vmap.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/jquery.vmap.world.js')}}"></s   cript>--}}
        {{--<script type="text/javascript" src="{{asset('js/jquery.vmap.sampledata.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/editor.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/dataTables.select.min.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/script.js')}}"></script>--}}


        {{--<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>--}}

        {{--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>--}}

        {{--<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/wowslider.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/sb-admin.min.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/wowslider.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/amcharts.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/serial.js')}}" ></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/jquery.vmap.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/jquery.vmap.world.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/jquery.vmap.sampledata.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/dataTables.select.min.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/moment.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/daterangepicker.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/toastr.min.js')}}"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/jquery.fancybox.min.js')}}"></script>--}}
        {{--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>--}}
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>--}}
        {{--<script type="text/javascript" src="{{asset('js/script.js')}}"></script>--}}


        {{--<script>--}}
            {{--$(document).ready(function() {--}}
                {{--var locale = '{{ config('translatable.locales.'.app()->getLocale()) }}';--}}
                {{--console.log('loc : '+ locale );--}}
                {{--$('#dataTable').DataTable({--}}
                    {{--"language": {--}}
                        {{--"url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"--}}
                    {{--}--}}
                {{--});--}}
            {{--});--}}
        {{--</script>--}}

        @yield('scripts')

    </body>
</html>
