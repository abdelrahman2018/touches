<ul class="sidebar navbar-nav">
    <a href="">
        <li class="userimg">
            <p> @lang('back/layout.dashboard') </p>
        </li>
    </a>

{{--@hasrole('super admin')--}}
@can('slieds')
        <li class="nav-item">
            <a class="nav-link" href="{{route('slide.index')}}" id="" role="button">
                <i class="glyphicon glyphicon-picture"></i> <span>@lang('back/layout.slides')</span>
            </a>
        </li>
    @endcan

    @can('packets')
        <li class="nav-item">
            <a class="nav-link" href="{{route('packet.index')}}" id="" role="button">
                <i class="fab fa-get-pocket"></i> <span>{{_lang('app.packets')}}</span>
            </a>
        </li>
    @endcan

    <li class="nav-item">
        <a class="nav-link" href="{{route('unit.index')}}" id="" role="button">
            <i class="fab fa-mizuni"></i> <span>{{_lang('app.units')}}</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{route('company.index')}}" id="" role="button">
            <i class="fas fa-building"></i> <span>{{_lang('app.companies')}}</span>
        </a>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="{{route('product.index')}}" id="" role="button">
            <i class="fas fa-star"></i> <span>{{_lang('app.products')}}</span>
        </a>
    </li>


    <li class="nav-item" data-toggle="tooltip" data-placement="right">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#dels"
           data-parent="#exampleAccordion">
            <i class="fa fa-certificate"></i>
            <span class="nav-link-text">{{_lang('app.delegates')}}</span>
        </a>

        <ul class="sidenav-second-level collapse" id="dels">

            <li class="nav-item">
                <a class="nav-link" href="{{route('del.index')}}" id="" role="button">
                    <i class="fa fa-circle-notch"></i> <span>{{_lang('app.all_delegates')}}</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('notifi.del.view')}}" id="" role="button">
                    <i class="fas fa-user-friends"></i> <span>{{_lang('app.send_notification')}}</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('del.create')}}" id="" role="button">
                    <i class="fa fa-plus"></i> <span>{{_lang('app.create')}}</span>
                </a>
            </li>

        </ul>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="{{route('coupon.index')}}" id="" role="button">
            <i class="fas fa-compact-disc"></i> <span>{{_lang('app.coupons')}}</span>
        </a>
    </li>

    <li class="nav-item" data-toggle="tooltip" data-placement="right">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#clients"
           data-parent="#exampleAccordion">
            <i class="fa fa-certificate"></i>
            <span class="nav-link-text">{{_lang('app.clients')}}</span>
        </a>

        <ul class="sidenav-second-level collapse" id="clients">
            <li class="nav-item">
                <a class="nav-link" href="{{route('client.index')}}" id="" role="button">
                    <i class="fas fa-user-friends"></i> <span>{{_lang('app.all_clients')}}</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('notifi.view')}}" id="" role="button">
                    <i class="fas fa-user-friends"></i> <span>{{_lang('app.send_notification')}}</span>
                </a>
            </li>

        </ul>
    </li>



    <li class="nav-item">
        <a class="nav-link" href="{{route('order.index')}}" id="" role="button">
            <i class="fab fa-first-order-alt"></i> <span>{{_lang('app.orders')}}</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{route('contact.msgs')}}" id="" role="button">
            <i class="fa fa-envelope"></i> <span>@lang('back/layout.msgs')</span>
        </a>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="{{route('page.index')}}" id="" role="button">
            <i class="fa fa-file"></i> <span>@lang('back/layout.pages')</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{route('role.index')}}" id="" role="button">
            <i class="fab fa-mizuni"></i> <span>{{_lang('app.roles')}}</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{route('admin.index')}}" id="" role="button">
            <i class="fab fa-mizuni"></i> <span>{{_lang('app.admins')}}</span>
        </a>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="{{route('settings.edit')}}" id="" role="button">
            <i class="fa fa-cogs"></i> <span>@lang('back/layout.setting')</span>
        </a>
    </li>

    {{--@endhasrole--}}



</ul>
