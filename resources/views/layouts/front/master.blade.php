
@if(\Illuminate\Support\Facades\Request::is('games*'))
    @include('front.includes.header-games')
@elseif(\Illuminate\Support\Facades\Request::is('time*'))
    @include('front.includes.header-time')
@elseif(\Illuminate\Support\Facades\Request::is('land*'))
    @include('front.includes.header-land')
@else
    @include('front.includes.header')
@endif

@yield('content')




<!--==========================
  Footer - Do not remove credits
============================-->
<footer id="footer" class="games-footer ">
    <div class="container">
        <div class="credits">
            © جميع الحقوق محفوظة لكثيب -<a href="http://atiafco.com/" target="_blank"> تصميم وبرمجة شركة اطياف للحلول المتكاملة  	</a>
        </div>
    </div>
</footer>

<a href="#" class="back-to-top games-top"><i class="fa fa-chevron-up"></i></a>

<!-- JavaScript  -->
<script src="{{asset('public/lib/jquery/jquery.min.js')}}"></script>
<script src="{{asset('public/lib/jquery/jquery-migrate.min.js')}}"></script>
<script src="{{asset('public/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('public/lib/easing/easing.min.js')}}"></script>
<script src="{{asset('public/lib/superfish/hoverIntent.js')}}"></script>
<script src="{{asset('public/lib/superfish/superfish.min.js')}}"></script>
<script src="{{asset('public/lib/wow/wow.min.js')}}"></script>
<script src="{{asset('public/lib/owlcarousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('public/lib/magnific-popup/magnific-popup.min.js')}}"></script>
<script src="{{asset('public/lib/sticky/sticky.js')}}"></script>
{{--<script src="contact/jqBootstrapValidation.js"></script>--}}
{{--<script src="contact/contact_me.js"></script>--}}
<script src="{{asset('public/js/front/main.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

@yield('scripts')


    @if(session()->has('success'))
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'center',
            showConfirmButton: false,
            timer: 2000
        });

        Toast.fire({
            type: 'success',
            title: "{{session()->get('success')}}"
        })
    </script>
    @endif
</body>
</html>