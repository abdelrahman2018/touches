<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Max-Age: 1000");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: PUT, POST, GET, OPTIONS, DELETE");



Route::post('test/pusher', function (\Illuminate\Http\Request $request){
    $lat = $request->lat;
    $lng = $request->lng;

    $location = ['lat' => $lat, 'lng' => $lng];
    event(new \App\Events\sendLocation($location));
    return response()->json(['data' => $location], 200);
});

Route::group(['prefix' => 'auth'], function (){
    Route::post('register', 'Auth\Api\AuthController@register');
    Route::post('register/code/send', 'Auth\Api\AuthController@sendRegisterCode');
    Route::post('register/code/verify', 'Auth\Api\AuthController@verifyRegisterCode');
    Route::post('login', 'Auth\Api\AuthController@login');
    Route::post('delegate/login', 'Auth\Api\AuthController@delegateLogin');
    Route::post('logout', 'Auth\Api\AuthController@logout');
    Route::post('verify', 'Auth\Api\AuthController@verfyCode');
    Route::post('forget/password', 'Auth\Api\AuthController@forgetPassword');
    Route::post('forget/verify/code', 'Auth\Api\AuthController@verifyCode');
    Route::post('reset/password', 'Auth\Api\AuthController@resetPassword');
    Route::post('token/refresh', 'Auth\Api\AuthController@refreshToken')->middleware('refreshTokenCheck');
    Route::post('device/token', 'Api\UserController@refreshDeviceToken');
});


Route::get('pages/{id}', 'Api\PageController@getPage');
Route::get('contact/info', 'Api\PageController@settings');

/*************** companies ***************************************/
Route::get('companies', 'Api\CompanyController@index');
Route::get('companies/{id}', 'Api\CompanyController@show');

/*************** Products ****************************************/
Route::get('products', 'Api\ProductController@index');
Route::get('products/{id}', 'Api\ProductController@show');

/*************** comments ****************************************/
Route::post('comment/save', 'Api\CommentController@createComment');

/*************** rates ****************************************/
Route::post('rate/save', 'Api\RateController@rateSave');

/****************** orders ***************************/
Route::get('my/orders', 'Api\OrderController@getOrders');
Route::get('orders/{id}', 'Api\OrderController@getOrder');


Route::group(['prefix' => 'profile'], function (){
    Route::post('edit', 'Api\UserController@editProfile');
    Route::post('edit/phone', 'Api\UserController@verifyPhone');
    Route::post('edit/password', 'Api\UserController@changePassword');
});

Route::post('contact', 'Api\ContactController@sendMsg');


Route::group(['prefix' => 'cart'], function (){
   Route::post('validate', 'Api\CartController@checkCartValidation');
    Route::post('update', 'Api\CartController@updateCart');
});

Route::group(['prefix' => 'order'], function (){
   Route::post('date/validate', 'Api\OrderController@checkOrderTime');
   Route::post('save', 'Api\OrderController@orderSave');
   Route::post('coupon/validate', 'Api\OrderController@validateCoupon');
});


Route::group(['prefix' => 'delegate'], function (){
   Route::post('orders/get', 'Api\Delegate\OrderController@getOrders');
    Route::post('orders/status/change', 'Api\Delegate\OrderController@changeStatus');
    Route::get('orders/count/get', 'Api\Delegate\OrderController@getOrdersCount');
    Route::get('rates', 'Api\Delegate\DelegateController@getRate');
    Route::post('trip/save', 'Api\Delegate\DelegateController@saveTrip');
    Route::post('track/trip', function (Request $request){
       \Illuminate\Support\Facades\DB::table('delegate_trips')->insert([
           'user_id' => $request->user()->id,
           'lat' => $request->lat,
           'lng' => $request->lng,
       ]);
    });
});

Route::post('resend/sms', 'Api\Helpers\MobSMS@resendCode');




