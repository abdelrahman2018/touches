<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


Route::get('test/softdelete', function (){
   $c = \App\Company::findOrFail(17);
   $c->delete();
});

Route::get('test/softdelete2', function (){
    $rates = \App\Rate::all();
    $arr = array();
    foreach ($rates as $rate){
        array_push($arr, $rate->rateable_id);
    }
    dd($arr);
});


Route::get('test/noti', function (){
    $user = \App\User::findOrFail(48);
//    \Illuminate\Support\Facades\Notification::send($user, new \Illuminate\Notifications\DatabaseNotification());
    $user->notify(new \App\Notifications\FirebaseNotification('test', 'm3lem 7oooda mesa mesa '));
    return 'done';
//    dd($user->notifications);
});


Route::group(['middleware' => 'frontLang'], function (){
    Route::get('/', 'front\HomeController@index')->name('front.home');
    Route::get('about-us', 'front\AboutusController@view')->name('about');


    Route::get('videos', function (){
       $videos = \App\Video::all();
       return view('front.videos', compact('videos'));
    })->name('videos');

    /*************************** contact us ***********************************/
    Route::get('contact-us', 'front\ContactUsController@index')->name('contact');
    Route::post('contact-us/send', 'front\ContactUsController@store')->name('contact.send');
    Route::post('contact-us/send/ajax', 'front\ContactUsController@contactAjax')->name('contact.ajax');


    /*************************** Basket ***********************************/
    Route::post('basket/add', 'front\BasketController@addToBasket')->name('basket.add');
    Route::get('basket/remove/{id}', 'front\BasketController@remove')->name('basket.remove');


    /*************************** Auth ***********************************/
    Route::get('register', 'Auth\front\RegisterController@showForm')->name('register.form');
    Route::post('register/submit', 'Auth\front\RegisterController@register')->name('register.submit');

    Route::get('login', 'Auth\front\LoginController@showForm')->name('login.form');
    Route::post('login/submit', 'Auth\front\LoginController@login')->name('login.submit');
    Route::get('logout', 'Auth\front\LoginController@logout')->name('customer.logout');

    /*************************** password reset ***********************************/
    Route::get('password/reset', 'Auth\front\ForgetPasswordController@showLinkRequestForm')->name('forget.form');
    Route::post('password/reset', 'Auth\front\ForgetPasswordController@sendResetLinkEmail')->name('forget.submit');

    Route::get('new/password/reset/{token}', 'Auth\front\ResetPasswordController@showResetForm')->name('newPassword.form');
    Route::post('new/password/reset/submit', 'Auth\front\ResetPasswordController@reset')->name('newPassword.submit');


});




Route::get('locales_front/{locale}', function ($locale){

    session()->put('frontend-lang', $locale);
    return redirect()->back();
})->name('front_lang');



Route::group(['prefix' => 'admin'], function (){
    Auth::routes();

});

Route::get('locales/{locale}', function ($locale){
//    dd($locale);
    session()->put('backend-lang', $locale);
    return redirect()->back();
});

Route::group(['prefix' => 'admin','middleware'=>['auth:web', 'lang']], function () {


    Route::get('/','HomeController@index')->name('admin.home');



    /********************** Customers Routes ****************/
    Route::get('customers/index', 'CustomerController@index')->name('customer.index');
    Route::get('customers/{id}/update', 'CustomerController@update')->name('customer.update');
    Route::get('customers/{id}/delete', 'CustomerController@destroy')->name('customer.delete');


    /********************** Pages Routes ****************/
    Route::get('pages/create', 'PageController@create')->name('page.create');
    Route::get('pages/index', 'PageController@index')->name('page.index');
    Route::get('pages/{id}/edit', 'PageController@edit')->name('page.edit');
    Route::post('pages/store', 'PageController@store')->name('page.store');
    Route::post('pages/{id}/update', 'PageController@update')->name('page.update');
    Route::get('pages/{id}/delete', 'PageController@destroy')->name('page.delete');

    /********************** Slides Routes ****************/
    Route::group(['middleware' => 'permission:slieds'], function (){
        Route::get('slides/create', 'SlideController@create')->name('slide.create');
        Route::get('slides/index', 'SlideController@index')->name('slide.index');
        Route::get('slides/{id}/edit', 'SlideController@edit')->name('slide.edit');
        Route::post('slides/store', 'SlideController@store')->name('slide.store');
        Route::post('slides/{id}/update', 'SlideController@update')->name('slide.update');
        Route::get('slides/{id}/delete', 'SlideController@destroy')->name('slide.delete');
    });



    /********************** Packets Routes ****************/
    Route::group(['middleware' => 'permission:packets'], function (){
        Route::get('packets/create', 'PacketController@create')->name('packet.create');
        Route::get('packets/index', 'PacketController@index')->name('packet.index');
        Route::get('packets/{id}/edit', 'PacketController@edit')->name('packet.edit');
        Route::post('packets/store', 'PacketController@store')->name('packet.store');
        Route::post('packets/{id}/update', 'PacketController@update')->name('packet.update');

    });

    /********************** Units Routes ****************/
    Route::get('units/create', 'UnitController@create')->name('unit.create');
    Route::get('units/index', 'UnitController@index')->name('unit.index');
    Route::get('units/{id}/edit', 'UnitController@edit')->name('unit.edit');
    Route::post('units/store', 'UnitController@store')->name('unit.store');
    Route::post('units/{id}/update', 'UnitController@update')->name('unit.update');

    /********************** Companies Routes ****************/
    Route::get('companies/create', 'CompanyController@create')->name('company.create');
    Route::get('companies/index', 'CompanyController@index')->name('company.index');
    Route::get('companies/{id}/edit', 'CompanyController@edit')->name('company.edit');
    Route::post('companies/store', 'CompanyController@store')->name('company.store');
    Route::post('companies/{id}/update', 'CompanyController@update')->name('company.update');
    Route::post('companies/delete', 'CompanyController@destroy')->name('company.delete');

    /********************** Products Routes ****************/
    Route::get('products/create', 'ProductController@create')->name('product.create');
    Route::get('products/index', 'ProductController@index')->name('product.index');
    Route::get('products/{id}/edit', 'ProductController@edit')->name('product.edit');
    Route::post('products/store', 'ProductController@store')->name('product.store');
    Route::post('products/{id}/update', 'ProductController@update')->name('product.update');
//    Route::post('companies/delete', 'CompanyController@destroy')->name('company.delete');


    /**************************** Delegate Routes ****************************/
    Route::get('delegates/index', 'DelegateController@index')->name('del.index');
    Route::get('delegates/data', 'DelegateController@getDelegates')->name('del.data');
    Route::get('delegates/create', 'DelegateController@create')->name('del.create');
    Route::get('delegates/edit/{id}', 'DelegateController@show')->name('del.show');
    Route::get('delegates/delete/{id}', 'DelegateController@destroy')->name('del.delete');
    Route::post('delegates/store', 'DelegateController@store')->name('del.store');
    Route::post('delegates/{id}/update', 'DelegateController@update')->name('del.update');
    Route::post('delegates/location', 'DelegateController@getLocation')->name('del.loc');


    /**************************** Orders Routes ****************************/
    Route::post('order/status', 'Helpers\OrderHelper@delegateOrderStatusName')->name('del.order.status');
    Route::post('delegate/orders', 'DelegateController@delegateOrders')->name('del.orders');
    Route::post('order/show/{id}', 'DelegateController@delegateOrders')->name('order.show');
    Route::post('orders/status/search', 'OrderController@statusSearch')->name('orders.status.search');
    Route::post('orders/assign', 'OrderController@assign')->name('order.assign');
    Route::get('orders/index', 'OrderController@index')->name('order.index');
    Route::post('orders/index/data', 'OrderController@ordersIndexAjax')->name('order.data');
    Route::get('orders/status/search/export/{del_id}/{status}', 'OrderController@statusSearchExport')->name('orders.status.exp');

    /**************************** Orders Routes ****************************/
    Route::get('coupons', 'CouponController@index')->name('coupon.index');
    Route::post('coupons/edit', 'CouponController@edit')->name('coupon.edit');
    Route::post('coupons/update', 'CouponController@update')->name('coupon.update');
    Route::post('coupons/store', 'CouponController@store')->name('coupon.store');
    Route::get('coupons/delete/{id}', 'CouponController@destroy')->name('coupon.delete');


    /**************************** Clients Routes ****************************/
    Route::get('clients/index', 'ClientController@index')->name('client.index');
    Route::get('clients/get', 'ClientController@getClients')->name('client.data');
    Route::get('clients/search/exp/{from}/{to}', 'ClientController@clientSearchExport')->name('client.search.exp');
    Route::get('clients/exp', 'ClientController@clientExport')->name('client.exp');
    Route::post('clients/search', 'ClientController@clientSearch')->name('client.search');
    Route::post('clients/delete', 'ClientController@destroy')->name('client.delete');
    Route::post('clients/status/change', 'ClientController@changeStatus')->name('client.status.change');

    /**************************** Roles Routes ****************************/
    Route::get('roles/index', 'RoleController@index')->name('role.index');
    Route::get('roles/create', 'RoleController@create')->name('role.create');
    Route::get('roles/edit/{id}', 'RoleController@edit')->name('role.edit');
    Route::get('roles/delete/{id}', 'RoleController@destroy')->name('role.delete');
    Route::post('roles/store', 'RoleController@store')->name('role.store');
    Route::post('roles/update/{id}', 'RoleController@update')->name('role.update');

    /**************************** Admins Routes ****************************/
    Route::get('admins/index', 'AdminController@index')->name('admin.index');
    Route::get('admins/create', 'AdminController@create')->name('admin.create');
    Route::get('admins/edit/{id}', 'AdminController@edit')->name('admin.edit');
    Route::get('admins/delete/{id}', 'AdminController@destroy')->name('admin.delete');
    Route::post('admins/store', 'AdminController@store')->name('admin.store');
    Route::post('admins/update/{id}', 'AdminController@update')->name('admin.update');

    /**************************** Notifications Routes ****************************/
    Route::get('clients/send', 'ClientController@notificationView')->name('notifi.view');
    Route::post('clients/notifi', 'ClientController@broadCastNotification')->name('notifi.send');
    Route::get('delegates/send', 'DelegateController@notificationView')->name('notifi.del.view');
    Route::post('delegates/notifi', 'DelegateController@broadCastNotification')->name('notifi.del.send');

    /**************************** contact Routes ****************************/
    Route::get('users/messages', function (){
        $msgs = DB::table('contact')->get();
        return view('dashboard.contact.index', compact('msgs'));
    })->name('contact.msgs');



    //    /********************** Settings Routes ***********************/
    Route::get('settings', 'SettingController@edit')->name('settings.edit');
    Route::post('settings/update', 'SettingController@update')->name('settings.update');
//    Route::get('/change-lang','BackendController@changeLang')->name('changeLang');



});

