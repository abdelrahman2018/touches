<?php $__env->startSection('title'); ?>
    <title>My Orders</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="content">

        <!--accessories-->
        <div class="reservation-details">
            <div class="container">
                <h1>طلباتى</h1>
                <?php $__currentLoopData = auth('customer')->user()->orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="reservation-games col-md-12">
                        <div class="col-md-3">
                            <img src="<?php echo e(asset('public/uploads/services/'.$order->services()->first()->image)); ?>" class="img-responsive" alt="" />
                        </div>
                        <div class="col-md-9">
                            <p>رقم الطلب: <span class="total-color"><?php echo e($order->id); ?></span></p>
                            <p><span>وقت الحجز :</span><?php echo e($order->reserve_date); ?></p>
                            <p><span>وقت الحجز :</span><?php echo e($order->reserve_time); ?></p>
                            <a href="<?php echo e(route('orders.show', $order->id)); ?>" class="details">تفاصيل الطلب</a>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>