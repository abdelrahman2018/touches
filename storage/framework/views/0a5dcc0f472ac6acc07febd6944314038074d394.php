<?php $__env->startSection('title'); ?>
    <title><?php echo e($cat->name); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="clients games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>الخدمات</h2>
                </div>

                <div class="clients-det">
                    <?php $__currentLoopData = $cat->services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="row">
                            <div class="col-lg-3 about-img">
                                <img src="<?php echo e(asset('public/uploads/services/'.$service->image)); ?>" alt="" class="img-responsive">
                            </div>

                            <div class="col-lg-9 content">
                                <h4><?php echo e($service->name); ?></h4>
                                <p><?php echo e(str_limit(strip_tags(html_entity_decode($service->description)), 800)); ?></p>
                                <a href="#" data-id="<?php echo e($service->id); ?>" class="addToBasket">اضف الى السلة</a>

                                <?php if($service->category->parent->id == 1): ?>
                                    <a href="<?php echo e(route('services.games', $service->id)); ?>">مزيد من التفاصيل</a>
                                <?php elseif($service->category->parent->id == 3): ?>
                                    <a href="<?php echo e(route('services.time', $service->id)); ?>">مزيد من التفاصيل</a>
                                <?php else: ?>
                                    <a href="<?php echo e(route('services.land', $service->id)); ?>">مزيد من التفاصيل</a>
                                <?php endif; ?>

                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>

            </div>
        </section>

    </main>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $('.addToBasket').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: "<?php echo e(route('basket.add')); ?>",
                data:{
                    _token: '<?php echo e(csrf_token()); ?>',
                    id: $(this).data('id')
                },

                success: function (data) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'center',
                        showConfirmButton: false,
                        timer: 2000
                    });

                    Toast.fire({
                        type: 'success',
                        title: data
                    })
                }
            })
        })
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>