<?php $__env->startSection('title'); ?>
    <title><?php echo e($cat->name); ?></title>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
       Our Portfolio Section
     ============================-->
        <section class="portfolio games-content" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>الأقسام الفرعية</h2>
                </div>
            </div>

            <div class="container">
                <div class="row no-gutters">

                    <?php $__currentLoopData = $cat->childes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $childe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-3">
                            <div class="portfolio-item wow fadeInUp">
                                <?php if($childe->parent->id == 1): ?>
                                    <a href="<?php echo e(route('cats.games.services', $childe->id)); ?>">
                                        <img src="<?php echo e(asset('public/uploads/SubCats/'.$childe->image)); ?>" alt="">
                                        <h5><?php echo e($childe->name); ?></h5>
                                    </a>
                                <?php elseif($childe->parent->id == 3): ?>
                                    <a href="<?php echo e(route('cats.time.services', $childe->id)); ?>">
                                        <img src="<?php echo e(asset('public/uploads/SubCats/'.$childe->image)); ?>" alt="">
                                        <h5><?php echo e($childe->name); ?></h5>
                                    </a>
                                <?php else: ?>
                                    <a href="<?php echo e(route('cats.land.services', $childe->id)); ?>">
                                        <img src="<?php echo e(asset('public/uploads/SubCats/'.$childe->image)); ?>" alt="">
                                        <h5><?php echo e($childe->name); ?></h5>
                                    </a>
                                <?php endif; ?>

                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

            </div>
        </section><!-- #portfolio -->

    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>