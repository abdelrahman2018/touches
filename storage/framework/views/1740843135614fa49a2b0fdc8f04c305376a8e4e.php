<?php $__env->startSection('title'); ?>
    <title>login</title>
<?php $__env->stopSection(); ?>


    


<?php $__env->startSection('content'); ?>


    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="about padding games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>تسجيل دخول</h2>
                </div>

                <div class="clients-det login-margin">
                    <div class="row">
                        <form action="<?php echo e(route('login.submit')); ?>" method="post" class="well games-form">
                            <?php echo csrf_field(); ?>
                            <div class="control-group">
                                <div class="form-group">
                                    <label>البريد الالكترونى:</label>
                                    <input type="email" name="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" placeholder="يجب كتابة البريد الالكترونى الذى سجلته عند الطلب">
                                    <?php if($errors->has('email')): ?>
                                        <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>كلمة المرور:</label>
                                    <input type="password" name="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" placeholder="كلمة المرور">
                                    <?php if($errors->has('password')): ?>
                                        <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('password')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <a href="<?php echo e(route('forget.form')); ?>" class="forget">نسيت كلمة المرور؟</a>
                            <button type="submit" class="btn btn-primary">دخول</button>
                            <a href="<?php echo e(route('register.form')); ?>" class="signin">غير مسجل من قبل, اريد التسجيل</a>
                        </form>
                    </div>

                </div>

            </div>
        </section>

    </main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>