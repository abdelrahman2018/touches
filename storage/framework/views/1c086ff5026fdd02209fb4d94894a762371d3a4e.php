<?php $__env->startSection('title'); ?>
    <title>Videos</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
          About Section
        ============================-->
        <section class="about videos wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>الفيديوهات</h2>
                </div>
                <div class="row">
                    <?php $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-lg-12 content">
                            <h3><?php echo e($video->title); ?></h3>
                            <div class="video">
                                <iframe src="https://www.youtube.com/embed/<?php echo e(explode('=', $video->link)[1]); ?>" width="100%" height="300" frameborder="0"  allowfullscreen></iframe>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>

            </div>
        </section><!-- #about -->


    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>