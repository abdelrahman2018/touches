<?php $__env->startSection('title'); ?>
    <title>About us</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
          About Section
        ============================-->
        <section class="about" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2><?php echo e($about->title); ?></h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 about-img">
                        <img src="<?php echo e(asset('public/uploads/pages/'.$about->image)); ?>" alt="">
                    </div>

                    <div class="col-lg-8 content">
                        <?php echo $about->description; ?>

                    </div>
                </div>

            </div>
        </section><!-- #about -->


    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>