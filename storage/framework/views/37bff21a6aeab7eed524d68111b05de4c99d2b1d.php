<?php $__env->startSection("title"); ?>
    
    users messages
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> <?php echo app('translator')->getFromJson('back/layout.dashboard'); ?></a></li>
                <li><a href="<?php echo e(route('contact.msgs')); ?>"><i class="fa fa-bars"></i><?php echo app('translator')->getFromJson('back/msg.msg_list'); ?></a></li>
                <li class="active"><?php echo app('translator')->getFromJson('back/msg.msg_all'); ?></li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">

                        
                            
                                
                            
                        
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                    <tr>
                                                        <th width="5%">id</th>
                                                        <th><?php echo app('translator')->getFromJson('back/msg.name'); ?></th>
                                                        <th><?php echo app('translator')->getFromJson('back/msg.email'); ?></th>
                                                        <th><?php echo app('translator')->getFromJson('back/msg.msg_type'); ?></th>
                                                        <th width="25%"><?php echo app('translator')->getFromJson('back/msg.msg'); ?></th>
                                                        <th><?php echo app('translator')->getFromJson('back/layout.added.modified'); ?></th>
                                                        <th width="5%">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php $__currentLoopData = $msgs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $msg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td><?php echo e($msg->id); ?></td>
                                                            <td><?php echo e($msg->name); ?></td>
                                                            <td><?php echo e($msg->email); ?></td>
                                                            <?php if(app()->getLocale() == 'en'): ?>
                                                                <?php switch($msg->msg_type):
                                                                    case (1): ?>
                                                                    <td>inquery</td>
                                                                    <?php break; ?>

                                                                    <?php default: ?>
                                                                    <td>complaint</td>
                                                                <?php endswitch; ?>
                                                            <?php else: ?>
                                                                <?php switch($msg->msg_type):
                                                                    case (1): ?>
                                                                    <td>استفسار</td>
                                                                    <?php break; ?>

                                                                    <?php default: ?>
                                                                    <td>شكوي</td>
                                                                <?php endswitch; ?>
                                                            <?php endif; ?>
                                                            <td><?php echo e($msg->msg); ?></td>
                                                            <td>
                                                                <strong><?php echo app('translator')->getFromJson('back/layout.added.date'); ?>: </strong>
                                                                <?php echo e($msg->created_at); ?>

                                                                <br>
                                                                <strong><?php echo app('translator')->getFromJson('back/layout.modified.date'); ?>: </strong>
                                                                <?php echo e($msg->updated_at); ?>

                                                            </td>
                                                            <td>
                                                                <a href="mailto:<?php echo e($msg->email); ?>" class="btn btn-primary"><?php echo app('translator')->getFromJson('back/msg.reply'); ?></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>