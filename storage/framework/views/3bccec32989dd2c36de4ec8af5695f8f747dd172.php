<?php $__env->startSection('title'); ?>
    <title><?php echo e($service->name); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
          About Section
        ============================-->
        <section class="about news-details games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2><?php echo e($service->name); ?></h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 about-img">
                        <img src="<?php echo e(asset('public/uploads/services/'.$service->image)); ?>" alt="">
                    </div>

                    <div class="col-lg-8 content">
                        <?php echo $service->description; ?>

                        <div class="basket">
                            <a href="" data-id="<?php echo e($service->id); ?>" id="addBasket2">اضف الى سلة الطلبات</a>
                            <?php if(\Illuminate\Support\Facades\Request::is('games*')): ?>
                                <a href="<?php echo e(route('games.order.now', $service->id)); ?>">اطلب الخدمة الأن</a>
                            <?php elseif(\Illuminate\Support\Facades\Request::is('time*')): ?>
                                <a href="<?php echo e(route('time.order.now', $service->id)); ?>">اطلب الخدمة الأن</a>
                            <?php else: ?>
                                <a href="<?php echo e(route('land.order.now', $service->id)); ?>">اطلب الخدمة الأن</a>
                            <?php endif; ?>

                        </div>
                        <?php
                            $link = route('services.games', $service->id);
                            $twitter_link = $service->name . '%0A' . route('services.games', $service->id)
                        ?>
                        <div class="share">
                            <h4>شارك الموضوع:</h4>
                            <!-- facebook -->
                            <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e($link); ?>" target="blank"><i class="fa fa-facebook"></i></a>
                            <!-- twitter -->
                            <a class="twitter" href="https://twitter.com/home?status=<?php echo $twitter_link; ?>" target="blank"><i class="fa fa-twitter"></i></a>
                            <!-- linkedin -->
                            <a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo e(route('services.games', $service->id)); ?>&title=<?php echo e($service->name); ?>&summary=<?php echo e($service->description); ?>&source=LinkedIn" target="blank"><i class="fa fa-linkedin"></i></a>
                            <!-- pinterest -->
                            <a class="pinterest" href="https://pinterest.com/pin/create/button/?url=<?php echo e($service->name); ?>&media=<?php echo e(asset('public/uploads/services/'.$service->image)); ?>&description=<?php echo $twitter_link; ?>" target="blank"><i class="fa fa-pinterest-p"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- #about -->


    </main>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $('#addBasket2').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: "<?php echo e(route('basket.add')); ?>",
                data:{
                    _token: '<?php echo e(csrf_token()); ?>',
                    id: $(this).data('id')
                },

                success: function (data) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'center',
                        showConfirmButton: false,
                        timer: 2000
                    });

                    Toast.fire({
                        type: 'success',
                        title: data
                    })
                }
            })
        })
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>