<?php $__env->startSection("title"); ?>
    
    
    All Delegates
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> <?php echo app('translator')->getFromJson('back/layout.dashboard'); ?></a></li>
                <li><a href="<?php echo e(route('del.index')); ?>"><i class="fa fa-bars"></i>List All Delegates</a></li>
                <li class="active"> all Delegates</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">all Delegates</h1>
                            <div class="box-tools <?php if(app()->getLocale() == 'en'): ?> pull-right <?php else: ?> pull-left <?php endif; ?>">
                                <a href="<?php echo e(route('del.create')); ?>" type="button" class="btn btn-block btn-primary">Add Delegate</a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th>name</th>
                                                    <th width="5%">id</th>
                                                    <th>username</th>
                                                    <th>image</th>
                                                    <th>phone</th>
                                                    <th>email</th>
                                                    <th>status</th>
                                                    <th><?php echo app('translator')->getFromJson('back/layout.added.modified'); ?></th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php $__currentLoopData = $delegates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $delegate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr class="trData">
                                                        <td><?php echo e($delegate->id); ?></td>
                                                        <td><?php echo e($delegate->name); ?></td>
                                                        <td><?php echo e($delegate->username); ?></td>
                                                        <td>
                                                            <a href="<?php echo e(asset('public/uploads/delegates/'.$delegate->image)); ?>" data-fancybox> <img src="<?php echo e(asset('public/uploads/delegates/'.$delegate->image)); ?>" width="100%"></a>
                                                        </td>
                                                        <td><?php echo e($delegate->phone); ?></td>
                                                        <td><?php echo e($delegate->email); ?></td>
                                                        <td><span class="label label-<?php echo e($delegate->active == 1? 'success':'danger'); ?>"><?php echo e($delegate->active == 1? 'Active':'not Active'); ?></span></td>
                                                        <td>
                                                            <strong><?php echo app('translator')->getFromJson('back/layout.added.date'); ?>: </strong>
                                                            <?php echo e($delegate->created_at); ?>

                                                            <br>
                                                            <strong><?php echo app('translator')->getFromJson('back/layout.modified.date'); ?>: </strong>
                                                            <?php echo e($delegate->updated_at); ?>

                                                        </td>
                                                        <td>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="<?php echo app('translator')->getFromJson('back/layout.edit'); ?>" href="<?php echo e(route('del.show', $delegate->id)); ?>" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-eye" aria-hidden="true"></i></a>
                                                            <a data-toggle="tooltip"  data-placement="bottom" title="<?php echo app('translator')->getFromJson('back/layout.delete'); ?>" href="<?php echo e(route('del.delete', $delegate->id)); ?>"  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            
                                                            
                                                            
                                                            
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $(document).ready(function() {
            var locale = '<?php echo e(config('translatable.locales.'.app()->getLocale())); ?>';
            $('#dataTable').DataTable({
                // processing: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/"+ locale +".json"
                },
                serverSide: true,
                ajax: '<?php echo e(route('del.data')); ?>',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'username', name: 'username'},
                    {data: 'email', 'name': 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'image', name: 'image'},


                    // {data: 'link', name: 'link'},
                    // {data: 'action', name: 'action'},
                ]
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>