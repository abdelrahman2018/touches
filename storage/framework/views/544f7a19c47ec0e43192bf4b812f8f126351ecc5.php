<?php $__env->startSection('title'); ?>
    <title>Forget password</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="about padding games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>نسيت كلمة المرور</h2>
                </div>

                <div class="clients-det login-margin">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <form action="<?php echo e(route('forget.submit')); ?>" method="post" class="well games-form">
                            <?php echo csrf_field(); ?>
                            <div class="control-group">
                                <div class="form-group">
                                    <label>ادخل بريدك الالكترونى</label>
                                    <input type="email" name="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('email')); ?>">
                                    <?php if($errors->has('email')): ?>
                                        <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                             </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ارسال</button>
                        </form>
                    </div>

                </div>

            </div>
        </section>

    </main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>