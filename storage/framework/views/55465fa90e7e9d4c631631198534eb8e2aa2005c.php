<?php $__env->startSection('title'); ?>
    <title>News</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="clients wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>الأخبار</h2>
                </div>

                <div class="clients-det">
                    <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="row">
                            <div class="col-lg-3 about-img">
                                <img src="<?php echo e(asset('public/uploads/news/'.$item->image)); ?>" alt="" class="img-responsive">
                            </div>

                            <div class="col-lg-9 content">
                                <h5><?php echo e($item->title); ?></h5>
                                <p><?php echo e(str_limit(strip_tags(html_entity_decode($item->description)), 800)); ?></p>
                                <a href="<?php echo e(route('news.front.show', $item->id)); ?>">قراءة المزيد</a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>

            </div>
        </section>

    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>