<?php $__env->startSection("title"); ?>
    
    edit
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo e(route('loc.index')); ?>"><i class="fa fa-bars"></i> List All Locations</a></li>
                <li class="active">Edit Location</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Edit Location </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="<?php echo e(route('loc.update', $location->id)); ?>" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>

                                                <?php $__currentLoopData = $locales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loc => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">Name (<?php echo e($val); ?>)</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" value="<?php echo e($location->translate($loc)->name); ?>" name="name[<?php echo e($loc); ?>]" class="form-control field-validate">
                                                        </div>
                                                    </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                <div class="form-group">
                                                    <label class="col-sm-2 col-md-3 control-label">status</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <select name="status" class="form-control">
                                                            <option value="1" <?php echo e(($location->active == 1)? 'selected': ''); ?>>Active</option>
                                                            <option value="0" <?php echo e(($location->active == 0)? 'selected': ''); ?>>Deactive</option>
                                                        </select>
                                                    </div>

                                                </div>


                                            <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">update Location</button>
                                                    <a href="<?php echo e(route('loc.index')); ?>" type="button" class="btn btn-default">Back</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>