<ul class="sidebar navbar-nav">
    <a href="">
        <li class="userimg">
            <p> <?php echo app('translator')->getFromJson('back/layout.dashboard'); ?> </p>
        </li>
    </a>




    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('loc.index')); ?>" id="" role="button">
            <i class="fa fa-map-marker"></i> <span>Locations</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('order.index')); ?>" id="" role="button">
            <i class="glyphicon glyphicon-hourglass"></i> <span>Reservation Orders</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('now.index')); ?>" id="" role="button">
            <i class="glyphicon glyphicon-list-alt"></i> <span>Orders Now</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('customer.index')); ?>" id="" role="button">
            <i class="fa fa-users"></i> <span>Customers</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('service.index')); ?>" id="" role="button">
            <i class="fab fa-servicestack"></i> <span>Services</span>
        </a>
    </li>


    <li class="nav-item" data-toggle="tooltip" data-placement="right">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#cats"
           data-parent="#exampleAccordion">
            <i class="fa fa-certificate"></i>
            <span class="nav-link-text"><?php echo app('translator')->getFromJson('back/layout.cats'); ?></span>
        </a>

        <ul class="sidenav-second-level collapse" id="cats">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('main.index')); ?>" id="" role="button">
                    <i class="fa fa-circle"></i> <span><?php echo app('translator')->getFromJson('back/layout.cats.main'); ?></span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('sub.index')); ?>" id="" role="button">
                    <i class="fa fa-circle-notch"></i> <span><?php echo app('translator')->getFromJson('back/layout.cats.sub'); ?></span>
                </a>
            </li>

        </ul>
    </li>



    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('videos.index')); ?>" id="" role="button">
            <i class="fas fa-video"></i> <span>Videos</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('news.index')); ?>" id="" role="button">
            <i class="fas fa-newspaper"></i> <span>News</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('event.index')); ?>" id="" role="button">
            <i class="fas fa-calendar-alt"></i> <span>Events</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('album.index')); ?>" id="" role="button">
            <i class="fas fa-images"></i> <span>Albums</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('contact.msgs')); ?>" id="" role="button">
            <i class="fa fa-envelope"></i> <span><?php echo app('translator')->getFromJson('back/layout.msgs'); ?></span>
        </a>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('page.index')); ?>" id="" role="button">
            <i class="fa fa-file"></i> <span><?php echo app('translator')->getFromJson('back/layout.pages'); ?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('slide.index')); ?>" id="" role="button">
            <i class="glyphicon glyphicon-picture"></i> <span><?php echo app('translator')->getFromJson('back/layout.slides'); ?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('settings.edit')); ?>" id="" role="button">
            <i class="fa fa-cogs"></i> <span><?php echo app('translator')->getFromJson('back/layout.setting'); ?></span>
        </a>
    </li>

</ul>
