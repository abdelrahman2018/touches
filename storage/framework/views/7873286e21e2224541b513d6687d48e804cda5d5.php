<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <?php echo $__env->yieldContent('title'); ?>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <meta content="Author" name="Abdelrahman-01060144799">
  <!-- Favicons -->
    <link href="<?php echo e(asset('public/images2/front/favicon.png')); ?>" rel="icon">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo e(asset('public/lib/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo e(asset('public/lib/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(asset('public/lib/animate/animate.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(asset('public/lib/ionicons/css/ionicons.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(asset('public/lib/owlcarousel/assets/owl.carousel.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(asset('public/lib/magnific-popup/magnific-popup.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(asset('public/lib/ionicons/css/ionicons.min.css')); ?>" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <?php if(app()->getLocale() == 'en'): ?>
    <link href="<?php echo e(asset('public/css/front/style-en.css')); ?>" rel="stylesheet">
  <?php else: ?>
    <link href="<?php echo e(asset('public/css/front/style.css')); ?>" rel="stylesheet">
  <?php endif; ?>

  <?php echo $__env->yieldContent('styles'); ?>
</head>

<body id="body">

  <!--==========================
    Top Bar
  ============================-->
  <section id="topbar" class="d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <?php if(app()->getLocale() == 'ar'): ?>
          <i class="fa fa-globe"></i> <a href="<?php echo e(route('front_lang', 'en')); ?>">English</a>
        <?php else: ?>
          <i class="fa fa-globe"></i> <a href="<?php echo e(route('front_lang', 'ar')); ?>">العربية</a>
        <?php endif; ?>

      </div>
      <div class="social-links float-right">
        <a href="<?php echo e($settings['twitter']); ?>" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="<?php echo e($settings['facebook']); ?>" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="<?php echo e($settings['linkedin']); ?>" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="<?php echo e($settings['instagram']); ?>" class="instagram"><i class="fa fa-instagram"></i></a>
      </div>
    </div>
  </section>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo">
         <a href="#body"><img src="<?php echo e(asset('public/css/images/logo.png')); ?>" alt="" title="" /></a>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="<?php echo e(route('front.home')); ?>">الرئيسية</a></li>
          <li><a href="<?php echo e(route('about')); ?>">من نحن</a></li>
          <li class="menu-has-children"><a href="">شركاتنا</a>
            <ul>
              <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($category->id == 1): ?>
                  <li><a href="<?php echo e(route('cats.games.show', $category->id)); ?>"><?php echo e($category->name); ?></a></li>
                <?php elseif($category->id == 3): ?>
                  <li><a href="<?php echo e(route('cats.time.show', $category->id)); ?>"><?php echo e($category->name); ?></a></li>
                <?php else: ?>
                  <li><a href="<?php echo e(route('cats.land.show', $category->id)); ?>"><?php echo e($category->name); ?></a></li>
                <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </li>
          <li><a href="<?php echo e(route('news.front.index')); ?>">الأخبار</a></li>
          <li><a href="<?php echo e(route('event.front.index')); ?>">الفعاليات</a></li>
          <li><a href="<?php echo e(route('album.front.index')); ?>">ألبوم الصور</a></li>
          <li><a href="<?php echo e(route('videos')); ?>">الفيديوهات</a></li>
          <li><a href="<?php echo e(route('contact')); ?>">اتصل بنا</a></li>
        </ul>
      </nav>
    </div>
  </header><!-- #header -->