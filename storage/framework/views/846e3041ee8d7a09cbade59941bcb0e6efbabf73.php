<?php $__env->startSection("title"); ?>
    
    users orders
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> <?php echo app('translator')->getFromJson('back/layout.dashboard'); ?></a></li>
                <li><a href="<?php echo e(route('loc.index')); ?>"><i class="fa fa-bars"></i>all locations</a></li>
                <li class="active">all locations</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">

                        <div class="box-header">
                            <div class="box-tools <?php if(app()->getLocale() == 'en'): ?> pull-right <?php else: ?> pull-left <?php endif; ?>">
                                <a href="<?php echo e(route('loc.create')); ?>"  type="button" class="btn btn-block btn-success">Create Location</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                    <tr>
                                                        <th width="5%">id</th>
                                                        <th><?php echo app('translator')->getFromJson('back/msg.name'); ?></th>
                                                        <th>status</th>
                                                        <th width="10%">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody class="td-body">
                                                    <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td><?php echo e($location->id); ?></td>
                                                            <td><?php echo e($location->name); ?></td>
                                                            <?php if($location->active == 0): ?>
                                                                <td>Deactive</td>
                                                            <?php else: ?>
                                                                <td>Active</td>
                                                            <?php endif; ?>

                                                            <td>
                                                                <a  data-placement="bottom" title="edit" href="<?php echo e(route('loc.edit', $location->id)); ?>" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>


    <div id="createModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create Location</h4>
                </div>
                <div class="modal-body">
                    <form id="create-form">
                        <?php $__currentLoopData = $locales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loc => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="form-group">
                                <label class="form-control-label">Name (<?php echo e($val); ?>)</label>
                                <input class="form-control" type="text" name="name[<?php echo e($loc); ?>]" required>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <button type="button" id="store" class="btn btn-primary">Save</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>



    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Location</h4>
                </div>
                <div class="modal-body">
                    <form>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

    <script>
        $('#store').on('click', function () {
            obje = new FormData(document.getElementById('create-form'));
            console.log(obje);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },

                type: 'post',
                url: "<?php echo e(route('loc.store')); ?>",
                processData: false,
                contentType: false,
                dataType: 'text',

                data: {
                    // loc: obje,
                    test: 2
                },
                
                success: function (data) {

                }
            })
        })
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>