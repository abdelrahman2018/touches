<?php $__env->startSection("title"); ?>
    
    create Sub Category
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo e(route('sub.index')); ?>"><i class="fa fa-bars"></i> List All Sub-Categories</a></li>
                <li class="active">Add Sub-Categories</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Add sub-Categories </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="<?php echo e(route('sub.store')); ?>" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>

                                                <div class="form-group">
                                                    <label for="parent" class="col-sm-2 col-md-3 control-label">Main Category</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <select name="main_cat" class="form-control">
                                                            <?php $__currentLoopData = $mainCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">choose main category.</span>
                                                    </div>
                                                </div>

                                                <?php $__currentLoopData = $locales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loc => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">Name (<?php echo e($val); ?>)</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="name[<?php echo e($loc); ?>]" value="<?php echo e(old('name:'.$loc)); ?>" class="form-control field-validate">
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Image</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input id="newImage" name="image" type="file">
                                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Upload category image.</span>
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">Add Sub-Category</button>
                                                    <a href="<?php echo e(route('sub.index')); ?>" type="button" class="btn btn-default">Back</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>