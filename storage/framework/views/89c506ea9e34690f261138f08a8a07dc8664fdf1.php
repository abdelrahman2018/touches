<?php $__env->startSection("title"); ?>
    
    Events
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo e(route('event.index')); ?>"><i class="fa fa-bars"></i> List All events</a></li>
                <li class="active">all events</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">Listing events </h1>
                            <div class="box-tools <?php if(app()->getLocale() == 'en'): ?> pull-right <?php else: ?> pull-left <?php endif; ?>">
                                <a href="<?php echo e(route('event.create')); ?>" type="button" class="btn btn-block btn-primary">Add New event</a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="5%">id</th>
                                                    <th>title</th>
                                                    <th>image</th>
                                                    <th>Added/Last modified date</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($event->id); ?></td>
                                                        <td><?php echo e($event->title); ?></td>
                                                        <td>
                                                            <a href="<?php echo e(asset('public/uploads/events/'.$event->image)); ?>" data-fancybox> <img src="<?php echo e(asset('public/uploads/events/'.$event->image)); ?>" width="100%"></a>
                                                        </td>
                                                        <td>
                                                            <strong>Added date: </strong>
                                                            <?php echo e($event->created_at); ?>

                                                            <br>
                                                            <strong>Modified date: </strong>
                                                            <?php echo e($event->updated_at); ?>

                                                        </td>
                                                        <td>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="<?php echo e(route('event.edit', $event->id)); ?>" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="Delete" href="<?php echo e(route('event.delete', $event->id)); ?>"  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            
                                                            
                                                            
                                                            
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>