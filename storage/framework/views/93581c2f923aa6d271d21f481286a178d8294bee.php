<?php $__env->startSection("title"); ?>
    
    edit Service
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo e(route('service.index')); ?>"><i class="fa fa-bars"></i> List All services</a></li>
                <li class="active">edit service</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">edit services </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="<?php echo e(route('service.update', $service->id)); ?>" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">category</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <select name="category" class="form-control field-validate">
                                                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($category->id); ?>" <?php if($service->category_id == $category->id): ?> selected <?php endif; ?>><?php echo e($category->name); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <?php $__currentLoopData = $locales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loc=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">title (<?php echo e($val); ?>)</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="name[<?php echo e($loc); ?>]" value="<?php echo e($service{'name:'.$loc.''}); ?>" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="desc" class="col-sm-2 col-md-3 control-label">description (<?php echo e($val); ?>)</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <textarea name="description[<?php echo e($loc); ?>]" id="desc[<?php echo e($loc); ?>]">
                                                                <?php echo e($service{'description:'.$loc.''}); ?>

                                                            </textarea>
                                                        </div>
                                                    </div>

                                                    <script>
                                                        CKEDITOR.replace( 'desc[<?php echo e($loc); ?>]' );
                                                    </script>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Image</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input id="newImage" name="image" type="file">
                                                        <img src="<?php echo e(asset('public/uploads/services/'.$service->image)); ?>" width="100px">
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">update service</button>
                                                    <a href="<?php echo e(route('service.index')); ?>" type="button" class="btn btn-default">Back</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>