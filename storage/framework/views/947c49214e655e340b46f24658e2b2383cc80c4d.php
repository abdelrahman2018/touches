<?php $__env->startSection('styles'); ?>
    <style>
        #mapCanvas > div{
            height: 50% !important;
            position: relative !important;
        }
    </style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
    <title>Order Now</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="clients games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>Order Now</h2>
                </div>

                <div class="clients-det games-margin">
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <form action="<?php echo e(route('order.save')); ?>" method="post" class="well games-form">
                            <?php echo csrf_field(); ?>
                            <input type="hidden" name="service_id" value="<?php echo e($service_id); ?>">
                            <div class="control-group">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="الاسم">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="text" name="organization" class="form-control" placeholder="اسم الجهة">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="controls">
                                    <select name="site" class="form-control">
                                        <option value="">اختر نوع الموقع</option>
                                        <?php $__currentLoopData = $sites; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $site): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($site->id); ?>"><?php echo e($site->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="text" name="phone" class="form-control" placeholder="رقم الجوال">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="البريد الالكترونى">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="text" name="address" class="form-control" placeholder="العنوان بالتفصيل">
                                </div>
                            </div>

                            <div class="form-group">
                                <div id="mapCanvas" style="overflow: hidden" ></div>
                            </div>

                            <div class="form-group">
                                <input type="hidden" id="lat" name="lat" value="29.996533" >
                                <input type="hidden" id="lng" name="lng" value="31.207152" >
                            </div>

                            <button type="submit" class="btn btn-primary">إرسال</button><br />
                        </form>
                    </div>

                </div>

            </div>
        </section>

    </main>

<?php $__env->stopSection(); ?>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5Jrp9PtHe0WapppUzxbIpMDWMAcV3qE4&libraries=places"></script>
<?php $__env->startSection('scripts'); ?>
    <script>
        var position = [29.996533, 31.207152];

        function initialize() {
            var latlng = new google.maps.LatLng(position[0], position[1]);
            var myOptions = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
            google.maps.event.addListener(searchBox,'places_changed',function(){
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, place;
                for(i=0; place=places[i];i++){
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location); //set marker position new...
                    $('#lat').val(place.geometry.location.lat());
                    $('#lng').val(place.geometry.location.lng());
                }
                map.fitBounds(bounds);
                map.setZoom(15);

            });

            map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);

            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: "Latitude:"+position[0]+" | Longitude:"+position[1]
            });

            google.maps.event.addListener(map, 'click', function(event) {
                var result = [event.latLng.lat(), event.latLng.lng()];
                transition(result);
            });
            $('#lat').val(position[0]);
            $('#lng').val(position[1]);
        }

        //Load google map
        google.maps.event.addDomListener(window, 'load', initialize);


        var numDeltas = 100;
        var delay = 10; //milliseconds
        var i = 0;
        var deltaLat;
        var deltaLng;

        function transition(result){
            i = 0;
            deltaLat = (result[0] - position[0])/numDeltas;
            deltaLng = (result[1] - position[1])/numDeltas;
            $('#lat').val(position[0]);
            $('#lng').val(position[1]);
            moveMarker();
        }

        function moveMarker(){
            position[0] += deltaLat;
            position[1] += deltaLng;
            var latlng = new google.maps.LatLng(position[0], position[1]);
            marker.setTitle("Latitude:"+position[0]+" | Longitude:"+position[1]);
            marker.setPosition(latlng);

            $('#lat').val(position[0]);
            $('#lng').val(position[1]);
            if(i!=numDeltas){
                i++;
                setTimeout(moveMarker, delay);
            }

        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>