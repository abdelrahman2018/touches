<?php $__env->startSection('title'); ?>
    <title><?php echo e($news->title); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
          About Section
        ============================-->
        <section class="about news-details wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2><?php echo e($news->title); ?></h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 about-img">
                        <img src="<?php echo e(asset('public/uploads/news/'.$news->image)); ?>" alt="">
                    </div>

                    <div class="col-lg-8 content">
                        <?php echo $news->description; ?>

                        <div class="share">
                            <h4>شارك الموضوع:</h4>
                            <!-- facebook -->
                            <a class="facebook" href="#" target="blank"><i class="fa fa-facebook"></i></a>
                            <!-- twitter -->
                            <a class="twitter" href="#" target="blank"><i class="fa fa-twitter"></i></a>
                            <!-- linkedin -->
                            <a class="linkedin" href="#" target="blank"><i class="fa fa-linkedin"></i></a>
                            <!-- pinterest -->
                            <a class="pinterest" href="#" target="blank"><i class="fa fa-pinterest-p"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- #about -->


    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>