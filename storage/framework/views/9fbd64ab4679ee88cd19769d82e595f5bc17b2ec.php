<?php $__env->startSection('title'); ?>
    <title>katheeb</title>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <!--==========================
    Intro Section
  ============================-->
    <section id="intro">
        <div class="container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <?php for($i = 0; $i < count($slides); $i++): ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo e($i); ?>" class="<?php echo e($i == 0? 'active' : ''); ?>"></li>
                    <?php endfor; ?>
                </ol>
                <div class="carousel-inner">
                    <?php for($i = 0; $i < count($slides); $i++): ?>
                        <div class="carousel-item <?php echo e($i == 0? 'active': ''); ?>">
                            <img src="<?php echo e(asset('public/uploads/slides/'.$slides[$i]->image)); ?>" class="d-block w-100" alt="...">
                        </div>
                    <?php endfor; ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
    <!-- #intro -->

    <main id="main">

        <!--==========================
          About Section
        ============================-->
        <section id="about" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2><?php echo e($about->title); ?></h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 about-img">
                        <img src="<?php echo e(asset('public/uploads/pages/'.$about->image)); ?>" alt="">
                    </div>

                    <div class="col-lg-8 content">
                        <?php echo e(strip_tags(html_entity_decode($about->description))); ?>

                    </div>
                </div>

            </div>
        </section><!-- #about -->


        <!--==========================
          Services Section
        ============================-->
        <section id="services">
            <div class="container">
                <div class="section-header">
                    <h2>شركاتنا</h2>
                </div>

                <div class="row">

                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-lg-4">
                            <?php if($category->id != 3): ?>
                                <a href="<?php echo e(route('cats.games.show', $category->id)); ?>">
                                    <div class="box wow fadeInLeft">
                                        <div class="icon">
                                            <img src="<?php echo e(asset('public/uploads/mainCats/'.$category->image)); ?>" alt="" class="img-responsive">
                                            <h4><?php echo e($category->name); ?></h4>
                                        </div>
                                    </div>
                                </a>
                            <?php else: ?>
                                <a href="<?php echo e(route('cats.time.show', $category->id)); ?>">
                                    <div class="box wow fadeInLeft">
                                        <div class="icon">
                                            <img src="<?php echo e(asset('public/uploads/mainCats/'.$category->image)); ?>" alt="" class="img-responsive">
                                            <h4><?php echo e($category->name); ?></h4>
                                        </div>
                                    </div>
                                </a>
                            <?php endif; ?>

                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </section><!-- #services -->

        <!--==========================
          Clients Section
        ============================-->
        <section id="clients" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>الأخبار</h2>
                </div>

                <div class="owl-carousel clients-carousel">
                    <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="row">
                            <div class="col-lg-3 about-img">
                                <img src="<?php echo e(asset('public/uploads/news/'.$item->image)); ?>" alt="" class="img-responsive">
                            </div>

                            <div class="col-lg-9 content">
                                <h5><?php echo e($item->title); ?></h5>
                                <p><?php echo e(str_limit(strip_tags(html_entity_decode($item->description)), 800)); ?></p>
                                <a href="#">قراءة المزيد</a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>

            </div>
        </section>

        <!--==========================
          Our Portfolio Section
        ============================-->
        <section id="portfolio" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>ألبوم الصور</h2>
                </div>
            </div>

            <div class="container">
                <div class="row no-gutters">
                    <?php $__currentLoopData = $album->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-3">
                            <div class="portfolio-item wow fadeInUp">
                                <a href="<?php echo e(asset('public/uploads/images/'.$image->image)); ?>" class="portfolio-popup">
                                    <img src="<?php echo e(asset('public/uploads/images/'.$image->image)); ?>" alt="">
                                </a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>

            </div>
        </section><!-- #portfolio -->

        <!--==========================
          Testimonials Section
        ============================-->
        <section id="testimonials" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>الفعاليات</h2>
                </div>
                <div class="owl-carousel testimonials-carousel">

                    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="testimonial-item">
                            <div class="latest-post">
                                <img src="<?php echo e(asset('public/uploads/events/'.$event->image)); ?>" class="img-responsive" alt="">
                                <div class="details">
                                    <h4><?php echo e($event->title); ?></h4>
                                    <p><?php echo e(str_limit(strip_tags(html_entity_decode($event->description)), 800)); ?></p>
                                    <a href="news-details.php" class="btn btn-primary">المزيد</a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

            </div>
        </section><!-- #testimonials -->

        <!--==========================
          Contact Section
        ============================-->
        <section id="contact" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>اتصل بنا</h2>
                </div>

                <div class="row contact-info">
                    <div class="col-lg-5">
                        <div class="contact-address">
                            <i class="ion-ios-location-outline"></i>
                            <h3>العنوان</h3>
                            <?php if(app()->getLocale() == 'ar'): ?>
                                <address><?php echo e($settings['address_ar']); ?></address>
                            <?php else: ?>
                                <address><?php echo e($settings['address_en']); ?></address>
                            <?php endif; ?>

                        </div>
                        <div class="contact-phone">
                            <i class="ion-ios-telephone-outline"></i>
                            <h3>رقم الجوال</h3>
                            <p><a><?php echo e($settings['phone']); ?></a></p>
                        </div>
                        <div class="contact-email">
                            <i class="ion-ios-email-outline"></i>
                            <h3>البريد الالكتروني</h3>
                            <p><a href="mailto:<?php echo e($settings['email']); ?>"><?php echo e($settings['email']); ?></a></p>
                        </div>
                        <div class="social-links">
                            <a href="<?php echo e($settings['twitter']); ?>" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="<?php echo e($settings['facebook']); ?>" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo e($settings['linkedin']); ?>" class="linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="<?php echo e($settings['instagram']); ?>" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="container">
                            <div class="form">

                                <!-- Form itself -->
                                <form action="" class="well" id="contactForm" >
                                    <div class="control-group">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" placeholder="الاسم" id="name">
                                            <p class="help-block"></p>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="email" name="email" class="form-control" placeholder="البريد الالكتروني" id="email">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <select name="msg_type" class="form-control">
                                                <option value="">نوع الرسالة</option>
                                                <option value="1">استفسار</option>
                                                <option value="0">شكوى</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <textarea name="message" rows="10" cols="100" class="form-control" placeholder="الرسالة" id="message"  style="resize:none"></textarea>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary" onclick="sendForm()">إرسال</button><br/>
                                </form>
                            </div>

                        </div>
                    </div>


                </div>
            </div>

            <div class="container mb-4 map">
                <iframe src="https://maps.google.com/maps?q=<?php echo e($settings['lat']); ?>%2C<?php echo e($settings['lng']); ?>&t=&z=13&ie=UTF8&iwloc=&output=embed" height="300" width="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>
            </div>

        </section><!-- #contact -->

    </main>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        function sendForm() {
            var data = document.querySelector('#contactForm');
            var dataForm = new FormData(data);
            console.log(dataForm);

            $.ajax({
                headers: {'X-CSRF-Token': "<?php echo e(csrf_token()); ?>"},
                url: "<?php echo e(route('contact.ajax')); ?>",
                type: 'post',
                processData: false,
                contentType: false,
                data: dataForm,

                success: function (data) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'center',
                        showConfirmButton: false,
                        timer: 2000
                    });

                    Toast.fire({
                        type: 'success',
                        title: data
                    })
                },
                
                error: function (data) {
                    result = '';
                    $.each(data.responseJSON.errors, function (i, item) {
                        result+= item + '\n';
                    });

                    console.log(result);
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: result
                    })

                }
            })
        }
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>