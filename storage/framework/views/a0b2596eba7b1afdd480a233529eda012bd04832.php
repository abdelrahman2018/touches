<?php $__env->startSection('title'); ?>
    <title>Basket</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="clients games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>سلة الطلبات</h2>
                </div>

                <div class="clients-det games-margin">
                    <?php if(auth('customer')->check()): ?>
                        <?php ($services = \App\Http\Controllers\Helpers\shoppingCart::wishlistFromDB()); ?>
                        <?php if(!empty($services->items)): ?>
                            <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="row">
                                    <div class="col-lg-3 about-img">
                                        <a href="<?php echo e(route('basket.remove', $service->rowId)); ?>"><i class="fa fa-close"></i></a>
                                    </div>
                                    <div class="col-lg-9 content">
                                        <h4><?php echo e($service->name); ?></h4>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <div class="row order">
                                    <?php if(\Illuminate\Support\Facades\Request::is('games*')): ?>
                                        <a href="<?php echo e(route('games.order.reserve')); ?>">أرسل الطلب</a>
                                    <?php elseif(\Illuminate\Support\Facades\Request::is('time*')): ?>
                                        <a href="<?php echo e(route('time.order.reserve')); ?>">أرسل الطلب</a>
                                    <?php else: ?>
                                        <a href="<?php echo e(route('land.order.reserve')); ?>">أرسل الطلب</a>
                                    <?php endif; ?>

                                </div>
                        <?php else: ?>
                            <h3 class="text-center">Basket is Empty</h3>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if(\Gloudemans\Shoppingcart\Facades\Cart::content()->count() != 0): ?>
                            <?php $__currentLoopData = \Gloudemans\Shoppingcart\Facades\Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="row">
                                    <div class="col-lg-3 about-img">
                                        <a href="<?php echo e(route('basket.remove', $item->rowId)); ?>"><i class="fa fa-close"></i></a>
                                    </div>
                                    <div class="col-lg-9 content">
                                        <h4><?php echo e($item->name); ?></h4>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <div class="row order">
                                    <?php if(\Illuminate\Support\Facades\Request::is('games*')): ?>
                                        <a href="<?php echo e(route('games.order.reserve')); ?>">أرسل الطلب</a>
                                    <?php elseif(\Illuminate\Support\Facades\Request::is('time*')): ?>
                                        <a href="<?php echo e(route('time.order.reserve')); ?>">أرسل الطلب</a>
                                    <?php else: ?>
                                        <a href="<?php echo e(route('land.order.reserve')); ?>">أرسل الطلب</a>
                                    <?php endif; ?>
                                </div>
                        <?php else: ?>
                            <h3 class="text-center">Basket is Empty</h3>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>

            </div>
        </section>

    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>