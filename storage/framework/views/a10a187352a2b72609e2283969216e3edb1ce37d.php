<?php $__env->startSection("title"); ?>
    
    users orders
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> <?php echo app('translator')->getFromJson('back/layout.dashboard'); ?></a></li>
                <li><a href="<?php echo e(route('now.index')); ?>"><i class="fa fa-bars"></i>all orders</a></li>
                <li class="active">all orders</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">

                        
                            
                                
                            
                        
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">
                                            <input type="hidden" id="body">
                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                    <tr>
                                                        <th width="5%">id</th>
                                                        <th><?php echo app('translator')->getFromJson('back/msg.name'); ?></th>
                                                        <th>organization</th>
                                                        <th><?php echo app('translator')->getFromJson('back/msg.phone'); ?></th>
                                                        <th>location</th>
                                                        <th><?php echo app('translator')->getFromJson('back/layout.added.modified'); ?></th>
                                                        <th width="10%">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td><?php echo e($order->id); ?></td>
                                                            <td><?php echo e($order->name); ?></td>
                                                            <td><?php echo e($order->organization); ?></td>
                                                            <td><?php echo e($order->phone); ?></td>
                                                            <td><?php echo e(\App\Site::findOrFail($order->site_id)->name); ?></td>

                                                            <td>
                                                                <strong><?php echo app('translator')->getFromJson('back/layout.added.date'); ?>: </strong>
                                                                <?php echo e($order->created_at); ?>

                                                                <br>
                                                                <strong><?php echo app('translator')->getFromJson('back/layout.modified.date'); ?>: </strong>
                                                                <?php echo e($order->updated_at); ?>

                                                            </td>
                                                            <td>
                                                                <a data-toggle="modal" data-placement="bottom" title="show" href="#myModal" data-id="<?php echo e($order->id); ?>" class="badge bg-red"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Order View</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $('.bg-red').on('click', function () {
            id = $(this).data('id');
            
            $.ajax({
                url: "<?php echo e(url('admin/orders/now')); ?>" + '/' + id,
                type: 'get',
                success: function (data) {
                      getSite(data.site_id);
                      getOrderGame(data.id);

                    $('.modal-body').html('');
                    $('.modal-body').append('<form>\n' +
                        '                        <div class="form-group">\n' +
                        '                            <label for="recipient-name" class="form-control-label">Name:</label>\n' +
                        '                            <input type="text" value="'+data.name+'" class="form-control" id="recipient-name" readonly>\n' +
                        '                        </div>\n' +
                        '                        <div class="form-group">\n' +
                        '                            <label for="message-text" class="form-control-label">Phone:</label>\n' +
                        '                            <input type="text" value="'+data.phone+'" class="form-control" id="recipient-name" readonly>\n' +
                        '                        </div>\n' +
                        '\n' +
                        '                        <div class="form-group">\n' +
                        '                            <label for="message-text" class="form-control-label">Address:</label>\n' +
                        '                            <input type="text" value="'+data.address+'" class="form-control" id="recipient-name" readonly>\n' +
                        '                        </div>\n' +
                        '\n' +
                        '                        <div class="form-group">\n' +
                        '                            <label for="message-text" class="form-control-label">Location:</label>\n' +
                        '                            <input id="location" type="text" value="" class="form-control" id="recipient-name" readonly>\n' +
                        '                        </div>\n' +
                        '\n' +
                        '                        <div class="form-group">\n' +
                        '                            <label for="message-text" class="form-control-label">organization:</label>\n' +
                        '                            <input type="text" value="'+data.organization+'" class="form-control" id="recipient-name" readonly>\n' +
                        '                        </div>\n' +

                        '                        <div class="form-group">\n' +
                        '                            <label for="message-text" class="form-control-label">Games:</label>\n' +
                        '                            <input type="text"  class="form-control" id="game" readonly>\n' +
                        '                        </div>\n' +
                        '\n' +
                        '                        <div class="form-group">\n' +
                        '                            <label for="message-text" class="form-control-label">Map:</label>\n' +
                        '                            <iframe src="https://maps.google.com/maps?q='+data.lat+'%2C'+data.lng+'&t=&z=13&ie=UTF8&iwloc=&output=embed" height="300" width="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>\n' +
                        '                        </div>\n' +
                        '                    </form>');
                }
            })
        })
    </script>

    <script>

        function getSite(id) {
            $.ajax({
                url: "<?php echo e(route('now.site')); ?>",
                type: 'post',
                data:{
                    _token: '<?php echo e(csrf_token()); ?>',
                    site_id: id
                },
                success: function (data) {
                    $('#location').val(data);
                }
            });

        }

    </script>

    <script>
        function getOrderGame(orderId) {
            $.ajax({
                url: "<?php echo e(route('order.game')); ?>",
                type: 'post',
                data:{
                    _token: '<?php echo e(csrf_token()); ?>',
                    order_id: orderId
                },
                success: function (data) {
                    $('#game').val(data);
                    console.log(data);
                }
            });

        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>