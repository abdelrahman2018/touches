<?php $__env->startSection('title'); ?>
    <title>Reset password</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>



    <main id="main">

        <!--==========================
      Clients Section
    ============================-->
        <section class="about padding games-content wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>تغيير كلمة المرور</h2>
                </div>

                <div class="clients-det login-margin">
                    <div class="row">
                        <form action="<?php echo e(route('newPassword.submit')); ?>" method="post" class="well games-form">
                            <?php echo csrf_field(); ?>
                            <input type="hidden" name="token" value="<?php echo e($token); ?>">

                            <div class="control-group">
                                <div class="form-group">
                                    <label>البريد الالكتروني</label>
                                    <input type="email" name="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" value="<?php echo e($email ?? old('email')); ?>" >

                                    <?php if($errors->has('email')): ?>
                                        <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="form-group">
                                    <label>كلمة المرور الجديدة:</label>
                                    <input type="password" name="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>">
                                    <?php if($errors->has('password')): ?>
                                        <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('password')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>اعادة كلمة المرور الجديدة:</label>
                                    <input type="password" name="password_confirmation" class="form-control">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ارسال</button>
                        </form>
                    </div>

                </div>

            </div>
        </section>

    </main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>