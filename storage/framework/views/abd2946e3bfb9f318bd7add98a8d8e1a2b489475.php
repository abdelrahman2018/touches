<?php $__env->startSection("title"); ?>
    
    Sub Categories
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo e(route('sub.index')); ?>"><i class="fa fa-bars"></i> List All Sub-Categories</a></li>
                <li class="active">Sub Categories</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">Listing All Sub Categories </h1>
                            <div class="box-tools <?php if(app()->getLocale() == 'en'): ?> pull-right <?php else: ?> pull-left <?php endif; ?>">
                                <a href="<?php echo e(route('sub.create')); ?>" type="button" class="btn btn-block btn-primary">Add New Sub-Category</a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="10%">id</th>
                                                    <th>name</th>
                                                    <th>image</th>
                                                    <th>main category</th>
                                                    <th>Added/Last modified date</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($category->id); ?></td>
                                                        <td><?php echo e($category->name); ?></td>
                                                        <td>
                                                            <a href="<?php echo e(asset('public/uploads/SubCats/'.$category->image)); ?>" data-fancybox> <img src="<?php echo e(asset('public/uploads/SubCats/'.$category->image)); ?>" width="100%"></a>
                                                        </td>
                                                        <td><?php echo e($category->parent->name); ?></td>
                                                        <td>
                                                            <strong>Added date: </strong>
                                                            <?php echo e($category->created_at); ?>

                                                            <br>
                                                            <strong>Modified date: </strong>
                                                            <?php echo e($category->updated_at); ?>

                                                        </td>
                                                        <td>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="<?php echo e(route('sub.edit', $category->id)); ?>" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="Delete" href="<?php echo e(route('sub.delete', $category->id)); ?>"  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            
                                                            
                                                            
                                                            
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>