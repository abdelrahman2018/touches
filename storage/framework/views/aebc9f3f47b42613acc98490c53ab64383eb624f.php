<?php $__env->startSection("title"); ?>
    
    Albums
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo e(route('album.index')); ?>"><i class="fa fa-bars"></i> List All albums</a></li>
                <li class="active">all albums</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1 class="box-title">Listing albums </h1>
                            <div class="box-tools <?php if(app()->getLocale() == 'en'): ?> pull-right <?php else: ?> pull-left <?php endif; ?>">
                                <a href="<?php echo e(route('album.create')); ?>" type="button" class="btn btn-block btn-primary">Add New album</a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <table id="dataTable" class="table table-bordered table-striped">

                                                <thead>
                                                <tr>
                                                    <th width="5%">id</th>
                                                    <th>title</th>
                                                    <th>image</th>
                                                    <th>Added/Last modified date</th>
                                                    <th width="10%">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php $__currentLoopData = $albums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $album): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($album->id); ?></td>
                                                        <td><?php echo e($album->title); ?></td>
                                                        <td>
                                                            <a href="<?php echo e(asset('public/uploads/albums/'.$album->image)); ?>" data-fancybox> <img src="<?php echo e(asset('public/uploads/albums/'.$album->image)); ?>" width="100%"></a>
                                                        </td>
                                                        <td>
                                                            <strong>Added date: </strong>
                                                            <?php echo e($album->created_at); ?>

                                                            <br>
                                                            <strong>Modified date: </strong>
                                                            <?php echo e($album->updated_at); ?>

                                                        </td>
                                                        <td>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="<?php echo e(route('album.edit', $album->id)); ?>" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                                            <a data-toggle="tooltip" data-placement="bottom" title="Delete" href="<?php echo e(route('album.delete', $album->id)); ?>"  class="badge bg-red" style="background-color: red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            <a data-toggle="modal" data-placement="bottom" title="Add Images" href="#myModal" data-id="<?php echo e($album->id); ?>"  class="badge bg-red images" style="background-color: green"><i class="fas fa-plus-circle" aria-hidden="true"></i></a>
                                                            
                                                            
                                                            
                                                            
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Images</h4>
                </div>

                <form  id="imagesForm" method="post" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 col-md-3 control-label">Image</label>
                            <div class="col-sm-10 col-md-4">
                                <input id="newImage" name="images[]"  type="file" multiple>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="submit" class="btn btn-primary" data-dismiss="modal">Save</button>
                    </div>
                </form>

            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $('.images').on('click', function () {
            album_id = $(this).data('id');
        });


       $('#submit').on('click', function () {
           form = document.getElementById('imagesForm');
           var fd = new FormData(form);
           fd.append('album_id', album_id);

           console.log(fd);
           $.ajax({
              headers: {
                  'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
              },
              url: "<?php echo e(route('image.submit')); ?>",
              type: 'post',
              processData: false,
              contentType: false,
              data: fd,
              success: function (data) {
                  // alert('done')
                  Swal.fire({
                      position: 'top-end',
                      type: 'success',
                      title: data,
                      showConfirmButton: false,
                      timer: 1500
                  })
              },

               error: function (data) {
                   Swal.fire({
                       type: 'error',
                       title: 'Oops...',
                       text: data.msg
                   })
               }
          })
       })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>