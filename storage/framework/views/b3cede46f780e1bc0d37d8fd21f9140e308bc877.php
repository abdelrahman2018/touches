<?php $__env->startSection("title"); ?>
    
    edit event
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo e(route('event.index')); ?>"><i class="fa fa-bars"></i> List All events</a></li>
                <li class="active">edit news</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">edit event </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="<?php echo e(route('event.update', $event->id)); ?>" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>

                                                <?php $__currentLoopData = $locales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loc=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">title (<?php echo e($val); ?>)</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="title[<?php echo e($loc); ?>]" value="<?php echo e($event{'title:'.$loc.''}); ?>" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="desc" class="col-sm-2 col-md-3 control-label">description (<?php echo e($val); ?>)</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <textarea name="desc[<?php echo e($loc); ?>]" id="desc[<?php echo e($loc); ?>]">
                                                                <?php echo e($event{'description:'.$loc.''}); ?>

                                                            </textarea>
                                                        </div>
                                                    </div>

                                                    <script>
                                                        CKEDITOR.replace( 'desc[<?php echo e($loc); ?>]' );
                                                    </script>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Image</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input id="newImage" name="image" type="file">
                                                        <img src="<?php echo e(asset('public/uploads/events/'.$event->image)); ?>" width="100px">
                                                    </div>
                                                </div>

                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">update event</button>
                                                    <a href="<?php echo e(route('event.index')); ?>" type="button" class="btn btn-default">Back</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>