<?php $__env->startSection("title"); ?>
    
    
    View Delegate
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> <?php echo app('translator')->getFromJson('back/layout.dashboard'); ?></a></li>
                <li><a href="<?php echo e(route('del.index')); ?>"><i class="fa fa-bars"></i> List all Delegates</a></li>
                <li class="active">View Delegate</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">View Delegate </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>



                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#edit">Edit</a></li>
                                            <li><a id="orders" data-toggle="tab" href="#orders">Orders</a></li>
                                            <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div id="edit" class="tab-pane fade in active">
                                                <h3>Edit Delegate</h3>
                                                <form method="POST" action="<?php echo e(route('del.update', $delegate->id)); ?>" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                    <?php echo csrf_field(); ?>


                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">Name</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="name" value="<?php echo e($delegate->name); ?>" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">Username</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="username" value="<?php echo e($delegate->username); ?>" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">Phone</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" id="phone-number" name="phone" value="<?php echo e($delegate->phone); ?>" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">Email</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="email" name="email" value="<?php echo e($delegate->email); ?>"  class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                    <hr style="font-size: larger;">

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">Do You want change password</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="checkbox" name="passCheck" value="yes" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">Password</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="password" name="password"  class="form-control field-validate">
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo app('translator')->getFromJson('back/layout.image'); ?></label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input id="newImage" name="image" type="file">
                                                            <img src="<?php echo e(asset('public/uploads/delegates/'.$delegate->image)); ?>" width="100px">
                                                        </div>
                                                    </div>

                                                    <!-- /.box-body -->
                                                    <div class="box-footer text-center">
                                                        <button type="submit" class="btn btn-primary"><?php echo app('translator')->getFromJson('back/layout.edit'); ?></button>
                                                        <a href="<?php echo e(route('product.index')); ?>" type="button" class="btn btn-default"><?php echo app('translator')->getFromJson('back/layout.back'); ?></a>
                                                    </div>
                                                    <!-- /.box-footer -->
                                                </form>


                                            </div>

                                            <div id="orders" class="tab-pane fade">
                                                <h3>Orders</h3>
                                                <table id="dataTable" class="table table-bordered table-striped">

                                                    <thead>
                                                    <tr>
                                                        <th width="5%">id</th>
                                                        <th>date</th>
                                                        <th>address</th>
                                                        <th>client</th>
                                                        <th>status</th>
                                                        <th>total</th>
                                                        <th>Added date</th>
                                                        <th width="10%">Action</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody id="orders-body">
                                                    
                                                        
                                                            
                                                            
                                                            
                                                            
                                                                
                                                            
                                                            
                                                            
                                                            
                                                            
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            
                                                            
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            
                                                        
                                                    
                                                    </tbody>
                                                </table>
                                            </div>


                                            <div id="menu2" class="tab-pane fade">
                                                <h3>Menu 2</h3>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                            </div>
                                            <div id="menu3" class="tab-pane fade">
                                                <h3>Menu 3</h3>
                                                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                            </div>
                                        </div>
                                        <div class="box-body">


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $('#orders').on('click', function () {
           $.ajax({
               type: 'post',
               url: "<?php echo e(route('del.orders')); ?>",
               data: {
                   _token: '<?php echo e(csrf_token()); ?>',
                   id: '<?php echo e($delegate->id); ?>'
               },
               success: function (data) {
                    data.data.forEach(function(item){
                        status = getStatus(item.id);
                        show = "<?php echo e(url('order/show')); ?>"+'/'+ item.id;
                        console.log(show);
                        body ='<tr>';
                        body+= '<td>'+item.id+'</td>';
                        body+= '<td>'+item.date+'</td>';
                        if (item.payment_type == 0)
                            body+= '<td>Cash</td>';
                        else
                            body+= '<td>Visa</td>';


                        body+= '<td>'+item.address["city"]+'</td>';
                        body+= '<td>'+item.user.name+'</td>';
                        body+= '<td>'+status.res+'</td>';
                        body+= '<td>'+item.total+'</td>';
                        body+= '<td>'+item.created_at+'</td>';
                        body+= '<td>'+
                            '  <a data-toggle="tooltip" data-placement="bottom" title="View" href="'+show+'" class="badge bg-light-blue" style="background-color: #0d95e8"><i class="fas fa-eye" aria-hidden="true"></i></a>'
                            +'</td>';

                        body+= '</tr>'
                    });
                    console.log(body);
                    $('#orders-body').append(body);
               }
           })
        });

    </script>

    <script>
        function getStatus(orderId) {
            var st = '';
            $.ajax({
                type: 'post',
                url: "<?php echo e(route('del.order.status')); ?>",
                data: {
                    _token: '<?php echo e(csrf_token()); ?>',
                    id: orderId
                },
                success: function (data) {
                    st =  data;
                }
            });
            return st;
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>