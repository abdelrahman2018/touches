<?php $__env->startSection('title'); ?>
    <title><?php echo e($album->title); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
       Our Portfolio Section
     ============================-->
        <section class="portfolio wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2><?php echo e($album->title); ?></h2>
                </div>
            </div>

            <div class="container">
                <div class="row no-gutters">

                    <?php $__currentLoopData = $album->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-3">
                            <div class="portfolio-item wow fadeInUp">
                                <a href="<?php echo e(asset('public/uploads/images/'.$image->image)); ?>" class="portfolio-popup">
                                    <img src="<?php echo e(asset('public/uploads/images/'.$image->image)); ?>" alt="">
                                </a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

            </div>
        </section><!-- #portfolio -->

    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>