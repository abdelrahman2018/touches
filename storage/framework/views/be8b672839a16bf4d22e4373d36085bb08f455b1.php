
<?php if(\Illuminate\Support\Facades\Request::is('games*')): ?>
    <?php echo $__env->make('front.includes.header-games', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php elseif(\Illuminate\Support\Facades\Request::is('time*')): ?>
    <?php echo $__env->make('front.includes.header-time', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php elseif(\Illuminate\Support\Facades\Request::is('land*')): ?>
    <?php echo $__env->make('front.includes.header-land', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php else: ?>
    <?php echo $__env->make('front.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>

<?php echo $__env->yieldContent('content'); ?>




<!--==========================
  Footer - Do not remove credits
============================-->
<footer id="footer" class="games-footer ">
    <div class="container">
        <div class="credits">
            © جميع الحقوق محفوظة لكثيب -<a href="http://atiafco.com/" target="_blank"> تصميم وبرمجة شركة اطياف للحلول المتكاملة  	</a>
        </div>
    </div>
</footer>

<a href="#" class="back-to-top games-top"><i class="fa fa-chevron-up"></i></a>

<!-- JavaScript  -->
<script src="<?php echo e(asset('public/lib/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/lib/jquery/jquery-migrate.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/lib/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/lib/easing/easing.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/lib/superfish/hoverIntent.js')); ?>"></script>
<script src="<?php echo e(asset('public/lib/superfish/superfish.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/lib/wow/wow.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/lib/owlcarousel/owl.carousel.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/lib/magnific-popup/magnific-popup.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/lib/sticky/sticky.js')); ?>"></script>


<script src="<?php echo e(asset('public/js/front/main.js')); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<?php echo $__env->yieldContent('scripts'); ?>


    <?php if(session()->has('success')): ?>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'center',
            showConfirmButton: false,
            timer: 2000
        });

        Toast.fire({
            type: 'success',
            title: "<?php echo e(session()->get('success')); ?>"
        })
    </script>
    <?php endif; ?>
</body>
</html>