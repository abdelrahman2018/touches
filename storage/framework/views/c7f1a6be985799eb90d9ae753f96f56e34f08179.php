<?php $__env->startSection("title"); ?>
    
    edit album
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>
    <style>
        .close{

        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="content-wrapper" style="min-height: 1060px;">
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo e(route('album.index')); ?>"><i class="fa fa-bars"></i> List All albums</a></li>
                <li class="active">edit album</li>
            </ol>
        </section>


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">edit album </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <?php echo $__env->make('dashboard.includes.feedback', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <br>

                                        <!-- form start -->
                                        <div class="box-body">

                                            <form method="POST" action="<?php echo e(route('album.update', $album->id)); ?>" accept-charset="UTF-8" class="form-horizontal form-validate" enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>

                                                <?php $__currentLoopData = $locales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loc=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-3 control-label">title (<?php echo e($val); ?>)</label>
                                                        <div class="col-sm-10 col-md-4">
                                                            <input type="text" name="title[<?php echo e($loc); ?>]" value="<?php echo e($album{'title:'.$loc.''}); ?>" class="form-control field-validate">
                                                        </div>
                                                    </div>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">show in home</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input type="checkbox" name="show" <?php if($album->show_home == 1): ?> checked <?php endif; ?>>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">Image</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <input id="newImage" name="image" type="file">
                                                        <img src="<?php echo e(asset('public/uploads/albums/'.$album->image)); ?>" width="100px">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="name" class="col-sm-2 col-md-3 control-label">album images</label>
                                                    <div class="col-sm-10 col-md-4">
                                                        <table>
                                                            <tbody>

                                                                <?php $__currentLoopData = $album->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="<?php echo e(asset('public/uploads/images/'.$image->image)); ?>" data-fancybox>  <img src="<?php echo e(asset('public/uploads/images/'.$image->image)); ?>" width="100px"></a>
                                                                        </td>

                                                                        <td>
                                                                            <a href="#" data-id="<?php echo e($image->id); ?>" class="close" style="color: red">&times;</a>
                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>



                                                <!-- /.box-body -->
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">update album</button>
                                                    <a href="<?php echo e(route('album.index')); ?>" type="button" class="btn btn-default">Back</a>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $('.close').on('click', function () {
            id = $(this).data('id');
            btn = $(this);

            $.ajax({
                url: "<?php echo e(route('image.delete')); ?>",
                type: 'post',
                data: {
                    _token: '<?php echo e(csrf_token()); ?>',
                    img_id: id
                },
                success: function (data) {
                    btn.closest('tr').remove();
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: data,
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            })
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>