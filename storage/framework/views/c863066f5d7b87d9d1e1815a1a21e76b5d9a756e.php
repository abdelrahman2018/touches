

<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="<?php echo e(asset('public/css/admin/bootstrap.min.css')); ?>" />

    <?php if(app()->getLocale() == 'en'): ?>
        <link rel="stylesheet" href="<?php echo e(asset('public/css/admin/style.css')); ?>" />
    <?php else: ?>
        <link rel="stylesheet" href="<?php echo e(asset('public/css/admin/bootstrap-rtl.css')); ?>" />
        <link rel="stylesheet" href="<?php echo e(asset('public/css/admin/style-en.css')); ?>" />
    <?php endif; ?>

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/css/admin/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/css/admin/settings.css')); ?>" media="screen" />
    <link rel="stylesheet" href="<?php echo e(asset('public/css/admin/jquery.dataTables.min.css')); ?>" media="screen" />
    <link rel="stylesheet" href="<?php echo e(asset('public/css/admin/settings.css')); ?>" media="screen" />
    <link rel="stylesheet" href="<?php echo e(asset('public/css/admin/editor.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/css/admin/jquery.fancybox.min.css')); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/css/admin/daterangepicker.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/css/admin/toastr.min.css')); ?>" />
    <link rel="icon" href="<?php echo e(asset('public/images2/favicon.png')); ?>" type="image/png">
    <title>Katheeb - Admin</title>



    
    

    

    
    

    
    
    

    
    
</head>
<body>
    <div id="app">
        
            
                
                    
                
                
                    
                

                
                    
                    

                    

                    
                    
                        
                        
                            
                                
                            
                            
                                
                                    
                                
                            
                        
                            
                                
                                    
                                

                                
                                    
                                       
                                                     
                                        
                                    

                                    
                                        
                                    
                                
                            
                        
                    
                
            
        

        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>
</body>
</html>
