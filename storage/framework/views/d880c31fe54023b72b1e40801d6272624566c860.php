<ul class="sidebar navbar-nav">
    <a href="">
        <li class="userimg">
            <p> <?php echo app('translator')->getFromJson('back/layout.dashboard'); ?> </p>
        </li>
    </a>



    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('slide.index')); ?>" id="" role="button">
            <i class="glyphicon glyphicon-picture"></i> <span><?php echo app('translator')->getFromJson('back/layout.slides'); ?></span>
        </a>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('packet.index')); ?>" id="" role="button">
            <i class="fab fa-get-pocket"></i> <span>Packets</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('unit.index')); ?>" id="" role="button">
            <i class="fab fa-mizuni"></i> <span>Units</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('company.index')); ?>" id="" role="button">
            <i class="fas fa-building"></i> <span>Companies</span>
        </a>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('product.index')); ?>" id="" role="button">
            <i class="fas fa-star"></i> <span>Products</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('customer.index')); ?>" id="" role="button">
            <i class="fa fa-users"></i> <span>Customers</span>
        </a>
    </li>

    <li class="nav-item" data-toggle="tooltip" data-placement="right">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#dels"
           data-parent="#exampleAccordion">
            <i class="fa fa-certificate"></i>
            <span class="nav-link-text">Delegates</span>
        </a>

        <ul class="sidenav-second-level collapse" id="dels">

            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('del.index')); ?>" id="" role="button">
                    <i class="fa fa-circle-notch"></i> <span>All delegates</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('del.create')); ?>" id="" role="button">
                    <i class="fa fa-plus"></i> <span>Create</span>
                </a>
            </li>

        </ul>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('coupon.index')); ?>" id="" role="button">
            <i class="fas fa-compact-disc"></i> <span>Coupons</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('client.index')); ?>" id="" role="button">
            <i class="fas fa-user-friends"></i> <span>Clients</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('contact.msgs')); ?>" id="" role="button">
            <i class="fa fa-envelope"></i> <span><?php echo app('translator')->getFromJson('back/layout.msgs'); ?></span>
        </a>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('page.index')); ?>" id="" role="button">
            <i class="fa fa-file"></i> <span><?php echo app('translator')->getFromJson('back/layout.pages'); ?></span>
        </a>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('settings.edit')); ?>" id="" role="button">
            <i class="fa fa-cogs"></i> <span><?php echo app('translator')->getFromJson('back/layout.setting'); ?></span>
        </a>
    </li>

</ul>
