<?php $__env->startSection('title'); ?>
    <title>Events</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
          Testimonials Section
        ============================-->
        <section class="testimonials wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>الفعاليات</h2>
                </div>
                <div class="row no-gutters">

                    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-3 testimonial-item">
                            <div class="latest-post">
                                <img src="<?php echo e(asset('public/uploads/events/'.$event->image)); ?>" class="img-responsive" alt="">
                                <div class="details">
                                    <h4><?php echo e($event->title); ?></h4>
                                    <p><?php echo e(str_limit(strip_tags(html_entity_decode($event->description)), 35)); ?></p>
                                    <a href="<?php echo e(route('event.front.show', $event->id)); ?>" class="btn btn-primary">المزيد</a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

            </div>
        </section><!-- #testimonials -->

    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>