<?php $__env->startSection('title'); ?>
    <title>Contact Us</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
      Contact Section
    ============================-->
        <section class="contact wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>اتصل بنا</h2>
                </div>

                <div class="row contact-info">
                    <div class="col-lg-5">
                        <div class="contact-address">
                            <i class="ion-ios-location-outline"></i>
                            <h3>العنوان</h3>
                            <?php if(app()->getLocale() == 'ar'): ?>
                                <address><?php echo e($settings['address_ar']); ?></address>
                            <?php else: ?>
                                <address><?php echo e($settings['address_en']); ?></address>
                            <?php endif; ?>

                        </div>
                        <div class="contact-phone">
                            <i class="ion-ios-telephone-outline"></i>
                            <h3>رقم الجوال</h3>
                            <p><a><?php echo e($settings['phone']); ?></a></p>
                        </div>
                        <div class="contact-email">
                            <i class="ion-ios-email-outline"></i>
                            <h3>البريد الالكتروني</h3>
                            <p><a href="mailto:<?php echo e($settings['email']); ?>"><?php echo e($settings['email']); ?></a></p>
                        </div>
                        <div class="social-links">
                            <a href="<?php echo e($settings['twitter']); ?>" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="<?php echo e($settings['facebook']); ?>" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo e($settings['linkedin']); ?>" class="linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="<?php echo e($settings['instagram']); ?>" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="container">
                            <div class="form">

                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger">
                                        <ul>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                                <!-- Form itself -->
                                <form action="<?php echo e(route('contact.send')); ?>" method="post" class="well" id="contactForm" >
                                    <?php echo csrf_field(); ?>
                                    <div class="control-group">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" placeholder="الاسم" id="name">
                                            <p class="help-block"></p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="email" name="email" class="form-control" placeholder="البريد الالكتروني" id="email">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <select name="msg_type" class="form-control">
                                                <option value="">نوع الرسالة</option>
                                                <option value="1">استفسار</option>
                                                <option value="0">شكوى</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                        <textarea name="message" rows="10" cols="100" class="form-control" placeholder="الرسالة" id="message"  style="resize:none"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">إرسال</button><br />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container mb-4 map">
                <iframe src="https://maps.google.com/maps?q=<?php echo e($settings['lat']); ?>%2C<?php echo e($settings['lng']); ?>&t=&z=13&ie=UTF8&iwloc=&output=embed" height="350" width="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>
            </div>

        </section><!-- #contact -->


    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>