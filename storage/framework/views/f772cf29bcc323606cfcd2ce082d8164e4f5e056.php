<?php $__env->startSection('title'); ?>
    <title>Albums</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main id="main">

        <!--==========================
       Our Portfolio Section
     ============================-->
        <section class="portfolio wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>ألبوم الصور</h2>
                </div>
            </div>

            <div class="container">
                <div class="row no-gutters">

                    <?php $__currentLoopData = $albums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $album): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-3">
                            <div class="portfolio-item wow fadeInUp">
                                <a href="<?php echo e(route('album.front.show', $album->id)); ?>">
                                    <img src="<?php echo e(asset('public/uploads/albums/'.$album->image)); ?>" alt="">
                                    <h5><?php echo e($album->title); ?></h5>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

            </div>
        </section><!-- #portfolio -->

    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>